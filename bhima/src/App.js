import React from "react";
import './App.css';
import './style.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import Header from './Layouts/Header'
import { Routes, Route } from 'react-router';
import Index from "./Pages/Index";
import PrepaidPlan from "./Pages/PrepaidPlan";
import StepForm from "./Pages/StepForm";
import MorePlans from "./Pages/MorePlans";
import { Carousel } from "react-bootstrap";

function App() {
  return (
   <>
   <Header/>
    <Routes>
      <Route path="/" element={<Index/>}/>
      <Route path="/prepaid-plan" element={<PrepaidPlan/>}/>
      <Route path="/step-form" element={<StepForm/>}/>
      <Route path="/more-plans" element={<MorePlans/>}/>
      <Route path="/carousel" element={<Carousel/>}/>
    </Routes>
   </>
  );
}

export default App;
