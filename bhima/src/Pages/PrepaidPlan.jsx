import React from "react";
import {
  Grid,
  Box,
  Modal,
  Fade,
  Backdrop,
  Radio,
  RadioGroup,
  FormControlLabel,
  FormControl,
  Stack,
  styled,
  Button,
} from "@mui/material";
import { FiUpload, FiXCircle } from "react-icons/fi";
import { Container, Row, Col } from "react-bootstrap";
import { FaCheckCircle } from "react-icons/fa";
import { useNavigate } from "react-router";

const style = {
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: 700,
  bgcolor: "background.paper",
  border: "2px solid #000",
  boxShadow: 24,
  pt: 2,
  px: 4,
  pb: 3,
};
const BpIcon = styled("span")(({ theme }) => ({
  borderRadius: "50%",
  width: 16,
  height: 16,
  boxShadow:
    theme.palette.mode === "dark"
      ? "0 0 0 1px rgb(16 22 26 / 40%)"
      : "inset 0 0 0 1px rgba(16,22,26,.2), inset 0 -1px 0 rgba(16,22,26,.1)",
  backgroundColor: theme.palette.mode === "dark" ? "#394b59" : "#f5f8fa",
  backgroundImage:
    theme.palette.mode === "dark"
      ? "linear-gradient(180deg,hsla(0,0%,100%,.05),hsla(0,0%,100%,0))"
      : "linear-gradient(180deg,hsla(0,0%,100%,.8),hsla(0,0%,100%,0))",
  ".Mui-focusVisible &": {
    outline: "2px auto #CB0171",
    outlineOffset: 2,
  },
  "input:hover ~ &": {
    backgroundColor: theme.palette.mode === "dark" ? "#30404d" : "#CB0171",
  },
  "input:disabled ~ &": {
    boxShadow: "none",
    background:
      theme.palette.mode === "dark"
        ? "rgba(57,75,89,.5)"
        : "rgba(206,217,224,.5)",
  },
}));

const BpCheckedIcon = styled(BpIcon)({
  backgroundColor: "#CB0171",
  backgroundImage:
    "linear-gradient(180deg,hsla(0,0%,100%,.1),hsla(0,0%,100%,0))",
  "&:before": {
    display: "block",
    width: 16,
    height: 16,
    backgroundImage: "radial-gradient(#fff,#fff 28%,transparent 32%)",
    content: '""',
  },
  "input:hover ~ &": {
    backgroundColor: "#CB0171",
  },
});

function BpRadio(props) {
  return (
    <Radio
      disableRipple
      color="default"
      checkedIcon={<BpCheckedIcon />}
      icon={<BpIcon />}
      {...props}
    />
  );
} 
function ChildModal() {
  const [open, setOpen] = React.useState(false);
  const handleOpen = () => {
    setOpen(true);
  };
  const handleClose = () => {
    setOpen(false);
  };

  return (
    <React.Fragment>
      <h4 className="add-ekyc" onClick={handleOpen}>e-KYC</h4>

      <Modal
        open={open}
        onClose={handleClose}
        aria-labelledby="child-modal-title"
        aria-describedby="child-modal-description"
      >
        <Box sx={{ ...style }}>
          <div className="">
            <iframe
              width="100%"
              height="500"
              src="https://kyc.sandboxing.tech/"
            ></iframe>
          </div>
          <Button onClick={handleClose}>Close</Button>
        </Box>
      </Modal>
    </React.Fragment>
  );
}

const PrepaidPlan = () => {
  const navigate = useNavigate();
  const [open, setOpen] = React.useState(false);
  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);

  return (
    <div>
      <Box className="prepaid-plan-banner" sx={{ flexGrow: 1 }}>
        <Container className="h-100">
          <Grid className="h-100 align-items-center" container spacing={2}>
            <Grid className="" item xs={6}>
              {/* <h2 className="home-banner-title text-center">
                IM3 Prepaid Plan{" "}
              </h2> */}
            </Grid>
            <Grid className="" item xs={6}></Grid>
          </Grid>
        </Container>
      </Box>
      <Box className="best-offer py-4">
        <Container>
          <h3 className="primary-heading">
            Our Best Offer (Pre - Select Plan)
          </h3>
          <h3 className="plan-text py-3">**Plan IOH- 250**</h3>
          <div className="plan-benefits">
            <img className="coupon" src="../images/cupon.png" alt="cupon" />
            <h3 className="secondary-heading text-center mb-5">
              Plan Benefits :-
            </h3>
            <Row className="mx-md-4">
              <Col md={3}>
                <h4 className="d-flex align-items-center">
                  <FaCheckCircle className="check-green me-3" />
                  Talk Time
                </h4>
              </Col>
              <Col md={3}>
                <h4 className="d-flex align-items-center">
                  <FaCheckCircle className="check-green me-3" />
                  Data
                </h4>
              </Col>
              <Col md={3}>
                <h4 className="d-flex align-items-center">
                  <FaCheckCircle className="check-green me-3" />
                  SMS
                </h4>
              </Col>
              <Col md={3}>
                <h4 className="d-flex align-items-center">
                  <FaCheckCircle className="check-green me-3" />
                  Free Offerings
                </h4>
              </Col>
            </Row>
            <Box className="mt-3">
              <ul className="ms-md-4">
                <li>Validity 30 Days</li>
                <li>Main quota 18 GB</li>
                <li>Pulsa Safe</li>
                <li>
                  Available at *123*111#, myBHIMA apps, BHIMA Official WhatsApp,
                  e-Commerce and outlets
                </li>
                <li>Call to BHIMA and Tri Limitless (5,000 min)</li>
                <li>PRICE Rp 60,000</li>
              </ul>
            </Box>
          </div>

          <Box className="text-center mt-5">
            <button className="primary-btn" onClick={handleOpen}>
              Buy Now
            </button>
          </Box>
          <Box className="text-center mt-3">
            <a className="link-btn" onClick={() => navigate("/more-plans")}>
              More Plans
            </a>
          </Box>
        </Container>
      </Box>

      <Modal
        aria-labelledby="transition-modal-title"
        aria-describedby="transition-modal-description"
        open={open}
        onClose={handleClose}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 500,
        }}
      >
        <Fade in={open}>
          <Box className="get-plan-modal">
            <div className="get-paln-inner">
              <Box className="text-end">
                <FiXCircle
                  onClick={() => handleClose()}
                  className="color-AFAFB1 cursor-pointer"
                />
              </Box>
              <div className="p-md-4">
              <ChildModal />
                or 
                <h4 className="modal-heading">Upload KYC</h4>

                <div className="">
                  <FormControl>
                    <RadioGroup
                      row
                      defaultValue="Resident"
                      aria-labelledby="demo-row-radio-buttons-group-label"
                      name="row-radio-buttons-group"
                    >
                      <FormControlLabel
                        value="Resident"
                        control={<BpRadio />}
                        label="Resident"
                      />
                      <FormControlLabel
                        value="Tourist"
                        control={<BpRadio />}
                        label="Tourist"
                      />
                    </RadioGroup>
                  </FormControl>
                  <Box className="upload-card p-3">
                    <div className="py-4 upload-text">
                      <p className="mb-0">Upload document</p>
                      <small>Supports png, jpeg, pdf (max size 2mb)</small>
                    </div>
                    <Stack direction="row" alignItems="center" spacing={2}>
                      <Box
                        className="upload-file"
                        color="primary"
                        aria-label="upload picture"
                        component="label"
                      >
                        <FiUpload className="color-f58a1f" />
                        <p className="m-0">Upload</p>
                        <input hidden accept="image/*" type="file" />
                      </Box>
                    </Stack>
                  </Box>
                </div>
                <Box
                  onClick={() => navigate("/step-form")}
                  className="text-center"
                >
                  <button className="modal-btn">Submit</button>
                </Box>
              </div>
            </div>
          </Box>
        </Fade>
      </Modal>
    </div>
  );
};

export default PrepaidPlan;
