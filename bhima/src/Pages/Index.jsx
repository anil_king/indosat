import React from "react";
import { styled } from "@mui/material/styles";
import { Grid, Paper, Box, Link } from "@mui/material";
import { Container } from "react-bootstrap";
import { useNavigate } from "react-router";

const Index = () => {
  const navigate = useNavigate();
  const Item = styled(Paper)(({ theme }) => ({
    backgroundColor: theme.palette.mode === "dark" ? "#1A2027" : "#fff",
    ...theme.typography.body2,
    padding: theme.spacing(1),
    textAlign: "center",
    color: theme.palette.text.secondary,
  }));

  return (
    <div>
      <Box
        onClick={() => navigate("/prepaid-plan")}
        className="hb-cursor prepaid-banner"
        sx={{ flexGrow: 1 }}
      >
        <img className="" src="../images/happy100.jpg" alt="cupon" />
        {/* <Container className="h-100">
          <Grid className="h-100 align-items-end" container spacing={2}>
            <Grid className="" item xs={6}>
              <h2 className="home-banner-title text-center">
                IM3 Prepaid Plan{" "}
              </h2>
            </Grid>
            <Grid className="" item xs={6}></Grid>
          </Grid>
        </Container> */}
      </Box>

      <Box className="postpaid-banner" sx={{ flexGrow: 1 }}>
        <img className="" src="../images/happy55.png" alt="cupon" />
        {/* <Container className="h-100">
          <Grid className="h-100 align-items-end" container spacing={2}>
            <Grid className="" item xs={6}></Grid>
            <Grid className="" item xs={6}>
              <h2 className="home-banner-title text-center">
                IM3 Prepaid Plan{" "}
              </h2>
            </Grid>
          </Grid>
        </Container> */}
      </Box>

      <Box className="starter-banner" sx={{ flexGrow: 1 }}>
      <img className="" src="../images/happy42.png" alt="cupon" />
        {/* <Container className="h-100">
          <Grid className="h-100 align-items-end" container spacing={2}>
            <Grid className="" item xs={6}>
              <h2 className="home-banner-title text-center">
                IM3 Prepaid Plan{" "}
              </h2>
            </Grid>
            <Grid className="" item xs={6}></Grid>
          </Grid>
        </Container> */}
      </Box>

      <Box className="prepaid-starter-banner " sx={{ flexGrow: 1 }}>
        <img className="" src="../images/happy52.png" alt="cupon" />
        {/* <Container className="h-100">
          <Grid className="h-100 align-items-end" container spacing={2}>
            <Grid className="" item xs={6}></Grid>
            <Grid className="" item xs={6}>
              <h2 className="home-banner-title text-center">
                IM3 Prepaid Plan{" "}
              </h2>
            </Grid>
          </Grid>
        </Container> */}
      </Box>
    </div>
  );
};

export default Index;
