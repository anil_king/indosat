import React from "react";
import {
  Box,
  Stepper,
  Step,
  StepLabel,
  StepContent,
  Button,
  Paper,
  styled,
  TextField,
  alpha,
  Radio,
  RadioGroup,
  FormControl,
  FormControlLabel,
  InputLabel,
  Select,
  MenuItem,
  Divider,
  Modal,
  Fade,
  Backdrop,
} from "@mui/material";
import { Row, Col } from "react-bootstrap";
import { useNavigate } from "react-router";
import { FaCheckCircle } from "react-icons/fa";
import StepConnector, {
  stepConnectorClasses,
} from "@mui/material/StepConnector";

const BpIcon = styled("span")(({ theme }) => ({
  borderRadius: "50%",
  width: 16,
  height: 16,
  boxShadow:
    theme.palette.mode === "dark"
      ? "0 0 0 1px rgb(16 22 26 / 40%)"
      : "inset 0 0 0 1px rgba(16,22,26,.2), inset 0 -1px 0 rgba(16,22,26,.1)",
  backgroundColor: theme.palette.mode === "dark" ? "#394b59" : "#f5f8fa",
  backgroundImage:
    theme.palette.mode === "dark"
      ? "linear-gradient(180deg,hsla(0,0%,100%,.05),hsla(0,0%,100%,0))"
      : "linear-gradient(180deg,hsla(0,0%,100%,.8),hsla(0,0%,100%,0))",
  ".Mui-focusVisible &": {
    outline: "2px auto #CB0171",
    outlineOffset: 2,
  },
  "input:hover ~ &": {
    backgroundColor: theme.palette.mode === "dark" ? "#30404d" : "#ebf1f5",
  },
  "input:disabled ~ &": {
    boxShadow: "none",
    background:
      theme.palette.mode === "dark"
        ? "rgba(57,75,89,.5)"
        : "rgba(206,217,224,.5)",
  },
}));

const BpCheckedIcon = styled(BpIcon)({
  backgroundColor: "#CB0171",
  backgroundImage:
    "linear-gradient(180deg,hsla(0,0%,100%,.1),hsla(0,0%,100%,0))",
  "&:before": {
    display: "block",
    width: 16,
    height: 16,
    backgroundImage: "radial-gradient(#fff,#fff 28%,transparent 32%)",
    content: '""',
  },
  "input:hover ~ &": {
    backgroundColor: "#CB0171",
  },
});

function BpRadio(props) {
  return (
    <Radio
      disableRipple
      color="default"
      checkedIcon={<BpCheckedIcon />}
      icon={<BpIcon />}
      {...props}
    />
  );
}

const RedditTextField = styled((props) => (
  <TextField InputProps={{ disableUnderline: true }} {...props} />
))(({ theme }) => ({
  "& .MuiFilledInput-root": {
    overflow: "hidden",
    borderRadius: 4,
    backgroundColor: theme.palette.mode === "light" ? "#F2F6F8" : "#F2F6F8",
    transition: theme.transitions.create([
      "border-color",
      "background-color",
      "box-shadow",
    ]),
    "&:hover": {
      backgroundColor: "#eeeeee",
    },
    "&.Mui-focused": {
      backgroundColor: "#eeeeee",
      boxShadow: `${alpha(theme.palette.primary.main, 0.25)} 0 0 0 2px`,
      borderColor: theme.palette.primary.main,
    },
  },
}));
const ValidationTextField = styled(TextField)({
  "& input:valid + fieldset": {
    borderColor: "green",
    borderWidth: 2,
  },
  "& input:invalid + fieldset": {
    borderColor: "red",
    borderWidth: 2,
  },
  "& input:valid:focus + fieldset": {
    borderLeftWidth: 6,
    padding: "4px !important", // override inline-style
  },
});




const StepForm = () => {
  const [activeStep, setActiveStep] = React.useState(0);
  const [stepForm, setStepForm] = React.useState({
    first_name:'John',
    last_name:'Deo',
    email:'johndeo@gmail.com',
    mobile_no:'968574',
    gender:'',
    flat_no:'121',
    area:'',
    landmark:'',
    city:'',
    postal_code:'',
    name_on_card:'',
    card_no:''
  });

  const handleChangeForm=(e)=>{
    setStepForm({...stepForm, [e.target.name]: e.target.value});
  }
  const steps = [
    {
      label: "Customer Details",
      description: (
        <Box>
          <Row>
            <Col md={6}>
              <RedditTextField
                fullWidth
                label="First name"
                name="first_name"
                value={stepForm.first_name}
                onChange={handleChangeForm}
                id="reddit-input"
                variant="filled"
                style={{ marginTop: 11 }}
              />
            </Col>
            <Col md={6}>
              <RedditTextField
                fullWidth
                label="Last name"
                name="last_name"
                value={stepForm.last_name}
                onChange={handleChangeForm}
                id="reddit-input"
                variant="filled"
                style={{ marginTop: 11 }}
              />
            </Col>
            <Col md={6}>
              <RedditTextField
                fullWidth
                label="Email Id"
                name="email"
                value={stepForm.email}
                onChange={handleChangeForm}
                id="reddit-input"
                variant="filled"
                style={{ marginTop: 11 }}
              />
            </Col>
            <Col md={6}>
              <RedditTextField
                fullWidth
                label="Mobile number"
                name="mobile_no"
                value={stepForm.mobile_no}
                onChange={handleChangeForm}
                id="reddit-input"
                variant="filled"
                style={{ marginTop: 11 }}
              />
            </Col>
            <Col md={12}>
              <FormControl>
                <p className="color-AFAFB1 mt-3 mb-2">Gender</p>
                <RadioGroup
                  row
                  defaultValue="Male"
                  aria-labelledby="demo-row-radio-buttons-group-label"
                  name="row-radio-buttons-group"
                >
                  <FormControlLabel
                    defaultChecked
                    value="Male"
                    control={<BpRadio />}
                    label="Male"
                  />
                  <FormControlLabel
                    value="Female"
                    control={<BpRadio />}
                    label="Female"
                  />
                  <FormControlLabel
                    value="Other"
                    control={<BpRadio />}
                    label="Other"
                  />
                  <FormControlLabel
                    value="Prefer Not to Answer"
                    control={<BpRadio />}
                    label="Prefer Not to Answer"
                  />
                </RadioGroup>
              </FormControl>
            </Col>
          </Row>
        </Box>
      ),
    },
    {
      label: "Address Details",
      description: (
        <Box>
          <Row>
            <Col md={8}>
              <RedditTextField
                fullWidth
                label="Flat no, house no, Building"
                name="flat_no"
                value={stepForm.flat_no}
                onChange={handleChangeForm}
                id="reddit-input"
                variant="filled"
                style={{ marginTop: 11 }}
              />
            </Col>
            <Col md={8}>
              <RedditTextField
                fullWidth
                label="Area,Street"
                name="area"
                value={stepForm.area}
                onChange={handleChangeForm}
                id="reddit-input"
                variant="filled"
                style={{ marginTop: 11 }}
              />
            </Col>
            <Col md={8}>
              <RedditTextField
                fullWidth
                label="Landmark"
                name="landmark"
                value={stepForm.landmark}
                onChange={handleChangeForm}
                id="reddit-input"
                variant="filled"
                style={{ marginTop: 11 }}
              />
            </Col>
            <Col md={8}>
              <RedditTextField
                fullWidth
                label="City"
                name="city"
                value={stepForm.city}
                onChange={handleChangeForm}
                id="reddit-input"
                variant="filled"
                style={{ marginTop: 11 }}
              />
            </Col>
            <Col md={8}>
              <RedditTextField
                fullWidth
                label="Postal code"
                name="postal_code"
                value={stepForm.postal_code}
                onChange={handleChangeForm}
                id="reddit-input"
                variant="filled"
                style={{ marginTop: 11 }}
              />
            </Col>
          </Row>
        </Box>
      ),
    },
    {
      label: "Choose your Number",
      description: (
        <Box>
          <RadioGroup
            defaultValue="val1"
            aria-labelledby="demo-row-radio-buttons-group-label"
            name="row-radio-buttons-group"
          >
            <Row>
              <Col md={8}>
                <Box className="">
                  <FormControlLabel
                    className="check-bg"
                    value="val1"
                    control={<BpRadio />}
                    label="47896321"
                  />
                </Box>
                <Box className="">
                  <FormControlLabel
                    className="check-bg"
                    value="val2"
                    control={<BpRadio />}
                    label="77896888"
                  />
                </Box>
                <Box className="">
                  <FormControlLabel
                    className="check-bg"
                    value="val3"
                    control={<BpRadio />}
                    label="77896888"
                  />
                </Box>
              </Col>
            </Row>
          </RadioGroup>
          <RadioGroup
            className="mt-2"
            row
            defaultValue="Physical"
            aria-labelledby="demo-row-radio-buttons-group-label"
            name="row-radio-buttons-group"
          >
            <FormControlLabel
              value="Physical"
              control={<BpRadio />}
              label="Physical"
            />
            <FormControlLabel
              value="E-Sim"
              control={<BpRadio />}
              label="E-Sim"
            />
          </RadioGroup>
        </Box>
      ),
    },
    {
      label: "Delivery Option",
      description: (
        <Box>
          <RadioGroup
            defaultValue="store"
            aria-labelledby="demo-row-radio-buttons-group-label"
            name="row-radio-buttons-group"
          >
            <Row>
              <Col md={10}>
                <div className="d-flex align-items-cneter">
                  <Box>
                    <FormControlLabel
                      className="check-bg"
                      value="store"
                      control={<BpRadio />}
                      label="Pick Up from Store"
                    />
                  </Box>
                  <button
                    className="link-btn"
                    onClick={() => setAnotherModal(true)}
                  >
                    Find nearest store
                  </button>
                </div>

                <Box>
                  <FormControlLabel
                    className="check-bg"
                    value="address"
                    control={<BpRadio />}
                    label="Deliver to Address"
                  />
                </Box>
              </Col>
            </Row>
          </RadioGroup>
          <Box>
            <small>
              Details of activation will be sent to your email address
            </small>
            <Col md={6}>
              <RedditTextField
                fullWidth
                label="Email Id"
                defaultValue="john@gmail.com"
                id="reddit-input"
                variant="filled"
                style={{ marginTop: 11 }}
              />
            </Col>
          </Box>
        </Box>
      ),
    },
    {
      label: "Payment Details",
      description: (
        <Box>
          <Col md={8}>
            <Box className="check-bg d-flex align-items-center justify-content-between py-3">
              <p className="mb-0">Amount</p>
              <p className="mb-0">Rp 60,000</p>
            </Box>
          </Col>
          <Col md={8}>
            <Box className="check-bg d-flex align-items-center justify-content-between py-3">
              <p className="mb-0 color-AFAFB1">Redeem Points</p>
              <a className="mb-0 color-71CE7B">Redeem</a>
            </Box>
          </Col>
          <Col md={8}>
            <Box className="check-bg d-flex align-items-center justify-content-between py-3">
              <p className="mb-0 color-AFAFB1">Coupon Code</p>
              <a className="mb-0 color-71CE7B">Apply</a>
            </Box>
          </Col>
          <Box className="d-flex">
            <img
              className="cards-icon"
              style={{ border: "1px solid rgb(255 5 5)" }}
              src="../images/visa.png"
            />
            <img className="cards-icon" src="../images/paypal.png" />
          </Box>
          <Box className="mt-3">
            <Col md={10}>
              <RedditTextField
                fullWidth
                label="Name on Card"
                name="name_on_card"
                value={stepForm.name_on_card}
                onChange={handleChangeForm}
                id="reddit-input"
                variant="filled"
                style={{ marginTop: 11 }}
              />
            </Col>
            <Col md={10}>
              <RedditTextField
                fullWidth
                label="Card number"
                name="card_no"
                value={stepForm.card_no}
                onChange={handleChangeForm}
                id="reddit-input"
                variant="filled"
                style={{ marginTop: 11 }}
              />
            </Col>
            <Col md={10}>
              <Row className="align-items-center mt-3">
                <Col md={6}>
                  <label>Expiry Date</label>
                </Col>
                <Col md={6}>
                  <Row>
                    <Col md={6}>
                      <FormControl fullWidth>
                        <Select
                          fullWidth
                          labelId="demo-simple-select-label"
                          id="demo-simple-select"
                        >
                          <MenuItem value={1}>1</MenuItem>
                          <MenuItem value={2}>2</MenuItem>
                          <MenuItem value={3}>3</MenuItem>
                          <MenuItem value={4}>4</MenuItem>
                          <MenuItem value={5}>5</MenuItem>
                          <MenuItem value={6}>6</MenuItem>
                          <MenuItem value={7}>7</MenuItem>
                          <MenuItem value={8}>8</MenuItem>
                          <MenuItem value={9}>9</MenuItem>
                          <MenuItem value={10}>10</MenuItem>
                          <MenuItem value={11}>11</MenuItem>
                          <MenuItem value={12}>12</MenuItem>
                        </Select>
                      </FormControl>
                    </Col>
                    <Col md={6}>
                      <FormControl fullWidth>
                        <Select
                          fullWidth
                          labelId="demo-simple-select-label"
                          id="demo-simple-select"
                        >
                          <MenuItem value={10}>2023</MenuItem>
                          <MenuItem value={10}>2024</MenuItem>
                          <MenuItem value={10}>2025</MenuItem>
                          <MenuItem value={10}>2026</MenuItem>
                          <MenuItem value={20}>2027</MenuItem>
                          <MenuItem value={30}>2028</MenuItem>
                          <MenuItem value={30}>2029</MenuItem>
                          <MenuItem value={30}>2030</MenuItem>
                          <MenuItem value={30}>2031</MenuItem>
                          <MenuItem value={30}>2032</MenuItem>
                          <MenuItem value={30}>2033</MenuItem>
                          <MenuItem value={30}>2034</MenuItem>
                        </Select>
                      </FormControl>
                    </Col>
                  </Row>
                </Col>
              </Row>
              <Row className="align-items-center mt-3">
                <Col md={6}>
                  <label>CVV</label>
                </Col>
                <Col md={6}>
                  <Row>
                    <Col md={6}>
                      <FormControl fullWidth>
                        <RedditTextField
                          fullWidth
                          defaultValue=""
                          id="reddit-input"
                          variant="filled"
                          style={{ marginTop: 11 }}
                        />
                      </FormControl>
                    </Col>
                    <Col md={6}></Col>
                  </Row>
                </Col>
              </Row>
            </Col>
          </Box>
        </Box>
      ),
    },
    {
      label: "Order Summary",
      description: (
        <Box>
          <Col md={8}>
            <Box className="check-bg phone-show d-flex align-items-center py-3 px-3">
              <img className="me-2" src="../images/phone-icon.png" alt="" />
              <Box className="phone-icon">
                <small>Mobile Prepaid</small>
                <h5 className="mb-0">Plan IOH- 250</h5>
              </Box>
            </Box>
            <Box className="mt-3">
              <h6>Shipping Address</h6>
              <p className="shipping-address">
                {`${stepForm.first_name} ${stepForm.last_name} ${stepForm.flat_no} ${stepForm.area} ${stepForm.landmark} ${stepForm.city} Postal code: ${stepForm.postal_code}`}
                {/* John Doe 47896321 Menara building,9-11, 7th Street, near nursery,
                Jakarta, Postal code: 12930 */}
              </p>
            </Box>
            <Divider />
            <Box className="mt-3">
              <h6>Payment Method</h6>
              <div className="d-flex align-items-center">
                <img
                  className="me-3 card-num"
                  src="../images/visa.png"
                  alt=""
                />
                <span>{`Ending with ****${stepForm.card_no.slice(
                  stepForm.card_no.length - 4,
                  stepForm.card_no.length
                )}`}</span>
              </div>
            </Box>
            <Box className="total-rp">
              <p className="mb-0">Total</p>
              <p className="mb-0">Rp 60,000</p>
            </Box>
          </Col>
        </Box>
      ),
    },
  ];

  const handleNext = (step) => {
    setActiveStep(step);
  };

  const handleBack = () => {
    setActiveStep((prevActiveStep) => prevActiveStep - 1);
  };

  const handleReset = () => {
    setActiveStep(0);
  };
  const navigate = useNavigate();
  const [open, setOpen] = React.useState(false);
  const [anotherModal, setAnotherModal] = React.useState(false);
  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);

  const QontoConnector = styled(StepConnector)(({ theme }) => ({
    [`&.${stepConnectorClasses.alternativeLabel}`]: {
      top: 10,
      left: "calc(-50% + 16px)",
      right: "calc(50% + 16px)",
    },
    [`&.${stepConnectorClasses.active}`]: {
      [`& .${stepConnectorClasses.line}`]: {
        borderColor: "#CB0171",
      },
    },
    [`&.${stepConnectorClasses.completed}`]: {
      [`& .${stepConnectorClasses.line}`]: {
        borderColor: "#CB0171",
      },
    },
    [`& .${stepConnectorClasses.line}`]: {
      borderColor:
        theme.palette.mode === "dark" ? theme.palette.grey[800] : "#eaeaf0",
      borderTopWidth: 3,
      borderRadius: 1,
    },
  }));
  const QontoStepIconRoot = styled("div")(({ theme, ownerState }) => ({
    color: theme.palette.mode === "dark" ? theme.palette.grey[700] : "#eaeaf0",
    display: "flex",
    height: 22,
    alignItems: "center",
    ...(ownerState.active && {
      color: "#784af4",
    }),
    "& .QontoStepIcon-completedIcon": {
      color: "#71CE7B",
      zIndex: 1,
      fontSize: 18,
    },
    "& .QontoStepIcon-circle": {
      width: 20,
      height: 20,
      borderRadius: "50%",
      backgroundColor: "#fff",
      border: "1px solid #979797",
    },
  }));
  function QontoStepIcon(props) {
    const { active, completed, className } = props;

    return (
      <QontoStepIconRoot ownerState={{ active }} className={className}>
        {completed ? (
          <FaCheckCircle className="QontoStepIcon-completedIcon" />
        ) : (
          <div className="QontoStepIcon-circle" />
        )}
      </QontoStepIconRoot>
    );
  }

  return (
    <div>
      <Box
        className="card-cs px-md-5 px-2 py-4 my-4 step-form-cs"
        sx={{ maxWidth: 700, margin: "auto" }}
      >
        <Stepper
          activeStep={activeStep}
          orientation="vertical"
          connector={<QontoConnector />}
        >
          {steps.map((step, index) => {
            console.log(step, "129861789267816");
            return (
              <Step key={step.label}>
                <StepLabel
                  StepIconComponent={QontoStepIcon}
                  onClick={() => handleNext(index)}
                >
                  {step.label}
                </StepLabel>

                <StepContent>
                  <p>{step.description}</p>
                  {index !== steps.length - 1 && (
                    <Box sx={{ mb: 2 }}>
                      <div>
                        {/* <Button
                          variant="contained"
                          onClick={() => handleNext(index + 1)}
                          sx={{ mt: 1, mr: 1 }}
                        >
                          Continue
                          {index === steps.length - 1 ? "Finish" : "Continue"}
                        </Button> */}
                        {/* <Button
                        disabled={index === 0}
                        onClick={handleBack}
                        sx={{ mt: 1, mr: 1 }}
                      >
                        Back
                      </Button> */}
                      </div>
                    </Box>
                  )}
                </StepContent>
              </Step>
            );
          })}
        </Stepper>
        {activeStep === steps.length && (
          <Paper square elevation={0} sx={{ p: 3 }}>
            <p>All steps completed - you&apos;re finished</p>
            <Button onClick={handleReset} sx={{ mt: 1, mr: 1 }}>
              Reset
            </Button>
          </Paper>
        )}
        <Box className="text-center mt-3">
          <button
            onClick={handleOpen}
            className=" btn-confirm"
            disabled={activeStep !== steps.length - 1}
          >
            Confirm
          </button>
        </Box>
      </Box>

      <Modal
        aria-labelledby="transition-modal-title"
        aria-describedby="transition-modal-description"
        open={open}
        onClose={handleClose}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 500,
        }}
      >
        <Fade in={open}>
          <Box className="congratulations-modal">
            <div className="get-paln-inner px-md-5">
              <div className="text-center p-md-4">
                <img className="tick-icon" src="../images/tick.svg" />
                <h5 className="modal-heading">Congratulations</h5>
                <div className="congrt-text my-3">
                <p className=" mb-1">
                    Your order has been placed successfully
                  </p>
                  <p className="color-AFAFB1 mb-1">
                    you will receive a confirmation email shortly with further
                    details
                  </p>
                  <p className="transaction-id">Transaction ID : 7876543456</p>
                </div>
                <Box className="text-center">
                  <button
                    onClick={() => navigate("/prepaid-plan")}
                    className="primary-btn"
                  >
                    OK
                  </button>
                </Box>
              </div>
            </div>
          </Box>
        </Fade>
      </Modal>
      <Modal
        aria-labelledby="transition-modal-title"
        aria-describedby="transition-modal-description"
        open={anotherModal}
        onClose={handleClose}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 500,
        }}
      >
        <Fade in={anotherModal}>
          <Box className="congratulations-modal">
            <div className="get-paln-inner">
              <div className="text-center p-md-4">
              <div className="mapouter">

              <div className="gmap_canvas">
              <iframe width="100%" height="510" id="gmap_canvas" src="https://maps.google.com/maps?q=california&t=&z=10&ie=UTF8&iwloc=&output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe>
              </div>
              </div>
             
                
                <Box className="text-center">
                  <button
                    onClick={() => setAnotherModal(false)}
                    className="primary-btn"
                  >
                    OK
                  </button>
                </Box>
              </div>
            </div>
          </Box>
        </Fade>
      </Modal>
      <img className="happy-img" src="../images/happy-img.png" alt="cupon" />
    </div>
  );
};

export default StepForm;
