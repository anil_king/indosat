import React from "react";
import './App.css';
import './style.css';
import Header from './Layouts/Header'
import { Routes, Route } from 'react-router';
import Index from "./Pages/Index";
import PrepaidPlan from "./Pages/PrepaidPlan";
import StepForm from "./Pages/StepForm";

function App() {
  return (
   <>
   <Header/>
    <Routes>
      <Route path="/index" element={<Index/>}/>
      <Route path="/prepaid-plan" element={<PrepaidPlan/>}/>
      <Route path="/step-form" element={<StepForm/>}/>
    </Routes>
   </>
  );
}

export default App;
