import React from "react";
import {
  Box,
  Stepper,
  Step,
  StepLabel,
  StepContent,
  Button,
  Paper,
  styled,
  TextField,
  alpha,
  Radio,
  RadioGroup,
  FormControl,
  FormControlLabel,
  InputLabel,
  Select,
  MenuItem,
  Divider,
} from "@mui/material";
import { Row, Col } from "react-bootstrap";

const RedditTextField = styled((props) => (
  <TextField InputProps={{ disableUnderline: true }} {...props} />
))(({ theme }) => ({
  "& .MuiFilledInput-root": {
    overflow: "hidden",
    borderRadius: 4,
    backgroundColor: theme.palette.mode === "light" ? "#F2F6F8" : "#F2F6F8",
    transition: theme.transitions.create([
      "border-color",
      "background-color",
      "box-shadow",
    ]),
    "&:hover": {
      backgroundColor: "#eeeeee",
    },
    "&.Mui-focused": {
      backgroundColor: "#eeeeee",
      boxShadow: `${alpha(theme.palette.primary.main, 0.25)} 0 0 0 2px`,
      borderColor: theme.palette.primary.main,
    },
  },
}));
const ValidationTextField = styled(TextField)({
  "& input:valid + fieldset": {
    borderColor: "green",
    borderWidth: 2,
  },
  "& input:invalid + fieldset": {
    borderColor: "red",
    borderWidth: 2,
  },
  "& input:valid:focus + fieldset": {
    borderLeftWidth: 6,
    padding: "4px !important", // override inline-style
  },
});
const steps = [
  {
    label: "Customer Details",
    description: (
      <Box>
        <Row>
          <Col md={6}>
            <RedditTextField
              fullWidth
              label="First name"
              defaultValue="John"
              id="reddit-input"
              variant="filled"
              style={{ marginTop: 11 }}
            />
          </Col>
          <Col md={6}>
            <RedditTextField
              fullWidth
              label="Last name"
              defaultValue="Doe"
              id="reddit-input"
              variant="filled"
              style={{ marginTop: 11 }}
            />
          </Col>
          <Col md={6}>
            <RedditTextField
              fullWidth
              label="Email Id"
              defaultValue="john@gmail.com"
              id="reddit-input"
              variant="filled"
              style={{ marginTop: 11 }}
            />
          </Col>
          <Col md={6}>
            <RedditTextField
              fullWidth
              label="Mobile number"
              defaultValue="47896321"
              id="reddit-input"
              variant="filled"
              style={{ marginTop: 11 }}
            />
          </Col>
          <Col md={12}>
            <FormControl>
              <p className="color-AFAFB1 mt-3 mb-2">Gender</p>
              <RadioGroup
                row
                aria-labelledby="demo-row-radio-buttons-group-label"
                name="row-radio-buttons-group"
              >
                <FormControlLabel
                  value="Male"
                  control={<Radio />}
                  label="Male"
                />
                <FormControlLabel
                  value="Female"
                  control={<Radio />}
                  label="Female"
                />
              </RadioGroup>
            </FormControl>
          </Col>
        </Row>
      </Box>
    ),
  },
  {
    label: "Address Details",
    description: (
      <Box>
        <Row>
          <Col md={8}>
            <RedditTextField
              fullWidth
              label="Flat no, house no, Building"
              defaultValue="Menara building,9-11"
              id="reddit-input"
              variant="filled"
              style={{ marginTop: 11 }}
            />
          </Col>
          <Col md={8}>
            <RedditTextField
              fullWidth
              label="Area,Street"
              defaultValue="7th street"
              id="reddit-input"
              variant="filled"
              style={{ marginTop: 11 }}
            />
          </Col>
          <Col md={8}>
            <RedditTextField
              fullWidth
              label="Landmark"
              defaultValue="near nursary"
              id="reddit-input"
              variant="filled"
              style={{ marginTop: 11 }}
            />
          </Col>
          <Col md={8}>
            <RedditTextField
              fullWidth
              label="City"
              defaultValue="Jakarta"
              id="reddit-input"
              variant="filled"
              style={{ marginTop: 11 }}
            />
          </Col>
          <Col md={8}>
            <RedditTextField
              fullWidth
              label="Postal code"
              defaultValue="96321"
              id="reddit-input"
              variant="filled"
              style={{ marginTop: 11 }}
            />
          </Col>
        </Row>
      </Box>
    ),
  },
  {
    label: "Number Selection",
    description: (
      <Box>
        <Row>
          <Col md={8}>
            <Box className="check-bg">
              <FormControlLabel
                value="Male"
                control={<Radio />}
                label="47896321"
              />
            </Box>
            <Box className="check-bg">
              <FormControlLabel
                value="Male"
                control={<Radio />}
                label="77896888"
              />
            </Box>
            <Box className="check-bg">
              <FormControlLabel
                value="Male"
                control={<Radio />}
                label="77896888"
              />
            </Box>
          </Col>
        </Row>
        <RadioGroup
          row
          aria-labelledby="demo-row-radio-buttons-group-label"
          name="row-radio-buttons-group"
        >
          <FormControlLabel value="Male" control={<Radio />} label="Male" />
          <FormControlLabel value="Female" control={<Radio />} label="Female" />
        </RadioGroup>
      </Box>
    ),
  },
  {
    label: "Delivery Option",
    description: (
      <Box>
        <Row>
          <Col md={8}>
            <Box className="check-bg">
              <FormControlLabel
                value=""
                control={<Radio />}
                label="Collect from store"
              />
            </Box>
            <Box className="check-bg">
              <FormControlLabel
                value=""
                control={<Radio />}
                label="Courier to address"
              />
            </Box>
          </Col>
        </Row>
        <small>Details of activation will be sent to your email address</small>
        <Col md={6}>
          <RedditTextField
            fullWidth
            label="Email Id"
            defaultValue="john@gmail.com"
            id="reddit-input"
            variant="filled"
            style={{ marginTop: 11 }}
          />
        </Col>
      </Box>
    ),
  },
  {
    label: "Payment Details",
    description: (
      <Box>
        <Col md={8}>
          <Box className="check-bg d-flex align-items-center justify-content-between py-3">
            <p className="mb-0">Amount</p>
            <p className="mb-0">Rp 100</p>
          </Box>
        </Col>
        <Col md={8}>
          <Box className="check-bg d-flex align-items-center justify-content-between py-3">
            <p className="mb-0 color-AFAFB1">Redeem Points</p>
            <a className="mb-0 color-71CE7B">Redeem</a>
          </Box>
        </Col>
        <Col md={8}>
          <Box className="check-bg d-flex align-items-center justify-content-between py-3">
            <p className="mb-0 color-AFAFB1">Coupon Code</p>
            <a className="mb-0 color-71CE7B">Apply</a>
          </Box>
        </Col>
        <Box className="d-flex">
          <img className="cards-icon" src="../images/visa.png" />
          <img className="cards-icon" src="../images/paypal.png" />
        </Box>
        <Box className="mt-3">
          <Col md={8}>
            <RedditTextField
              fullWidth
              label="Name on Card"
              defaultValue="John Doe"
              id="reddit-input"
              variant="filled"
              style={{ marginTop: 11 }}
            />
          </Col>
          <Col md={8}>
            <RedditTextField
              fullWidth
              label="Card number"
              defaultValue="345 879 76776"
              id="reddit-input"
              variant="filled"
              style={{ marginTop: 11 }}
            />
          </Col>
          <Col md={8}>
            <Row className="align-items-center mt-3">
              <Col md={6}>
                <label>Expiry Date</label>
              </Col>
              <Col md={6}>
                <Row>
                  <Col md={6}>
                    <FormControl fullWidth>
                      <Select
                        fullWidth
                        labelId="demo-simple-select-label"
                        id="demo-simple-select"
                      >
                        <MenuItem value={10}>10</MenuItem>
                        <MenuItem value={20}>11</MenuItem>
                        <MenuItem value={30}>12</MenuItem>
                      </Select>
                    </FormControl>
                  </Col>
                  <Col md={6}>
                    <FormControl fullWidth>
                      <Select
                        fullWidth
                        labelId="demo-simple-select-label"
                        id="demo-simple-select"
                      >
                        <MenuItem value={10}>2026</MenuItem>
                        <MenuItem value={20}>2027</MenuItem>
                        <MenuItem value={30}>2028</MenuItem>
                      </Select>
                    </FormControl>
                  </Col>
                </Row>
              </Col>
            </Row>
            <Row className="align-items-center mt-3">
              <Col md={6}>
                <label>CVV</label>
              </Col>
              <Col md={6}>
                <Row>
                  <Col md={6}>
                    <FormControl fullWidth>
                      <RedditTextField
                        fullWidth
                        defaultValue=""
                        id="reddit-input"
                        variant="filled"
                        style={{ marginTop: 11 }}
                      />
                    </FormControl>
                  </Col>
                  <Col md={6}></Col>
                </Row>
              </Col>
            </Row>
          </Col>
        </Box>
      </Box>
    ),
  },
  {
    label: "Order Sumary",
    description: (
      <Box>
        <Col md={8}>
          <Box className="check-bg phone-show d-flex align-items-center py-3 px-3">
            <img className="me-2" src="../images/phone-icon.png" alt="" />
            <Box className="phone-icon">
              <small>Mobile Prepaid</small>
              <h5 className="mb-0">Plan IOH- 250</h5>
            </Box>
          </Box>
          <Box className="mt-3">
            <h6>Shipping Address</h6>
            <p>
              John Doe 47896321 Menara building,9-11, 7th Street, near nursery,
              Jakarta, Postal code: 12930
            </p>
          </Box>
          <Divider />
          <Box className="mt-3">
            <h6>Payment Method</h6>
            <div className="d-flex align-items-center">
              <img className="me-3 card-num" src="../images/visa.png" alt="" />
              <span>Ending with ****6767</span>
            </div>
          </Box>
        </Col>
      </Box>
    ),
  },
];

const StepForm = () => {
  const [activeStep, setActiveStep] = React.useState(0);

  const handleNext = () => {
    setActiveStep((prevActiveStep) => prevActiveStep + 1);
  };

  const handleBack = () => {
    setActiveStep((prevActiveStep) => prevActiveStep - 1);
  };

  const handleReset = () => {
    setActiveStep(0);
  };
  return (
    <div>
      <Box
        className="card-cs px-md-5 px-2 py-4 my-4 "
        sx={{ maxWidth: 700, margin: "auto" }}
      >
        <Stepper activeStep={activeStep} orientation="vertical">
          {steps.map((step, index) => (
            <Step key={step.label}>
              <StepLabel>{step.label}</StepLabel>
              <StepContent>
                <p>{step.description}</p>
                <Box sx={{ mb: 2 }}>
                  <div>
                    <Button
                      variant="contained"
                      onClick={handleNext}
                      sx={{ mt: 1, mr: 1 }}
                    >
                      {index === steps.length - 1 ? "Finish" : "Continue"}
                    </Button>
                    <Button
                      disabled={index === 0}
                      onClick={handleBack}
                      sx={{ mt: 1, mr: 1 }}
                    >
                      Back
                    </Button>
                  </div>
                </Box>
              </StepContent>
            </Step>
          ))}
        </Stepper>
        {activeStep === steps.length && (
          <Paper square elevation={0} sx={{ p: 3 }}>
            <p>All steps completed - you&apos;re finished</p>
            <Button onClick={handleReset} sx={{ mt: 1, mr: 1 }}>
              Reset
            </Button>
          </Paper>
        )}
      </Box>
    </div>
  );
};

export default StepForm;
