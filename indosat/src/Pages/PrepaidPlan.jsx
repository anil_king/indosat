import React from "react";
import {
  Grid,
  Paper,
  Box,
  Modal,
  Fade,
  Backdrop,
  Radio,
  RadioGroup,
  FormControlLabel,
  FormControl,
  Stack,
  Button,
} from "@mui/material";
import { FiUpload, FiXCircle } from "react-icons/fi";
import { Container, Row, Col } from "react-bootstrap";
import { FaCheckCircle } from "react-icons/fa";

const PrepaidPlan = () => {
  const [open, setOpen] = React.useState(false);
  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);

  return (
    <div>
      <Box className="prepaid-plan-banner" sx={{ flexGrow: 1 }}>
        <Container className="h-100">
          <Grid className="h-100 align-items-center" container spacing={2}>
            <Grid className="" item xs={6}>
              <h2 className="home-banner-title text-center">
                IM3 Prepaid Plan{" "}
              </h2>
            </Grid>
            <Grid className="" item xs={6}></Grid>
          </Grid>
        </Container>
      </Box>
      <Box className="best-offer py-4">
        <Container>
          <h3 className="primary-heading">
            Our Best Offer (Pre - Select Plan)
          </h3>
          <h3 className="plan-text py-3">**Plan IOH- 250**</h3>
          <div className="plan-benefits">
            <img className="coupon" src="../images/cupon.png" alt="cupon" />
            <h3 className="secondary-heading text-center mb-5">
              Plan Benefits :-
            </h3>
            <Row className="mx-md-4">
              <Col md={3}>
                <h4 className="d-flex align-items-center">
                  <FaCheckCircle className="check-green me-3" />
                  Talk Time
                </h4>
              </Col>
              <Col md={3}>
                <h4 className="d-flex align-items-center">
                  <FaCheckCircle className="check-green me-3" />
                  Data
                </h4>
              </Col>
              <Col md={3}>
                <h4 className="d-flex align-items-center">
                  <FaCheckCircle className="check-green me-3" />
                  SMS
                </h4>
              </Col>
              <Col md={3}>
                <h4 className="d-flex align-items-center">
                  <FaCheckCircle className="check-green me-3" />
                  Free Offerings
                </h4>
              </Col>
            </Row>
          </div>
          <Box className="text-center mt-5">
            <button className="primary-btn" onClick={handleOpen}>
              Get Plan
            </button>
          </Box>
          <Box className="text-center mt-3">
            <a className="link-btn">More Plan</a>
          </Box>
        </Container>
      </Box>

      <Modal
        aria-labelledby="transition-modal-title"
        aria-describedby="transition-modal-description"
        open={open}
        onClose={handleClose}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 500,
        }}
      >
        <Fade in={open}>
          <Box className="get-plan-modal">
            <div className="get-paln-inner">
                <Box className="text-end">
                  <FiXCircle className="color-AFAFB1 cursor-pointer" />
                </Box>
              <div className="p-md-4">
                <h4 className="modal-heading">Upload KYC</h4>
                <div className="">
                  <FormControl>
                    <RadioGroup
                      row
                      aria-labelledby="demo-row-radio-buttons-group-label"
                      name="row-radio-buttons-group"
                    >
                      <FormControlLabel
                        value="Resident"
                        control={<Radio />}
                        label="Resident"
                      />
                      <FormControlLabel
                        value="Tourist"
                        control={<Radio />}
                        label="Tourist"
                      />
                    </RadioGroup>
                  </FormControl>
                  <Box className="upload-card p-3">
                    <div className="py-4 upload-text">
                      <p className="mb-0">Upload document</p>
                      <small>Supports png, jpeg, pdf (max size 2mb)</small>
                    </div>
                    <Stack direction="row" alignItems="center" spacing={2}>
                      <Box
                        className="upload-file"
                        color="primary"
                        aria-label="upload picture"
                        component="label"
                      >
                        <FiUpload className="color-f58a1f" />
                        <p className="m-0">Upload</p>
                        <input hidden accept="image/*" type="file" />
                      </Box>
                    </Stack>
                  </Box>
                </div>
                <Box className="text-center">
                  <button className="modal-btn">Submit</button>
                </Box>
              </div>
            </div>
          </Box>
        </Fade>
      </Modal>
    </div>
  );
};

export default PrepaidPlan;
