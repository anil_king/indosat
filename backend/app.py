from flask import Flask, request
from pymongo import MongoClient
import base64
from bson.objectid import ObjectId
from dotenv import load_dotenv
import os
from flask_cors import CORS

basedir = os.path.abspath(os.path.dirname(__file__))

app = Flask(__name__)
CORS(app)
load_dotenv(os.path.join(basedir, ".env"))

host = os.environ.get("host")
port = int(os.environ.get("port"))
username = os.environ.get("username")
password = os.environ.get("password")
database_name = os.environ.get("database_name")
collection_name = os.environ.get("collection_name")

client = MongoClient(host, port, username=username, password=password)
db = client.get_default_database(database_name)
collection = db.get_collection(collection_name)

ALLOWED_EXTENSIONS = set(["png", "jpg", "jpeg", "pdf"])


def allowed_file(filename):
    return "." in filename and filename.rsplit(".", 1)[1].lower() in ALLOWED_EXTENSIONS


@app.route("/api/add", methods=["POST"])
def add_user():
    resp = {}
    file = request.files.get("file")
    if file:
        if file and allowed_file(file.filename):
            file_data = file.read()
            file_data_base64 = base64.b64encode(file_data)

            object = collection.insert_one(
                {
                    "document": {
                        "file_name": file.filename,
                        "file_data_base64": file_data_base64,
                    }
                }
            )
            resp["message"] = "Document uploaded successfully!"
            status_code = 201
            resp["id"] = str(object.inserted_id)
            return {"data": resp}, status_code
        else:
            resp["message"] = "Invalid file Name"
            status_code = 400

    return resp, status_code


@app.route("/api/update-data/<id>/", methods=["PUT"])
def update_user(id):
    resp = {}
    request_data = request.json
    try:
        data = collection.find_one_and_update(
            {"_id": ObjectId(id)},
            {"$set": request_data},
        )
        if data:
            resp["message"] = "User details added successfully!"
            data["_id"] = str(ObjectId(id))
            data["document"]["file_data_base64"] = data["document"][
                "file_data_base64"
            ].decode()
            resp["data"] = data
            status_code = 200
        else:
            resp["message"] = "Invalid ID"
            status_code = 400

    except Exception as e:
        resp["message"] = str(e)
        status_code = 400
    return resp, status_code


if __name__ == "__main__":
    app.run(debug=True, port=12000)
