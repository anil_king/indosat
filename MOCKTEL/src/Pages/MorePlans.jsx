import React from "react";
import {
  Grid,
  Box,
  Typography,
  Link,
  Paper,
  Button,
  FormControlLabel,
  FormControl,
  Breadcrumbs,
} from "@mui/material";
import { useNavigate } from "react-router";
import { Container, Row, Col } from "react-bootstrap";
import NavigateNextIcon from "@mui/icons-material/NavigateNext";


function handleClick(event) {
  event.preventDefault();
  console.info("You clicked a breadcrumb.");
}
const MorePlans = () => {
  const navigate =useNavigate()
  const breadcrumbs = [
    <Link
      underline="hover"
      key="1"
      color="inherit"
      onClick={()=>navigate('/')}
    >
      Home
    </Link>,
    <Typography key="2" color="#FFCB05">
      More Plans
    </Typography>,
  ];
  return (
    <div>
    
      {/* <Box className="prepaid-plan-banner" sx={{ flexGrow: 1 }}>
        <Container className="h-100">
          <Grid className="h-100 align-items-center" container spacing={2}>
            <Grid className="" item xs={6}>
              
            </Grid>
            <Grid className="" item xs={6}></Grid>
          </Grid>
        </Container>
      </Box> */}
      <Container>
        <Breadcrumbs
          className="mt-4"
          separator={<NavigateNextIcon fontSize="small" />}
          aria-label="breadcrumb"
        >
          {breadcrumbs}
        </Breadcrumbs>
        <Box className="mt-3">
          <div className="d-flex" style={{overflow: 'auto'}}>
            {/* <div className="plan-card me-3">
              <div className="product_card-body flex-body">
                <div className="benefit mb-4">
                  <h4 className="upper-title">Freedom Internet 100GB</h4>
                   <div className="price mb-3 fixed-h">
                  <h3 className="binding-price">Rp 16.500</h3>
                </div>
                  <div
                    className="d-flex align-items-start"
                    show="displayitems.validity"
                  >
                    <picture>
                      <img
                        src="https://im3-img.indosatooredoo.com/indosatassets/images/personal/periode.svg"
                        alt="..."
                        title=".."
                      />
                    </picture>
                    <p className="ml-2 mb-0">
                      Validity <span className="binding">30 Days </span>
                    </p>
                  </div>

              
                  <div
                    className="d-flex align-items-center hide"
                    show="pack.sms!=null    &amp;&amp; displayitems.sms"
                  >
                    <picture>
                      <img
                        src="https://im3-img.indosatooredoo.com/indosatassets/images/personal/sms.svg"
                        alt="..."
                        title=".."
                      />
                    </picture>
                    <p className="ml-2 mb-0">
                      SMS <span bind="pack.sms" className="binding"></span>{" "}
                    </p>
                  </div>
                  <div
                    className="d-flex align-items-center hide"
                    show="pack.calltoim3!=null    &amp;&amp; displayitems.calltoim3"
                  >
                    <picture>
                      <img
                        src="https://im3-img.indosatooredoo.com/indosatassets/images/personal/call.svg"
                        alt="..."
                        title=".."
                      />
                    </picture>
                    <p className="ml-2 mb-0">
                      <span className="binding"> </span> Call to IM3 Ooredoo{" "}
                    </p>
                  </div>
                  <div
                    className="d-flex align-items-center hide"
                    show="pack.smstoim3!=null  &amp;&amp; displayitems.smstoim3"
                  >
                    <picture>
                      <img
                        src="https://im3-img.indosatooredoo.com/indosatassets/images/personal/sms.svg"
                        alt="..."
                        title=".."
                      />
                    </picture>
                    <p className="ml-2 mb-0">
                      <span className="binding"> </span> SMS to IM3 Ooredoo{" "}
                    </p>
                  </div>
                  <div
                    className="d-flex align-items-center hide"
                    show="pack.appquota!=null  &amp;&amp; displayitems.appquota"
                  >
                    <picture>
                      <img
                        src="https://im3-img.indosatooredoo.com/indosatassets/images/personal/quota-app.svg"
                        alt="..."
                        title=".."
                      />
                    </picture>
                    <p className="ml-2 mb-0">
                      App quota <span className="binding"></span>{" "}
                    </p>
                  </div>
                  <div
                    className="d-flex align-items-center hide"
                    show="pack.streamquota!=null  &amp;&amp; displayitems.streamquota"
                  >
                    <picture>
                      <img
                        src="https://im3-img.indosatooredoo.com/indosatassets/images/personal/streaming-app.svg"
                        alt="..."
                        title=".."
                      />
                    </picture>
                    <p className="ml-2 mb-0">
                      $$Streaming quota$$ <span className="binding"></span>{" "}
                    </p>
                  </div>
                  <div
                    className="d-flex align-items-center hide"
                    show="pack.speedbooster!=null    &amp;&amp; displayitems.speedbooster"
                  >
                    <picture>
                      <img
                        src="https://im3-img.indosatooredoo.com/indosatassets/images/personal/speed-booster.svg"
                        alt="..."
                        title=".."
                      />
                    </picture>
                    <p className="ml-2 mb-0">
                      Speed booster <span className="binding"></span>{" "}
                    </p>
                  </div>
                  <div
                    className="d-flex align-items-center hide"
                    show="pack.impoinbonus !=null    &amp;&amp; displayitems.impoinbonus "
                  >
                    <picture>
                      <img
                        src="https://im3-img.indosatooredoo.com/indosatassets/images/personal/impoin.svg"
                        alt="..."
                        title=".."
                      />
                    </picture>
                    <p className="ml-2 mb-0">
                      <span className="binding"> point</span> Bonus IMPoin
                    </p>
                  </div>
                  <div
                    className="d-flex align-items-center hide"
                    show="pack.impoinreward !=null   &amp;&amp; displayitems.impoinreward  "
                  >
                    <picture>
                      <img
                        src="https://im3-img.indosatooredoo.com/indosatassets/images/personal/impoin.svg"
                        alt="..."
                        title=".."
                      />
                    </picture>
                    <p className="ml-2 mb-0">
                      <span className="binding"> point </span> rewards IMPoin{" "}
                    </p>
                  </div>

                  <div
                    className="d-flex align-items-start scope"
                    show="displayitems.cbenfit "
                    repeat="cbenfit in pack.customitems"
                  >
                    <picture>
                      <img
                        src="https://im3-img.indosatooredoo.com/indosatassets/images/personal/info.svg"
                        alt="..."
                        title=".."
                      />
                    </picture>
                    <p className="ml-2 mb-0 binding">
                      Available at *123*111#, myIM3 apps, IM3 Official WhatsApp,
                      e-Commerce and outlets <span className="binding"> </span>
                    </p>
                  </div>

                  <div
                    className="d-flex align-items-start scope"
                    show="displayitems.cbenfit "
                    repeat="cbenfit in pack.customitems"
                  >
                    <picture>
                      <img
                        src="https://im3-img.indosatooredoo.com/indosatassets/images/personal/info.svg"
                        alt="..."
                        title=".."
                      />
                    </picture>
                    <p className="ml-2 mb-0 binding">
                      Call to IM3 and Tri{" "}
                      <span className="binding">Limitless (5,000 min) </span>
                    </p>
                  </div>

                  <div
                    className="d-flex align-items-center hide"
                    show="pack.localquota!=null  &amp;&amp; displayitems.localquota"
                  >
                    <picture>
                      <img
                        src="https://im3-img.indosatooredoo.com/indosatassets/images/personal/quota.svg"
                        alt="..."
                        title=".."
                      />
                    </picture>
                    <p className="ml-2 mb-0">
                      Local quota <span className="binding"> </span>
                    </p>
                  </div>

                  <div
                    className="d-flex align-items-start hide"
                    show="pack.midnightquota!=null &amp;&amp; displayitems.midnightquota"
                  >
                    <picture>
                      <img
                        src="https://im3-img.indosatooredoo.com/indosatassets/images/personal/quota.svg"
                        alt="..."
                        title=".."
                      />
                    </picture>
                    <p className="ml-2 mb-0">
                      Night quota <span className="binding"> </span>
                    </p>
                  </div>

                  <div
                    className="d-flex align-items-center hide"
                    show="pack.midnightquota!=null  &amp;&amp; displayitems.extramidnightquota"
                  >
                    <picture>
                      <img
                        src="https://im3-img.indosatooredoo.com/indosatassets/images/personal/quota.svg"
                        alt="..."
                        title=".."
                      />
                    </picture>
                    <p className="ml-2 mb-0">
                      Extra night quota <span className="binding"> </span>
                    </p>
                  </div>
                </div>

                <div className="btn-link-cont d-flex justify-content-between align-items-center">
                 <button className="primary-btn px-4">CHOOSE</button>
                 <h6 className="mb-0">SEE DETAILS</h6>
                </div>
              </div>
            </div>

            <div className="plan-card me-3">
              <div className="product_card-body flex-body">
                <div className="benefit mb-4">
                  <h4 className="upper-title">Freedom Internet 100GB</h4>
                   <div className="price mb-3 fixed-h">
                  <h3 className="binding-price">Rp 16.500</h3>
                </div>
                  <div
                    className="d-flex align-items-start"
                    show="displayitems.validity"
                  >
                    <picture>
                      <img
                        src="https://im3-img.indosatooredoo.com/indosatassets/images/personal/periode.svg"
                        alt="..."
                        title=".."
                      />
                    </picture>
                    <p className="ml-2 mb-0">
                      Validity <span className="binding">30 Days </span>
                    </p>
                  </div>

                  <div
                    className="d-flex align-items-center hide"
                    show="pack.mainquota!=null &amp;&amp; displayitems.dailyquota"
                  >
                    <picture>
                      <img
                        src="https://im3-img.indosatooredoo.com/indosatassets/images/personal/quota.svg"
                        alt="..."
                        title=".."
                      />
                    </picture>
                    <p className="ml-2 mb-0">
                      Daily quota <span className="binding"> 2 GB / Day</span>
                    </p>
                  </div>

                  <div
                    className="d-flex align-items-center"
                    show="pack.mainquota!=null &amp;&amp; displayitems.mainquota"
                  >
                    <picture>
                      <img
                        src="https://im3-img.indosatooredoo.com/indosatassets/images/personal/quota.svg"
                        alt="..."
                        title=".."
                      />
                    </picture>
                    <p className="ml-2 mb-0">
                      Main quota <span className="binding">2 GB</span>
                    </p>
                  </div>
                  <div
                    className="d-flex align-items-center"
                    show="displayitems.pulsasafe"
                  >
                    <picture>
                      <img
                        src="https://im3-img.indosatooredoo.com/indosatassets/images/personal/pulsa-safe.svg"
                        alt="..."
                        title=".."
                      />
                    </picture>
                    <p className="ml-2 mb-0">Pulsa Safe</p>
                  </div>
                  <div
                    className="d-flex align-items-center hide"
                    show="pack.call!=null &amp;&amp; displayitems.call"
                  >
                    <picture>
                      <img
                        src="https://im3-img.indosatooredoo.com/indosatassets/images/personal/call.svg"
                        alt="..."
                        title=".."
                      />
                    </picture>
                    <p className="ml-2 mb-0">
                      Call <span className="binding"> Minutes</span>{" "}
                    </p>
                  </div>

                  <div
                    className="d-flex align-items-center hide"
                    show="pack.calltoindosat!=null &amp;&amp; displayitems.calltoindosat"
                  >
                    <picture>
                      <img
                        src="https://im3-img.indosatooredoo.com/indosatassets/images/personal/call.svg"
                        alt="..."
                        title=".."
                      />
                    </picture>
                    <p className="ml-2 mb-0">
                      Call to Indosat <span className="binding"> </span>{" "}
                    </p>
                  </div>

                  <div
                    className="d-flex align-items-center hide"
                    show="pack.calltoother!=null &amp;&amp; displayitems.calltoother"
                  >
                    <picture>
                      <img
                        src="https://im3-img.indosatooredoo.com/indosatassets/images/personal/call.svg"
                        alt="..."
                        title=".."
                      />
                    </picture>
                    <p className="ml-2 mb-0">
                      Call to Others <span className="binding"> </span>{" "}
                    </p>
                  </div>

                  <div
                    className="d-flex align-items-center hide"
                    show="pack.streamquota!=null  &amp;&amp; displayitems.streamquota"
                  >
                    <picture>
                      <img
                        src="https://im3-img.indosatooredoo.com/indosatassets/images/personal/streaming-app.svg"
                        alt="..."
                        title=".."
                      />
                    </picture>
                    <p className="ml-2 mb-0">
                      $$Streaming quota$$ <span className="binding"></span>{" "}
                    </p>
                  </div>
                  <div
                    className="d-flex align-items-center hide"
                    show="pack.speedbooster!=null    &amp;&amp; displayitems.speedbooster"
                  >
                    <picture>
                      <img
                        src="https://im3-img.indosatooredoo.com/indosatassets/images/personal/speed-booster.svg"
                        alt="..."
                        title=".."
                      />
                    </picture>
                    <p className="ml-2 mb-0">
                      Speed booster <span className="binding"></span>{" "}
                    </p>
                  </div>
                  <div
                    className="d-flex align-items-center hide"
                    show="pack.impoinbonus !=null    &amp;&amp; displayitems.impoinbonus "
                  >
                    <picture>
                      <img
                        src="https://im3-img.indosatooredoo.com/indosatassets/images/personal/impoin.svg"
                        alt="..."
                        title=".."
                      />
                    </picture>
                    <p className="ml-2 mb-0">
                      <span className="binding"> point</span> Bonus IMPoin
                    </p>
                  </div>
                  <div
                    className="d-flex align-items-center hide"
                    show="pack.impoinreward !=null   &amp;&amp; displayitems.impoinreward  "
                  >
                    <picture>
                      <img
                        src="https://im3-img.indosatooredoo.com/indosatassets/images/personal/impoin.svg"
                        alt="..."
                        title=".."
                      />
                    </picture>
                    <p className="ml-2 mb-0">
                      <span className="binding"> point </span> rewards IMPoin{" "}
                    </p>
                  </div>

                  <div
                    className="d-flex align-items-start scope"
                    show="displayitems.cbenfit "
                    repeat="cbenfit in pack.customitems"
                  >
                    <picture>
                      <img
                        src="https://im3-img.indosatooredoo.com/indosatassets/images/personal/info.svg"
                        alt="..."
                        title=".."
                      />
                    </picture>
                    <p className="ml-2 mb-0 binding">
                      Available at *123*111#, myIM3 apps, IM3 Official WhatsApp,
                      e-Commerce and outlets <span className="binding"> </span>
                    </p>
                  </div>

                  <div
                    className="d-flex align-items-start scope"
                    show="displayitems.cbenfit "
                    repeat="cbenfit in pack.customitems"
                  >
                    <picture>
                      <img
                        src="https://im3-img.indosatooredoo.com/indosatassets/images/personal/info.svg"
                        alt="..."
                        title=".."
                      />
                    </picture>
                    <p className="ml-2 mb-0 binding">
                      Call to IM3 and Tri{" "}
                      <span className="binding">Limitless (5,000 min) </span>
                    </p>
                  </div>

                  <div
                    className="d-flex align-items-center hide"
                    show="pack.localquota!=null  &amp;&amp; displayitems.localquota"
                  >
                    <picture>
                      <img
                        src="https://im3-img.indosatooredoo.com/indosatassets/images/personal/quota.svg"
                        alt="..."
                        title=".."
                      />
                    </picture>
                    <p className="ml-2 mb-0">
                      Local quota <span className="binding"> </span>
                    </p>
                  </div>

                  <div
                    className="d-flex align-items-start hide"
                    show="pack.midnightquota!=null &amp;&amp; displayitems.midnightquota"
                  >
                    <picture>
                      <img
                        src="https://im3-img.indosatooredoo.com/indosatassets/images/personal/quota.svg"
                        alt="..."
                        title=".."
                      />
                    </picture>
                    <p className="ml-2 mb-0">
                      Night quota <span className="binding"> </span>
                    </p>
                  </div>

                  <div
                    className="d-flex align-items-center hide"
                    show="pack.midnightquota!=null  &amp;&amp; displayitems.extramidnightquota"
                  >
                    <picture>
                      <img
                        src="https://im3-img.indosatooredoo.com/indosatassets/images/personal/quota.svg"
                        alt="..."
                        title=".."
                      />
                    </picture>
                    <p className="ml-2 mb-0">
                      Extra night quota <span className="binding"> </span>
                    </p>
                  </div>
                </div>

                <div className="btn-link-cont d-flex justify-content-between align-items-center">
                 <button className="primary-btn px-4">CHOOSE</button>
                 <h6 className="mb-0">SEE DETAILS</h6>
                </div>
              </div>
            </div>
            <div className="plan-card me-3">
              <div className="product_card-body flex-body">
                <div className="benefit mb-4">
                  <h4 className="upper-title">Freedom Internet 100GB</h4>
                   <div className="price mb-3 fixed-h">
                  <h3 className="binding-price">Rp 16.500</h3>
                </div>
                  <div
                    className="d-flex align-items-start"
                    show="displayitems.validity"
                  >
                    <picture>
                      <img
                        src="https://im3-img.indosatooredoo.com/indosatassets/images/personal/periode.svg"
                        alt="..."
                        title=".."
                      />
                    </picture>
                    <p className="ml-2 mb-0">
                      Validity <span className="binding">30 Days </span>
                    </p>
                  </div>

                  <div
                    className="d-flex align-items-center hide"
                    show="pack.mainquota!=null &amp;&amp; displayitems.dailyquota"
                  >
                    <picture>
                      <img
                        src="https://im3-img.indosatooredoo.com/indosatassets/images/personal/quota.svg"
                        alt="..."
                        title=".."
                      />
                    </picture>
                    <p className="ml-2 mb-0">
                      Daily quota <span className="binding"> 2 GB / Day</span>
                    </p>
                  </div>

                  <div
                    className="d-flex align-items-center"
                    show="pack.mainquota!=null &amp;&amp; displayitems.mainquota"
                  >
                    <picture>
                      <img
                        src="https://im3-img.indosatooredoo.com/indosatassets/images/personal/quota.svg"
                        alt="..."
                        title=".."
                      />
                    </picture>
                    <p className="ml-2 mb-0">
                      Main quota <span className="binding">2 GB</span>
                    </p>
                  </div>
                  <div
                    className="d-flex align-items-center"
                    show="displayitems.pulsasafe"
                  >
                    <picture>
                      <img
                        src="https://im3-img.indosatooredoo.com/indosatassets/images/personal/pulsa-safe.svg"
                        alt="..."
                        title=".."
                      />
                    </picture>
                    <p className="ml-2 mb-0">Pulsa Safe</p>
                  </div>
                  <div
                    className="d-flex align-items-center hide"
                    show="pack.call!=null &amp;&amp; displayitems.call"
                  >
                    <picture>
                      <img
                        src="https://im3-img.indosatooredoo.com/indosatassets/images/personal/call.svg"
                        alt="..."
                        title=".."
                      />
                    </picture>
                    <p className="ml-2 mb-0">
                      Call <span className="binding"> Minutes</span>{" "}
                    </p>
                  </div>

                  <div
                    className="d-flex align-items-center hide"
                    show="pack.calltoindosat!=null &amp;&amp; displayitems.calltoindosat"
                  >
                    <picture>
                      <img
                        src="https://im3-img.indosatooredoo.com/indosatassets/images/personal/call.svg"
                        alt="..."
                        title=".."
                      />
                    </picture>
                    <p className="ml-2 mb-0">
                      Call to Indosat <span className="binding"> </span>{" "}
                    </p>
                  </div>

                  <div
                    className="d-flex align-items-center hide"
                    show="pack.calltoother!=null &amp;&amp; displayitems.calltoother"
                  >
                    <picture>
                      <img
                        src="https://im3-img.indosatooredoo.com/indosatassets/images/personal/call.svg"
                        alt="..."
                        title=".."
                      />
                    </picture>
                    <p className="ml-2 mb-0">
                      Call to Others <span className="binding"> </span>{" "}
                    </p>
                  </div>

                  <div
                    className="d-flex align-items-center hide"
                    show="pack.sms!=null    &amp;&amp; displayitems.sms"
                  >
                    <picture>
                      <img
                        src="https://im3-img.indosatooredoo.com/indosatassets/images/personal/sms.svg"
                        alt="..."
                        title=".."
                      />
                    </picture>
                    <p className="ml-2 mb-0">
                      SMS <span bind="pack.sms" className="binding"></span>{" "}
                    </p>
                  </div>
                  <div
                    className="d-flex align-items-center hide"
                    show="pack.calltoim3!=null    &amp;&amp; displayitems.calltoim3"
                  >
                    <picture>
                      <img
                        src="https://im3-img.indosatooredoo.com/indosatassets/images/personal/call.svg"
                        alt="..."
                        title=".."
                      />
                    </picture>
                    <p className="ml-2 mb-0">
                      <span className="binding"> </span> Call to IM3 Ooredoo{" "}
                    </p>
                  </div>
                  <div
                    className="d-flex align-items-center hide"
                    show="pack.smstoim3!=null  &amp;&amp; displayitems.smstoim3"
                  >
                    <picture>
                      <img
                        src="https://im3-img.indosatooredoo.com/indosatassets/images/personal/sms.svg"
                        alt="..."
                        title=".."
                      />
                    </picture>
                    <p className="ml-2 mb-0">
                      <span className="binding"> </span> SMS to IM3 Ooredoo{" "}
                    </p>
                  </div>
                  <div
                    className="d-flex align-items-center hide"
                    show="pack.appquota!=null  &amp;&amp; displayitems.appquota"
                  >
                    <picture>
                      <img
                        src="https://im3-img.indosatooredoo.com/indosatassets/images/personal/quota-app.svg"
                        alt="..."
                        title=".."
                      />
                    </picture>
                    <p className="ml-2 mb-0">
                      App quota <span className="binding"></span>{" "}
                    </p>
                  </div>
                  <div
                    className="d-flex align-items-center hide"
                    show="pack.streamquota!=null  &amp;&amp; displayitems.streamquota"
                  >
                    <picture>
                      <img
                        src="https://im3-img.indosatooredoo.com/indosatassets/images/personal/streaming-app.svg"
                        alt="..."
                        title=".."
                      />
                    </picture>
                    <p className="ml-2 mb-0">
                      $$Streaming quota$$ <span className="binding"></span>{" "}
                    </p>
                  </div>
                  <div
                    className="d-flex align-items-center hide"
                    show="pack.speedbooster!=null    &amp;&amp; displayitems.speedbooster"
                  >
                    <picture>
                      <img
                        src="https://im3-img.indosatooredoo.com/indosatassets/images/personal/speed-booster.svg"
                        alt="..."
                        title=".."
                      />
                    </picture>
                    <p className="ml-2 mb-0">
                      Speed booster <span className="binding"></span>{" "}
                    </p>
                  </div>
                  <div
                    className="d-flex align-items-center hide"
                    show="pack.impoinbonus !=null    &amp;&amp; displayitems.impoinbonus "
                  >
                    <picture>
                      <img
                        src="https://im3-img.indosatooredoo.com/indosatassets/images/personal/impoin.svg"
                        alt="..."
                        title=".."
                      />
                    </picture>
                    <p className="ml-2 mb-0">
                      <span className="binding"> point</span> Bonus IMPoin
                    </p>
                  </div>
                  <div
                    className="d-flex align-items-center hide"
                    show="pack.impoinreward !=null   &amp;&amp; displayitems.impoinreward  "
                  >
                    <picture>
                      <img
                        src="https://im3-img.indosatooredoo.com/indosatassets/images/personal/impoin.svg"
                        alt="..."
                        title=".."
                      />
                    </picture>
                    <p className="ml-2 mb-0">
                      <span className="binding"> point </span> rewards IMPoin{" "}
                    </p>
                  </div>

                  <div
                    className="d-flex align-items-start scope"
                    show="displayitems.cbenfit "
                    repeat="cbenfit in pack.customitems"
                  >
                    <picture>
                      <img
                        src="https://im3-img.indosatooredoo.com/indosatassets/images/personal/info.svg"
                        alt="..."
                        title=".."
                      />
                    </picture>
                    <p className="ml-2 mb-0 binding">
                      Available at *123*111#, myIM3 apps, IM3 Official WhatsApp,
                      e-Commerce and outlets <span className="binding"> </span>
                    </p>
                  </div>

                  <div
                    className="d-flex align-items-start scope"
                    show="displayitems.cbenfit "
                    repeat="cbenfit in pack.customitems"
                  >
                    <picture>
                      <img
                        src="https://im3-img.indosatooredoo.com/indosatassets/images/personal/info.svg"
                        alt="..."
                        title=".."
                      />
                    </picture>
                    <p className="ml-2 mb-0 binding">
                      Call to IM3 and Tri{" "}
                      <span className="binding">Limitless (5,000 min) </span>
                    </p>
                  </div>

                  <div
                    className="d-flex align-items-center hide"
                    show="pack.localquota!=null  &amp;&amp; displayitems.localquota"
                  >
                    <picture>
                      <img
                        src="https://im3-img.indosatooredoo.com/indosatassets/images/personal/quota.svg"
                        alt="..."
                        title=".."
                      />
                    </picture>
                    <p className="ml-2 mb-0">
                      Local quota <span className="binding"> </span>
                    </p>
                  </div>

                  <div
                    className="d-flex align-items-start hide"
                    show="pack.midnightquota!=null &amp;&amp; displayitems.midnightquota"
                  >
                    <picture>
                      <img
                        src="https://im3-img.indosatooredoo.com/indosatassets/images/personal/quota.svg"
                        alt="..."
                        title=".."
                      />
                    </picture>
                    <p className="ml-2 mb-0">
                      Night quota <span className="binding"> </span>
                    </p>
                  </div>

                  <div
                    className="d-flex align-items-center hide"
                    show="pack.midnightquota!=null  &amp;&amp; displayitems.extramidnightquota"
                  >
                    <picture>
                      <img
                        src="https://im3-img.indosatooredoo.com/indosatassets/images/personal/quota.svg"
                        alt="..."
                        title=".."
                      />
                    </picture>
                    <p className="ml-2 mb-0">
                      Extra night quota <span className="binding"> </span>
                    </p>
                  </div>
                </div>

                <div className="btn-link-cont d-flex justify-content-between align-items-center">
                 <button className="primary-btn px-4">CHOOSE</button>
                 <h6 className="mb-0">SEE DETAILS</h6>
                </div>
              </div>
            </div>
            <div className="plan-card me-3">
              <div className="product_card-body flex-body">
                <div className="benefit mb-4">
                  <h4 className="upper-title">Freedom Internet 100GB</h4>
                   <div className="price mb-3 fixed-h">
                  <h3 className="binding-price">Rp 16.500</h3>
                </div>
                  <div
                    className="d-flex align-items-start"
                    show="displayitems.validity"
                  >
                    <picture>
                      <img
                        src="https://im3-img.indosatooredoo.com/indosatassets/images/personal/periode.svg"
                        alt="..."
                        title=".."
                      />
                    </picture>
                    <p className="ml-2 mb-0">
                      Validity <span className="binding">30 Days </span>
                    </p>
                  </div>

                  <div
                    className="d-flex align-items-center hide"
                    show="pack.mainquota!=null &amp;&amp; displayitems.dailyquota"
                  >
                    <picture>
                      <img
                        src="https://im3-img.indosatooredoo.com/indosatassets/images/personal/quota.svg"
                        alt="..."
                        title=".."
                      />
                    </picture>
                    <p className="ml-2 mb-0">
                      Daily quota <span className="binding"> 2 GB / Day</span>
                    </p>
                  </div>

                  <div
                    className="d-flex align-items-center"
                    show="pack.mainquota!=null &amp;&amp; displayitems.mainquota"
                  >
                    <picture>
                      <img
                        src="https://im3-img.indosatooredoo.com/indosatassets/images/personal/quota.svg"
                        alt="..."
                        title=".."
                      />
                    </picture>
                    <p className="ml-2 mb-0">
                      Main quota <span className="binding">2 GB</span>
                    </p>
                  </div>
                  <div
                    className="d-flex align-items-center"
                    show="displayitems.pulsasafe"
                  >
                    <picture>
                      <img
                        src="https://im3-img.indosatooredoo.com/indosatassets/images/personal/pulsa-safe.svg"
                        alt="..."
                        title=".."
                      />
                    </picture>
                    <p className="ml-2 mb-0">Pulsa Safe</p>
                  </div>
                  <div
                    className="d-flex align-items-center hide"
                    show="pack.call!=null &amp;&amp; displayitems.call"
                  >
                    <picture>
                      <img
                        src="https://im3-img.indosatooredoo.com/indosatassets/images/personal/call.svg"
                        alt="..."
                        title=".."
                      />
                    </picture>
                    <p className="ml-2 mb-0">
                      Call <span className="binding"> Minutes</span>{" "}
                    </p>
                  </div>

                  <div
                    className="d-flex align-items-center hide"
                    show="pack.calltoindosat!=null &amp;&amp; displayitems.calltoindosat"
                  >
                    <picture>
                      <img
                        src="https://im3-img.indosatooredoo.com/indosatassets/images/personal/call.svg"
                        alt="..."
                        title=".."
                      />
                    </picture>
                    <p className="ml-2 mb-0">
                      Call to Indosat <span className="binding"> </span>{" "}
                    </p>
                  </div>

                  <div
                    className="d-flex align-items-center hide"
                    show="pack.calltoother!=null &amp;&amp; displayitems.calltoother"
                  >
                    <picture>
                      <img
                        src="https://im3-img.indosatooredoo.com/indosatassets/images/personal/call.svg"
                        alt="..."
                        title=".."
                      />
                    </picture>
                    <p className="ml-2 mb-0">
                      Call to Others <span className="binding"> </span>{" "}
                    </p>
                  </div>

                  <div
                    className="d-flex align-items-center hide"
                    show="pack.sms!=null    &amp;&amp; displayitems.sms"
                  >
                    <picture>
                      <img
                        src="https://im3-img.indosatooredoo.com/indosatassets/images/personal/sms.svg"
                        alt="..."
                        title=".."
                      />
                    </picture>
                    <p className="ml-2 mb-0">
                      SMS <span bind="pack.sms" className="binding"></span>{" "}
                    </p>
                  </div>
                  <div
                    className="d-flex align-items-center hide"
                    show="pack.calltoim3!=null    &amp;&amp; displayitems.calltoim3"
                  >
                    <picture>
                      <img
                        src="https://im3-img.indosatooredoo.com/indosatassets/images/personal/call.svg"
                        alt="..."
                        title=".."
                      />
                    </picture>
                    <p className="ml-2 mb-0">
                      <span className="binding"> </span> Call to IM3 Ooredoo{" "}
                    </p>
                  </div>
                  <div
                    className="d-flex align-items-center hide"
                    show="pack.smstoim3!=null  &amp;&amp; displayitems.smstoim3"
                  >
                    <picture>
                      <img
                        src="https://im3-img.indosatooredoo.com/indosatassets/images/personal/sms.svg"
                        alt="..."
                        title=".."
                      />
                    </picture>
                    <p className="ml-2 mb-0">
                      <span className="binding"> </span> SMS to IM3 Ooredoo{" "}
                    </p>
                  </div>
                  <div
                    className="d-flex align-items-center hide"
                    show="pack.appquota!=null  &amp;&amp; displayitems.appquota"
                  >
                    <picture>
                      <img
                        src="https://im3-img.indosatooredoo.com/indosatassets/images/personal/quota-app.svg"
                        alt="..."
                        title=".."
                      />
                    </picture>
                    <p className="ml-2 mb-0">
                      App quota <span className="binding"></span>{" "}
                    </p>
                  </div>
                  <div
                    className="d-flex align-items-center hide"
                    show="pack.streamquota!=null  &amp;&amp; displayitems.streamquota"
                  >
                    <picture>
                      <img
                        src="https://im3-img.indosatooredoo.com/indosatassets/images/personal/streaming-app.svg"
                        alt="..."
                        title=".."
                      />
                    </picture>
                    <p className="ml-2 mb-0">
                      $$Streaming quota$$ <span className="binding"></span>{" "}
                    </p>
                  </div>
                  <div
                    className="d-flex align-items-center hide"
                    show="pack.speedbooster!=null    &amp;&amp; displayitems.speedbooster"
                  >
                    <picture>
                      <img
                        src="https://im3-img.indosatooredoo.com/indosatassets/images/personal/speed-booster.svg"
                        alt="..."
                        title=".."
                      />
                    </picture>
                    <p className="ml-2 mb-0">
                      Speed booster <span className="binding"></span>{" "}
                    </p>
                  </div>
                  <div
                    className="d-flex align-items-center hide"
                    show="pack.impoinbonus !=null    &amp;&amp; displayitems.impoinbonus "
                  >
                    <picture>
                      <img
                        src="https://im3-img.indosatooredoo.com/indosatassets/images/personal/impoin.svg"
                        alt="..."
                        title=".."
                      />
                    </picture>
                    <p className="ml-2 mb-0">
                      <span className="binding"> point</span> Bonus IMPoin
                    </p>
                  </div>
                  <div
                    className="d-flex align-items-center hide"
                    show="pack.impoinreward !=null   &amp;&amp; displayitems.impoinreward  "
                  >
                    <picture>
                      <img
                        src="https://im3-img.indosatooredoo.com/indosatassets/images/personal/impoin.svg"
                        alt="..."
                        title=".."
                      />
                    </picture>
                    <p className="ml-2 mb-0">
                      <span className="binding"> point </span> rewards IMPoin{" "}
                    </p>
                  </div>

                  <div
                    className="d-flex align-items-start scope"
                    show="displayitems.cbenfit "
                    repeat="cbenfit in pack.customitems"
                  >
                    <picture>
                      <img
                        src="https://im3-img.indosatooredoo.com/indosatassets/images/personal/info.svg"
                        alt="..."
                        title=".."
                      />
                    </picture>
                    <p className="ml-2 mb-0 binding">
                      Available at *123*111#, myIM3 apps, IM3 Official WhatsApp,
                      e-Commerce and outlets <span className="binding"> </span>
                    </p>
                  </div>

                  <div
                    className="d-flex align-items-start scope"
                    show="displayitems.cbenfit "
                    repeat="cbenfit in pack.customitems"
                  >
                    <picture>
                      <img
                        src="https://im3-img.indosatooredoo.com/indosatassets/images/personal/info.svg"
                        alt="..."
                        title=".."
                      />
                    </picture>
                    <p className="ml-2 mb-0 binding">
                      Call to IM3 and Tri{" "}
                      <span className="binding">Limitless (5,000 min) </span>
                    </p>
                  </div>

                  <div
                    className="d-flex align-items-center hide"
                    show="pack.localquota!=null  &amp;&amp; displayitems.localquota"
                  >
                    <picture>
                      <img
                        src="https://im3-img.indosatooredoo.com/indosatassets/images/personal/quota.svg"
                        alt="..."
                        title=".."
                      />
                    </picture>
                    <p className="ml-2 mb-0">
                      Local quota <span className="binding"> </span>
                    </p>
                  </div>

                  <div
                    className="d-flex align-items-start hide"
                    show="pack.midnightquota!=null &amp;&amp; displayitems.midnightquota"
                  >
                    <picture>
                      <img
                        src="https://im3-img.indosatooredoo.com/indosatassets/images/personal/quota.svg"
                        alt="..."
                        title=".."
                      />
                    </picture>
                    <p className="ml-2 mb-0">
                      Night quota <span className="binding"> </span>
                    </p>
                  </div>

                  <div
                    className="d-flex align-items-center hide"
                    show="pack.midnightquota!=null  &amp;&amp; displayitems.extramidnightquota"
                  >
                    <picture>
                      <img
                        src="https://im3-img.indosatooredoo.com/indosatassets/images/personal/quota.svg"
                        alt="..."
                        title=".."
                      />
                    </picture>
                    <p className="ml-2 mb-0">
                      Extra night quota <span className="binding"> </span>
                    </p>
                  </div>
                </div>

                <div className="btn-link-cont d-flex justify-content-between align-items-center">
                 <button className="primary-btn px-4">CHOOSE</button>
                 <h6 className="mb-0">SEE DETAILS</h6>
                </div>
              </div>
            </div>
            <div className="plan-card me-3">
              <div className="product_card-body flex-body">
                <div className="benefit mb-4">
                  <h4 className="upper-title">Freedom Internet 100GB</h4>
                   <div className="price mb-3 fixed-h">
                  <h3 className="binding-price">Rp 16.500</h3>
                </div>
                  <div
                    className="d-flex align-items-start"
                    show="displayitems.validity"
                  >
                    <picture>
                      <img
                        src="https://im3-img.indosatooredoo.com/indosatassets/images/personal/periode.svg"
                        alt="..."
                        title=".."
                      />
                    </picture>
                    <p className="ml-2 mb-0">
                      Validity <span className="binding">30 Days </span>
                    </p>
                  </div>

                  <div
                    className="d-flex align-items-center hide"
                    show="pack.mainquota!=null &amp;&amp; displayitems.dailyquota"
                  >
                    <picture>
                      <img
                        src="https://im3-img.indosatooredoo.com/indosatassets/images/personal/quota.svg"
                        alt="..."
                        title=".."
                      />
                    </picture>
                    <p className="ml-2 mb-0">
                      Daily quota <span className="binding"> 2 GB / Day</span>
                    </p>
                  </div>

                  <div
                    className="d-flex align-items-center"
                    show="pack.mainquota!=null &amp;&amp; displayitems.mainquota"
                  >
                    <picture>
                      <img
                        src="https://im3-img.indosatooredoo.com/indosatassets/images/personal/quota.svg"
                        alt="..."
                        title=".."
                      />
                    </picture>
                    <p className="ml-2 mb-0">
                      Main quota <span className="binding">2 GB</span>
                    </p>
                  </div>
                  <div
                    className="d-flex align-items-center"
                    show="displayitems.pulsasafe"
                  >
                    <picture>
                      <img
                        src="https://im3-img.indosatooredoo.com/indosatassets/images/personal/pulsa-safe.svg"
                        alt="..."
                        title=".."
                      />
                    </picture>
                    <p className="ml-2 mb-0">Pulsa Safe</p>
                  </div>
                  <div
                    className="d-flex align-items-center hide"
                    show="pack.call!=null &amp;&amp; displayitems.call"
                  >
                    <picture>
                      <img
                        src="https://im3-img.indosatooredoo.com/indosatassets/images/personal/call.svg"
                        alt="..."
                        title=".."
                      />
                    </picture>
                    <p className="ml-2 mb-0">
                      Call <span className="binding"> Minutes</span>{" "}
                    </p>
                  </div>

                  <div
                    className="d-flex align-items-center hide"
                    show="pack.calltoindosat!=null &amp;&amp; displayitems.calltoindosat"
                  >
                    <picture>
                      <img
                        src="https://im3-img.indosatooredoo.com/indosatassets/images/personal/call.svg"
                        alt="..."
                        title=".."
                      />
                    </picture>
                    <p className="ml-2 mb-0">
                      Call to Indosat <span className="binding"> </span>{" "}
                    </p>
                  </div>

                  <div
                    className="d-flex align-items-center hide"
                    show="pack.calltoother!=null &amp;&amp; displayitems.calltoother"
                  >
                    <picture>
                      <img
                        src="https://im3-img.indosatooredoo.com/indosatassets/images/personal/call.svg"
                        alt="..."
                        title=".."
                      />
                    </picture>
                    <p className="ml-2 mb-0">
                      Call to Others <span className="binding"> </span>{" "}
                    </p>
                  </div>

                  <div
                    className="d-flex align-items-center hide"
                    show="pack.sms!=null    &amp;&amp; displayitems.sms"
                  >
                    <picture>
                      <img
                        src="https://im3-img.indosatooredoo.com/indosatassets/images/personal/sms.svg"
                        alt="..."
                        title=".."
                      />
                    </picture>
                    <p className="ml-2 mb-0">
                      SMS <span bind="pack.sms" className="binding"></span>{" "}
                    </p>
                  </div>
                  <div
                    className="d-flex align-items-center hide"
                    show="pack.calltoim3!=null    &amp;&amp; displayitems.calltoim3"
                  >
                    <picture>
                      <img
                        src="https://im3-img.indosatooredoo.com/indosatassets/images/personal/call.svg"
                        alt="..."
                        title=".."
                      />
                    </picture>
                    <p className="ml-2 mb-0">
                      <span className="binding"> </span> Call to IM3 Ooredoo{" "}
                    </p>
                  </div>
                  <div
                    className="d-flex align-items-center hide"
                    show="pack.smstoim3!=null  &amp;&amp; displayitems.smstoim3"
                  >
                    <picture>
                      <img
                        src="https://im3-img.indosatooredoo.com/indosatassets/images/personal/sms.svg"
                        alt="..."
                        title=".."
                      />
                    </picture>
                    <p className="ml-2 mb-0">
                      <span className="binding"> </span> SMS to IM3 Ooredoo{" "}
                    </p>
                  </div>
                  <div
                    className="d-flex align-items-center hide"
                    show="pack.appquota!=null  &amp;&amp; displayitems.appquota"
                  >
                    <picture>
                      <img
                        src="https://im3-img.indosatooredoo.com/indosatassets/images/personal/quota-app.svg"
                        alt="..."
                        title=".."
                      />
                    </picture>
                    <p className="ml-2 mb-0">
                      App quota <span className="binding"></span>{" "}
                    </p>
                  </div>
                  <div
                    className="d-flex align-items-center hide"
                    show="pack.streamquota!=null  &amp;&amp; displayitems.streamquota"
                  >
                    <picture>
                      <img
                        src="https://im3-img.indosatooredoo.com/indosatassets/images/personal/streaming-app.svg"
                        alt="..."
                        title=".."
                      />
                    </picture>
                    <p className="ml-2 mb-0">
                      $$Streaming quota$$ <span className="binding"></span>{" "}
                    </p>
                  </div>
             

                  <div
                    className="d-flex align-items-start scope"
                    show="displayitems.cbenfit "
                    repeat="cbenfit in pack.customitems"
                  >
                    <picture>
                      <img
                        src="https://im3-img.indosatooredoo.com/indosatassets/images/personal/info.svg"
                        alt="..."
                        title=".."
                      />
                    </picture>
                    <p className="ml-2 mb-0 binding">
                      Call to IM3 and Tri{" "}
                      <span className="binding">Limitless (5,000 min) </span>
                    </p>
                  </div>

                  <div
                    className="d-flex align-items-center hide"
                    show="pack.localquota!=null  &amp;&amp; displayitems.localquota"
                  >
                    <picture>
                      <img
                        src="https://im3-img.indosatooredoo.com/indosatassets/images/personal/quota.svg"
                        alt="..."
                        title=".."
                      />
                    </picture>
                    <p className="ml-2 mb-0">
                      Local quota <span className="binding"> </span>
                    </p>
                  </div>

                  <div
                    className="d-flex align-items-start hide"
                    show="pack.midnightquota!=null &amp;&amp; displayitems.midnightquota"
                  >
                    <picture>
                      <img
                        src="https://im3-img.indosatooredoo.com/indosatassets/images/personal/quota.svg"
                        alt="..."
                        title=".."
                      />
                    </picture>
                    <p className="ml-2 mb-0">
                      Night quota <span className="binding"> </span>
                    </p>
                  </div>

                  <div
                    className="d-flex align-items-center hide"
                    show="pack.midnightquota!=null  &amp;&amp; displayitems.extramidnightquota"
                  >
                    <picture>
                      <img
                        src="https://im3-img.indosatooredoo.com/indosatassets/images/personal/quota.svg"
                        alt="..."
                        title=".."
                      />
                    </picture>
                    <p className="ml-2 mb-0">
                      Extra night quota <span className="binding"> </span>
                    </p>
                  </div>
                </div>

                <div className="btn-link-cont d-flex justify-content-between align-items-center">
                 <button className="primary-btn px-4">CHOOSE</button>
                 <h6 className="mb-0">SEE DETAILS</h6>
                </div>
              </div>
            </div>
            <div className="plan-card me-3">
              <div className="product_card-body flex-body">
                <div className="benefit mb-4">
                  <h4 className="upper-title">Freedom Internet 100GB</h4>
                   <div className="price mb-3 fixed-h">
                  <h3 className="binding-price">Rp 16.500</h3>
                </div>
                  <div
                    className="d-flex align-items-start"
                    show="displayitems.validity"
                  >
                    <picture>
                      <img
                        src="https://im3-img.indosatooredoo.com/indosatassets/images/personal/periode.svg"
                        alt="..."
                        title=".."
                      />
                    </picture>
                    <p className="ml-2 mb-0">
                      Validity <span className="binding">30 Days </span>
                    </p>
                  </div>

                  <div
                    className="d-flex align-items-center hide"
                    show="pack.mainquota!=null &amp;&amp; displayitems.dailyquota"
                  >
                    <picture>
                      <img
                        src="https://im3-img.indosatooredoo.com/indosatassets/images/personal/quota.svg"
                        alt="..."
                        title=".."
                      />
                    </picture>
                    <p className="ml-2 mb-0">
                      Daily quota <span className="binding"> 2 GB / Day</span>
                    </p>
                  </div>

                </div>

                <div className="btn-link-cont d-flex justify-content-between align-items-center">
                 <button className="primary-btn px-4">CHOOSE</button>
                 <h6 className="mb-0">SEE DETAILS</h6>
                </div>
              </div>
            </div>
            <div className="plan-card me-3">
              <div className="product_card-body flex-body">
                <div className="benefit mb-4">
                  <h4 className="upper-title">Freedom Internet 100GB</h4>
                   <div className="price mb-3 fixed-h">
                  <h3 className="binding-price">Rp 16.500</h3>
                </div>
                  <div
                    className="d-flex align-items-start"
                    show="displayitems.validity"
                  >
                    <picture>
                      <img
                        src="https://im3-img.indosatooredoo.com/indosatassets/images/personal/periode.svg"
                        alt="..."
                        title=".."
                      />
                    </picture>
                    <p className="ml-2 mb-0">
                      Validity <span className="binding">30 Days </span>
                    </p>
                  </div>

                
                  <div
                    className="d-flex align-items-center"
                    show="displayitems.pulsasafe"
                  >
                    <picture>
                      <img
                        src="https://im3-img.indosatooredoo.com/indosatassets/images/personal/pulsa-safe.svg"
                        alt="..."
                        title=".."
                      />
                    </picture>
                    <p className="ml-2 mb-0">Pulsa Safe</p>
                  </div>
                  <div
                    className="d-flex align-items-center hide"
                    show="pack.call!=null &amp;&amp; displayitems.call"
                  >
                    <picture>
                      <img
                        src="https://im3-img.indosatooredoo.com/indosatassets/images/personal/call.svg"
                        alt="..."
                        title=".."
                      />
                    </picture>
                    <p className="ml-2 mb-0">
                      Call <span className="binding"> Minutes</span>{" "}
                    </p>
                  </div>

                  <div
                    className="d-flex align-items-center hide"
                    show="pack.calltoindosat!=null &amp;&amp; displayitems.calltoindosat"
                  >
                    <picture>
                      <img
                        src="https://im3-img.indosatooredoo.com/indosatassets/images/personal/call.svg"
                        alt="..."
                        title=".."
                      />
                    </picture>
                    <p className="ml-2 mb-0">
                      Call to Indosat <span className="binding"> </span>{" "}
                    </p>
                  </div>

                  <div
                    className="d-flex align-items-center hide"
                    show="pack.calltoother!=null &amp;&amp; displayitems.calltoother"
                  >
                    <picture>
                      <img
                        src="https://im3-img.indosatooredoo.com/indosatassets/images/personal/call.svg"
                        alt="..."
                        title=".."
                      />
                    </picture>
                    <p className="ml-2 mb-0">
                      Call to Others <span className="binding"> </span>{" "}
                    </p>
                  </div>

                  <div
                    className="d-flex align-items-center hide"
                    show="pack.sms!=null    &amp;&amp; displayitems.sms"
                  >
                    <picture>
                      <img
                        src="https://im3-img.indosatooredoo.com/indosatassets/images/personal/sms.svg"
                        alt="..."
                        title=".."
                      />
                    </picture>
                    <p className="ml-2 mb-0">
                      SMS <span bind="pack.sms" className="binding"></span>{" "}
                    </p>
                  </div>
                  <div
                    className="d-flex align-items-center hide"
                    show="pack.calltoim3!=null    &amp;&amp; displayitems.calltoim3"
                  >
                    <picture>
                      <img
                        src="https://im3-img.indosatooredoo.com/indosatassets/images/personal/call.svg"
                        alt="..."
                        title=".."
                      />
                    </picture>
                    <p className="ml-2 mb-0">
                      <span className="binding"> </span> Call to IM3 Ooredoo{" "}
                    </p>
                  </div>
                  <div
                    className="d-flex align-items-center hide"
                    show="pack.smstoim3!=null  &amp;&amp; displayitems.smstoim3"
                  >
                    <picture>
                      <img
                        src="https://im3-img.indosatooredoo.com/indosatassets/images/personal/sms.svg"
                        alt="..."
                        title=".."
                      />
                    </picture>
                    <p className="ml-2 mb-0">
                      <span className="binding"> </span> SMS to IM3 Ooredoo{" "}
                    </p>
                  </div>
                  <div
                    className="d-flex align-items-center hide"
                    show="pack.appquota!=null  &amp;&amp; displayitems.appquota"
                  >
                    <picture>
                      <img
                        src="https://im3-img.indosatooredoo.com/indosatassets/images/personal/quota-app.svg"
                        alt="..."
                        title=".."
                      />
                    </picture>
                    <p className="ml-2 mb-0">
                      App quota <span className="binding"></span>{" "}
                    </p>
                  </div>
                  <div
                    className="d-flex align-items-center hide"
                    show="pack.streamquota!=null  &amp;&amp; displayitems.streamquota"
                  >
                    <picture>
                      <img
                        src="https://im3-img.indosatooredoo.com/indosatassets/images/personal/streaming-app.svg"
                        alt="..."
                        title=".."
                      />
                    </picture>
                    <p className="ml-2 mb-0">
                      $$Streaming quota$$ <span className="binding"></span>{" "}
                    </p>
                  </div>
                  <div
                    className="d-flex align-items-center hide"
                    show="pack.speedbooster!=null    &amp;&amp; displayitems.speedbooster"
                  >
                    <picture>
                      <img
                        src="https://im3-img.indosatooredoo.com/indosatassets/images/personal/speed-booster.svg"
                        alt="..."
                        title=".."
                      />
                    </picture>
                    <p className="ml-2 mb-0">
                      Speed booster <span className="binding"></span>{" "}
                    </p>
                  </div>
                  <div
                    className="d-flex align-items-center hide"
                    show="pack.impoinbonus !=null    &amp;&amp; displayitems.impoinbonus "
                  >
                    <picture>
                      <img
                        src="https://im3-img.indosatooredoo.com/indosatassets/images/personal/impoin.svg"
                        alt="..."
                        title=".."
                      />
                    </picture>
                    <p className="ml-2 mb-0">
                      <span className="binding"> point</span> Bonus IMPoin
                    </p>
                  </div>
           

                  <div
                    className="d-flex align-items-start hide"
                    show="pack.midnightquota!=null &amp;&amp; displayitems.midnightquota"
                  >
                    <picture>
                      <img
                        src="https://im3-img.indosatooredoo.com/indosatassets/images/personal/quota.svg"
                        alt="..."
                        title=".."
                      />
                    </picture>
                    <p className="ml-2 mb-0">
                      Night quota <span className="binding"> </span>
                    </p>
                  </div>

                  <div
                    className="d-flex align-items-center hide"
                    show="pack.midnightquota!=null  &amp;&amp; displayitems.extramidnightquota"
                  >
                    <picture>
                      <img
                        src="https://im3-img.indosatooredoo.com/indosatassets/images/personal/quota.svg"
                        alt="..."
                        title=".."
                      />
                    </picture>
                    <p className="ml-2 mb-0">
                      Extra night quota <span className="binding"> </span>
                    </p>
                  </div>
                </div>

                <div className="btn-link-cont d-flex justify-content-between align-items-center">
                 <button className="primary-btn px-4">CHOOSE</button>
                 <h6 className="mb-0">SEE DETAILS</h6>
                </div>
              </div>
            </div> */}
            
            <div className="plan-card mx-2">
              <img className="" src="../images/image.png" alt="more plan" />
            </div>
            <div className="plan-card mx-2">
              <img className="" src="../images/image (2).png" alt="more plan" />
            </div>
            <div className="plan-card mx-2">
              <img className="" src="../images/image (1).png" alt="more plan" />
            </div>
            
          </div>
        </Box>
        <Box className="text-center my-4">
          <button  onClick={()=>navigate('/prepaid-plan')} className="primary-btn">
            Go back
          </button>
        </Box>
      </Container>
    </div>
  );
};

export default MorePlans;
