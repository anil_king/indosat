import React from "react";
import './App.css';
import './style.css';
import Header from './Layouts/Header'
import { Routes, Route } from 'react-router';
import Index from "./Pages/Index";
import IndexNG from "./Pages/IndexNG";
import PrepaidPlan from "./Pages/PrepaidPlan";
import PrepaidPlanNG from "./Pages/PrepaidPlanNG";
import StepForm from "./Pages/StepForm";
import MorePlans from "./Pages/MorePlans";
import MorePlansNG from "./Pages/MorePlansNG";
import { Carousel } from "react-bootstrap";

function App() {
  return (
   <>
   <Header/>
    <Routes>
      <Route path="/" element={<Index/>}/>
      <Route path="/ng" element={<IndexNG/>}/>
      <Route path="/prepaid-plan" element={<PrepaidPlan/>}/>
      <Route path="/prepaid-plan-NG" element={<PrepaidPlanNG/>}/>
      <Route path="/step-form" element={<StepForm/>}/>
      <Route path="/more-plans" element={<MorePlans/>}/>
      <Route path="/more-plans-ng" element={<MorePlansNG/>}/>
      <Route path="/carousel" element={<Carousel/>}/>
    </Routes>
   </>
  );
}

export default App;
