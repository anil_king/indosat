import React from "react";
import { styled } from "@mui/material/styles";
import { Grid, Paper, Box, Link } from "@mui/material";
import { Container } from "react-bootstrap";
import { useNavigate } from "react-router";
import { FaCottonBureau } from "react-icons/fa";
import { SlGlobe } from "react-icons/sl";
import { RiContactsBookUploadLine } from "react-icons/ri";
import { GiBattery50 } from "react-icons/gi";

const Index = () => {
  const navigate = useNavigate();
  const Item = styled(Paper)(({ theme }) => ({
    backgroundColor: theme.palette.mode === "dark" ? "#1A2027" : "#fff",
    ...theme.typography.body2,
    padding: theme.spacing(1),
    textAlign: "center",
    color: theme.palette.text.secondary,
  }));

  return (
    <div>
      <Box
        onClick={() => navigate("/prepaid-plan-NG")}
        className="home-banner position-relative"
        sx={{ flexGrow: 1 }}
      >
        <img className="" src="../images/ng1.png" alt="cupon" />
        <div className="baneer-title text-start">
          <h4>Data plans</h4>
          <h5>
            Choosing a mobile data
            <br />
            plan has never been this simples
          </h5>
        </div>
        {/* <Container className="h-100">
          <Grid className="h-100 align-items-end" container spacing={2}>
            <Grid className="" item xs={6}>
              <h2 className="home-banner-title text-center">
                IM3 Prepaid Plan{" "}
              </h2>
            </Grid>
            <Grid className="" item xs={6}></Grid>
          </Grid>
        </Container> */}
      </Box>

      <Box className="home-banner position-relative" sx={{ flexGrow: 1 }}>
        <img className="" src="../images/ng2.png" alt="cupon" />
        <div className="baneer-title text-start">
          <h5>
            MTN 5G
            <br />
            5G BROADBAND ROUTER
          </h5>
        </div>
        {/* <Container className="h-100">
          <Grid className="h-100 align-items-end" container spacing={2}>
            <Grid className="" item xs={6}></Grid>
            <Grid className="" item xs={6}>
              <h2 className="home-banner-title text-center">
                IM3 Prepaid Plan{" "}
              </h2>
            </Grid>
          </Grid>
        </Container> */}
      </Box>

      <Box className="home-banner position-relative" sx={{ flexGrow: 1 }}>
        <img className="" src="../images/ng3.png" alt="cupon" />
        <div className="baneer-title text-start">
          <h4>
            BROADBAND
            <br />
            Broadband Services
          </h4>
        </div>
        {/* <Container className="h-100">
          <Grid className="h-100 align-items-end" container spacing={2}>
            <Grid className="" item xs={6}>
              <h2 className="home-banner-title text-center">
                IM3 Prepaid Plan{" "}
              </h2>
            </Grid>
            <Grid className="" item xs={6}></Grid>
          </Grid>
        </Container> */}
      </Box>

      <Box className="home-banner position-relative" sx={{ flexGrow: 1 }}>
        <img className="" src="../images/ng4.png" alt="cupon" />
        <div className="baneer-title text-start">
          <h4>
            Personal
            <br />
            4G LTE
          </h4>
        </div>
        {/* <Container className="h-100">
          <Grid className="h-100 align-items-end" container spacing={2}>
            <Grid className="" item xs={6}></Grid>
            <Grid className="" item xs={6}>
              <h2 className="home-banner-title text-center">
                IM3 Prepaid Plan{" "}
              </h2>
            </Grid>
          </Grid>
        </Container> */}
      </Box>
    </div>
  );
};

export default Index;
