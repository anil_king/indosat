import { Box } from "@mui/material";
import { useNavigate } from "react-router";
import { FaCheckCircle } from "react-icons/fa";
import { AiFillCloseCircle } from "react-icons/ai";
import React, { useEffect, useState } from "react";
import { Container, Row, Col } from "react-bootstrap";
import { useDispatch, useSelector } from "react-redux";

import Popup from "../components/prepaid/Popup";
import { getMorePlans } from "../redux/action/prepaidPlanActions";
import { addCart } from "../redux/action/cartAction";
import { toast } from "react-hot-toast";

const PrepaidPlan = () => {
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const [planID, setPlanID] = useState({});
  const [open, setOpen] = useState(false);
  const morePlans = useSelector((state) => state.plansReducer.morePlans);
  const cartVal = useSelector((state) => state.cartReducer.cart);

  const handleOpen = (id) => {
    setPlanID(id);
    setOpen(true);
  };
  const handleClose = () => setOpen(false);

  const handleCart = (id) => {
    if (cartVal.includes(id)) {
      toast.error("plan already exist in cart!");
    } else {
      dispatch(addCart(id));
    }
  };

  useEffect(() => {
    dispatch(getMorePlans());
  }, [dispatch]);

  return (
    <div>
      <Box className="prepaid-plan-banner bg-FCD401" sx={{ flexGrow: 1 }}>
        <Container className="">
          <Row className="align-items-center" container spacing={2}>
            <Col className="" md={6}>
              <h2 className="home-banner-title text-capitalize text-center pt-3">
                IM3 Prepaid
              </h2>
            </Col>
            <Col className="text-center" md={6}>
              <img
                className="prepaid-plan-banner-img"
                src="../images/sim-m.png"
                alt="pp"
              />
            </Col>
          </Row>
        </Container>
      </Box>

      <Box className="best-offer py-4">
        <Container>
          <h3 className="primary-heading">
            Our Best Offer (Pre - Select Plan)
          </h3>
          <h3 className="plan-text py-3">
            ** {morePlans[0] && morePlans[0].name} **
          </h3>
          <div className="plan-benefits">
            <img
              className="coupon"
              src="../images/choose_number.png"
              alt="cupon"
            />
            <h3 className="secondary-heading text-center mb-5">
              Plan Benefits :-
            </h3>
            <Row className="mx-md-4">
              {/* <Col md={6}>
                <Box>
                  <h3 className="secondary-heading mb-3">
                    CHOOSE YOUR OWN BONUS
                  </h3>
                  <Box className="">
                    <Row>
                      <Col className="mb-2 pe-1" xs="4">
                        <Box className="white-card h-100">
                          <img
                            className="choose-bonus-img"
                            src="../images/ico_tokopedia.png"
                            alt="cupon"
                          />
                          <p className="choose-bonus-text">
                            Tokopedia Voucher up to Rp100.000
                          </p>
                        </Box>
                      </Col>
                      <Col className="mb-2 px-1" xs="4">
                        <Box className="white-card h-100">
                          <img
                            className="choose-bonus-img"
                            src="../images/icon_socmedgopay.png"
                            alt="cupon"
                          />
                          <p className="choose-bonus-text">
                            Saldo GoPay up to Rp100.000
                          </p>
                        </Box>
                      </Col>
                      <Col className="mb-2 ps-1" xs="4">
                        <Box className="white-card h-100">
                          <img
                            className="choose-bonus-img"
                            src="../images/ovo.png"
                            alt="cupon"
                          />
                          <p className="choose-bonus-text">
                            Saldo OVO up to Rp100.000
                          </p>
                        </Box>
                      </Col>
                    </Row>
                  </Box>
                </Box>
              </Col> */}
              {/* <Col md={6}> */}
              {/* <Box className="white-card px-4 py-4"> */}
              {/* <h3 className="secondary-heading mb-2">More Benefits :-</h3> */}

              {/* Allowance Keys */}
              <Container className="w-100 py-3">
                <Row>
                  {morePlans[0] &&
                    Object.keys(morePlans[0].allowance).map((item) => {
                      let val = morePlans[0].allowance[item].amount;
                      return (
                        <div key={item} className="col-md-3 py-2">
                          <h3 className="d-flex align-items-center mb-0">
                            {val === 0 ? (
                              <AiFillCloseCircle
                                className={`check-red-2 me-2`}
                              />
                            ) : (
                              <FaCheckCircle className={`check-green-2 me-2`} />
                            )}
                            {item}
                          </h3>
                        </div>
                      );
                    })}
                </Row>
              </Container>
              {morePlans.slice(0, 1).map((item) => {
                return (
                  <>
                    <ul key={item.Productid} className="doted p-0">
                      {item.allowance.data.amount !== 0 && (
                        <li className="py-2">
                          <h6 className="d-flex align-items-center mb-0">
                            {/* <FaCheckCircle className="check-green me-3" /> */}
                            <span className="pe-2"> &#9679;</span>
                            Bonus main quota{" "}
                            {`${item.allowance.data.frequency} ${item.allowance.data.amount} ${item.allowance.data.units}`}{" "}
                            data
                          </h6>
                        </li>
                      )}
                      {item.allowance.sms.amount !== 0 && (
                        <li className="py-2">
                          <h6 className="d-flex align-items-center mb-0">
                            {/* <FaCheckCircle className="check-green me-3" /> */}
                            <span className="pe-2"> &#9679;</span>
                            Get free{" "}
                            {` ${item.allowance.sms.amount} ${item.allowance.sms.units} ${item.allowance.sms.frequency}`}
                          </h6>
                        </li>
                      )}

                      {item.allowance.voice.amount !== 0 && (
                        <li className="py-2">
                          <h6 className="d-flex align-items-center mb-0">
                            {/* <FaCheckCircle className="check-green me-3" /> */}
                            <span className="pe-2"> &#9679;</span>
                            Get free{" "}
                            {` ${item.allowance.voice.amount} ${item.allowance.voice.units} ${item.allowance.voice.frequency}`}
                          </h6>
                        </li>
                      )}

                      {item.allowance.airtime.amount !== 0 && (
                        <li className="py-2">
                          <h6 className="d-flex align-items-center mb-0">
                            {/* <FaCheckCircle className="check-green me-3" /> */}
                            <span className="pe-2"> &#9679;</span>
                            Get free airtime{" "}
                            {`${item.allowance.airtime.amount} ${item.allowance.airtime.units}`}
                          </h6>
                        </li>
                      )}
                      <li className="py-2">
                        <h6 className="d-flex align-items-center mb-0">
                          {/* <FaCheckCircle className="check-green me-3" /> */}
                          <span className="pe-2"> &#9679;</span>
                          Price {`${item.currency} ${item.price}`}
                        </h6>
                      </li>
                    </ul>
                    <Box className="text-center my-3">
                      <button
                        className={`${
                          cartVal.includes(item.Productid)
                            ? "secondary-btn-outlined"
                            : "primary-btn-outlined cursor-pointer"
                        } m-2 `}
                        disabled={cartVal.includes(item.Productid)}
                        onClick={() => handleCart(item.Productid)}
                      >
                        Add To Cart
                      </button>
                      <button
                        className="primary-btn"
                        disabled={cartVal.includes(item.Productid)}
                        onClick={() => handleOpen(item)}
                      >
                        Buy Now
                      </button>
                    </Box>
                    {/* More Plans */}
                    <Box className="text-center">
                      <span
                        className={`link-btn ${
                          cartVal.includes(item.Productid)
                            ? ""
                            : "cursor-pointer"
                        } `}
                        onClick={() =>
                          !cartVal.includes(item.Productid) &&
                          navigate("/prepaid/more-plans")
                        }
                      >
                        More Plans
                      </span>
                    </Box>
                  </>
                );
              })}

              {/* Previous static benefits */}
              {/* <ul className="p-0">
                    <li className="py-2">
                      <h6 className="d-flex align-items-center mb-0">
                        <FaCheckCircle className="check-green me-3" />
                        Bonus main quota up to 5GB
                      </h6>
                    </li>
                    <li className="py-2">
                      <h6 className="d-flex align-items-center mb-0">
                        <FaCheckCircle className="check-green me-3" />
                        Free shipping Java area & discount for outside Java
                      </h6>
                    </li>
                    <li className="py-2">
                      <h6 className="d-flex align-items-center mb-0">
                        <FaCheckCircle className="check-green me-3" />
                        Choose your favourite number
                      </h6>
                    </li>
                    <li className="py-2">
                      <h6 className="d-flex align-items-center mb-0">
                        <FaCheckCircle className="check-green me-3" />
                        Big quota with affordable price
                      </h6>
                    </li>
                    <li className="py-2">
                      <h6 className="d-flex align-items-center mb-0">
                        <FaCheckCircle className="check-green me-3" />
                        Free Amazon Prime Video subscription 30 days
                      </h6>
                    </li>
                  </ul> */}
              {/* </Box> */}
              {/* </Col> */}
            </Row>
          </div>
        </Container>
      </Box>

      {/* Popup */}
      <Popup
        handleClose={handleClose}
        open={open}
        planData={planID}
        dynamicPath="prepaid"
      />
    </div>
  );
};

export default PrepaidPlan;
