import { Box, Breadcrumbs, Switch, Typography, Link } from "@mui/material";
import { useNavigate } from "react-router";
import React, { useEffect, useState } from "react";
import { Container, Row, Col } from "react-bootstrap";
import { useDispatch, useSelector } from "react-redux";
import Popup from "../components/prepaid/Popup";
import { getMorePlans } from "../redux/action/prepaidPlanActions";
import { getCart } from "../redux/action/cartAction";
import NavigateNextIcon from "@mui/icons-material/NavigateNext";

const Cart = () => {
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const [planID, setPlanID] = useState({});
  const [open, setOpen] = useState(false);
  const morePlans = useSelector((state) => state.plansReducer.morePlans);
  const cartVal = useSelector((state) => state.cartReducer.cart);

  const handleOpen = (id) => {
    setPlanID(id);
    setOpen(true);
  };
  const handleClose = () => setOpen(false);

  useEffect(() => {
    dispatch(getMorePlans());
    dispatch(getCart());
  }, [dispatch]);

  const label = { inputProps: { "aria-label": "Color switch demo" } };

  const breadcrumbs = [
    <Link
      underline="hover"
      key="1"
      color="inherit"
      onClick={() => navigate("/")}
    >
      Home
    </Link>,
    <Typography key="2" color="#ED1C24">
      Shopping Cart
    </Typography>,
  ];

  return (
    <div>
      <Box className="prepaid-plan-banner bg-FCD401" sx={{ flexGrow: 1 }}>
        <Container className="">
          <Row className="align-items-center" container spacing={2}>
            <Col className="" md={6}>
              <h2 className="home-banner-title text-capitalize text-center pt-3">
                IM3 Prepaid
              </h2>
            </Col>
            <Col className="text-center" md={6}>
              <img
                className="prepaid-plan-banner-img"
                src="../images/sim-m.png"
                alt="pp"
              />
            </Col>
          </Row>
        </Container>
      </Box>

      <Box className="best-offer py-4">
        <Container>
          <Breadcrumbs
            className=""
            separator={<NavigateNextIcon fontSize="small" />}
            aria-label="breadcrumb"
          >
            {breadcrumbs}
          </Breadcrumbs>
          <div
            className="plan-benefits"
            style={{ background: "rgb(242 246 248 / 30%)" }}
          >
            <img className="cart" src="../images/gr.png" alt="cupon" />
            <h3 className="secondary-heading text-center mb-5">
              Your Slected Plans :-
            </h3>
            {cartVal.length === 0 ? (
              <div className="text-center">You have no selected plans</div>
            ) : (
              <div>
                {cartVal.map((id) => {
                  return morePlans.map((item) => {
                    return (
                      id === item.Productid && (
                        <Row className="mx-md-4">
                          <Col md={6}>
                            <Box className="bg-white">
                              <Box className="check-bg phone-show d-flex align-items-center py-3 px-3">
                                <img
                                  className="me-2"
                                  src="../images/phone-icon.png"
                                  alt=""
                                />
                                <Box className="phone-icon">
                                  <small>Mobile Prepaid</small>
                                  <h5 className="mb-0">{item.name}</h5>
                                </Box>
                              </Box>
                              <Box className="px-4 pb-4">
                                <table className="table table-borderless">
                                  <tbody>
                                    <tr>
                                      <td></td>
                                      <td>
                                        <small>One-Time</small>
                                      </td>
                                      <td>
                                        <small>Recurring</small>
                                      </td>
                                    </tr>
                                    <tr>
                                      <td>Mobile prepaid plans</td>
                                      <td>Rp 0.00</td>
                                      <td>Rp 100</td>
                                    </tr>
                                    <tr>
                                      <td>{item.name}</td>
                                      <td>
                                        {" "}
                                        {item.currency} {item.price}
                                      </td>
                                      <td>Rp 0.00</td>
                                    </tr>
                                    <tr>
                                      <td colSpan={3}>
                                        Contract item 12 Month
                                      </td>
                                    </tr>
                                    <tr>
                                      <td className="p-0" colSpan={3}>
                                        <hr className="m-0" />
                                      </td>
                                    </tr>
                                    <tr>
                                      <td>Tax</td>
                                      <td>Rp 0.00</td>
                                      <td>Rp 0.00</td>
                                    </tr>
                                    <tr>
                                      <td className="p-0" colSpan={3}>
                                        <hr className="m-0" />
                                      </td>
                                    </tr>
                                    <tr>
                                      <th>Subtotal Incl. Tax</th>
                                      <td>
                                        {item.currency} {item.price}
                                      </td>
                                      <td>Rp 0.00</td>
                                    </tr>
                                  </tbody>
                                </table>
                              </Box>
                            </Box>
                          </Col>
                          <Col md={6}>
                            {/* <Box className="">
                  <div className="position-relative mb-3">
                    <input
                      className="coupon-apply-input"
                      type="text"
                      value={redeemcode}
                      onChange={(e) => setRedeemCode(e.target.value)}
                      placeholder="Enter Coupon code"
                    />
                    <button className="coupon-apply-btn">Apply</button>
                  </div>
                </Box> */}
                            <Box className="bg-white p-3">
                              <div className="d-flex align-items-center justify-content-between">
                                <h6 className="mb-0">Total Cost</h6>
                                <Switch
                                  {...label}
                                  defaultChecked
                                  color="warning"
                                />
                              </div>
                              <table className="table table-borderless">
                                <tbody>
                                  <tr>
                                    <td></td>
                                    <td>
                                      <small>One-Time</small>
                                    </td>
                                    <td>
                                      <small>Recurring</small>
                                    </td>
                                  </tr>
                                  <tr>
                                    <td>Total excl. Tax</td>
                                    <td>Rp 0.00</td>
                                    <td>Rp 100</td>
                                  </tr>
                                  <tr>
                                    <td>Tax</td>
                                    <td>Rp 0.00</td>
                                    <td>Rp 0.00</td>
                                  </tr>
                                  <tr>
                                    <td>Subtotal incl.Tax</td>
                                    <td>Rp 0.00</td>
                                    <td>Rp 0.00</td>
                                  </tr>
                                  <tr>
                                    <td td className="p-0" colSpan={3}>
                                      <hr className="m-0" />
                                    </td>
                                  </tr>
                                  <tr>
                                    <td>Total</td>
                                    <td>
                                      {" "}
                                      {item.currency} {item.price}
                                    </td>
                                    <td>Rp 100</td>
                                  </tr>
                                </tbody>
                              </table>
                              <div className="text-center">
                                <button
                                  onClick={() => handleOpen(item)}
                                  className="primary-btn"
                                >
                                  Checkout
                                </button>
                              </div>
                            </Box>
                          </Col>
                        </Row>
                      )
                    );
                  });
                })}
              </div>
            )}
          </div>
        </Container>
      </Box>

      {/* Popup */}
      <Popup
        handleClose={handleClose}
        open={open}
        planData={planID}
        dynamicPath="prepaid"
      />
    </div>
  );
};

export default Cart;
