import * as yup from "yup";
import {
  Box,
  Stepper,
  Step,
  StepLabel,
  StepContent,
  styled,
  TextField,
  alpha,
  Radio,
  RadioGroup,
  FormControl,
  FormControlLabel,
  Divider,
  Modal,
  Fade,
  Backdrop,
} from "@mui/material";
import StepConnector, {
  stepConnectorClasses,
} from "@mui/material/StepConnector";
import { toast } from "react-hot-toast";
import update from "immutability-helper";
import { Row, Col } from "react-bootstrap";
import { FaCheckCircle } from "react-icons/fa";
import { useDispatch, useSelector } from "react-redux";
import React, { useCallback, useEffect, useState } from "react";
import { useLocation, useNavigate, useParams } from "react-router";
import { useFlutterwave, closePaymentModal } from "flutterwave-react-v3";

import {
  applyRedeemCode,
  customerCreateProxy,
  getMSIDNNums,
  updateData,
} from "../redux/action/prepaidPlanActions";
import { PHONE_REGEX, STRING_REGEX } from "../regexPattern";

const BpIcon = styled("span")(({ theme }) => ({
  borderRadius: "50%",
  width: 16,
  height: 16,
  boxShadow:
    theme.palette.mode === "dark"
      ? "0 0 0 1px rgb(16 22 26 / 40%)"
      : "inset 0 0 0 1px rgba(16,22,26,.2), inset 0 -1px 0 rgba(16,22,26,.1)",
  backgroundColor: theme.palette.mode === "dark" ? "#394b59" : "#f5f8fa",
  backgroundImage:
    theme.palette.mode === "dark"
      ? "linear-gradient(180deg,hsla(0,0%,100%,.05),hsla(0,0%,100%,0))"
      : "linear-gradient(180deg,hsla(0,0%,100%,.8),hsla(0,0%,100%,0))",
  ".Mui-focusVisible &": {
    outline: "2px auto #ED1C24",
    outlineOffset: 2,
  },
  "input:hover ~ &": {
    backgroundColor: theme.palette.mode === "dark" ? "#30404d" : "#ebf1f5",
  },
  "input:disabled ~ &": {
    boxShadow: "none",
    background:
      theme.palette.mode === "dark"
        ? "rgba(57,75,89,.5)"
        : "rgba(206,217,224,.5)",
  },
}));

const BpCheckedIcon = styled(BpIcon)({
  backgroundColor: "#ED1C24",
  backgroundImage:
    "linear-gradient(180deg,hsla(0,0%,100%,.1),hsla(0,0%,100%,0))",
  "&:before": {
    display: "block",
    width: 16,
    height: 16,
    backgroundImage: "radial-gradient(#fff,#fff 28%,transparent 32%)",
    content: '""',
  },
  "input:hover ~ &": {
    backgroundColor: "#ED1C24",
  },
});

function BpRadio(props) {
  return (
    <Radio
      disableRipple
      color="default"
      checkedIcon={<BpCheckedIcon />}
      icon={<BpIcon />}
      {...props}
    />
  );
}

const RedditTextField = styled((props) => (
  <>
    <TextField InputProps={{ disableUnderline: true }} {...props} />
    {props.hasError ? (
      <p
        style={{ color: "red", fontWeight: "300", fontSize: "14px" }}
      >{`Please fill correct value for "${props.label}".`}</p>
    ) : (
      ""
    )}
  </>
))(({ theme }) => ({
  "& .MuiFilledInput-root": {
    overflow: "hidden",
    borderRadius: 4,
    backgroundColor: theme.palette.mode === "light" ? "#F2F6F8" : "#F2F6F8",
    transition: theme.transitions.create([
      "border-color",
      "background-color",
      "box-shadow",
    ]),
    "&:hover": {
      backgroundColor: "#eeeeee",
    },
    "&.Mui-focused": {
      backgroundColor: "#eeeeee",
      boxShadow: `${alpha(theme.palette.primary.main, 0.25)} 0 0 0 2px`,
      borderColor: theme.palette.primary.main,
    },
  },
}));

const StepForm = () => {
  const params = useParams();
  const navigate = useNavigate();
  const location = useLocation();
  console.log(location.state);
  const dispatch = useDispatch();
  const { productData, fileId } = location.state;
  const RedeemLoader = useSelector((state) => state.plansReducer.loading);
  const SubmitLoader = useSelector((state) => state.plansReducer.apiLoading);

  const [open, setOpen] = useState(false);
  const [errors, setErrors] = useState({
    firstname: false,
    lastname: false,
    email: false,
    contactnumber: false,
    gender: false,
    dob: false,
    addresslineone: false,
    addresslinetwo: false,
    addresslinethree: false,
    country: false,
    state: false,
    city: false,
    postalcode: false,
    customercategory: false,
    source: false,
    msisdn: false,
  });
  const [redeemcode, setRedeemCode] = useState();
  const [activeStep, setActiveStep] = useState(0);
  const [redeemBtn, setRedeemBtn] = useState(false);
  const [stepForm, setStepForm] = useState({
    firstname: "",
    lastname: "",
    email: "",
    contactnumber: "",
    gender: "",
    dob: "",
    accounttype: params.plan_title,
    addresslineone: "",
    addresslinetwo: "",
    addresslinethree: "",
    country: "",
    state: "",
    city: "",
    postalcode: "",
    productid: productData.Productid,
    customercategory: "",
    source: "",
    msisdn: "",
    simType: "Physical",
    // name_on_card: "",
    // card_no: "",
  });
  const [transactionId, setTransactionId] = useState();

  const formSchema = {
    step0: yup.object().shape({
      firstname: yup
        .string()
        .matches(STRING_REGEX, "First Name is not valid")
        .required(),
      lastname: yup
        .string()
        .matches(STRING_REGEX, "Last Name is not valid")
        .required(),
      email: yup
        .string()
        .email()
        .required(),
      contactnumber: yup
        .string()
        .matches(PHONE_REGEX, "Phone number is not valid")
        .max(15)
        .required("A phone number is required"),
      dob: yup.string().required(),
      gender: yup.string().required(),
    }),
    step1: yup.object().shape({
      addresslineone: yup.string().required(),
      addresslinetwo: yup.string().required(),
      country: yup
        .string()
        .matches(STRING_REGEX, "Country is not valid")
        .required(),
      state: yup
        .string()
        .matches(STRING_REGEX, "State is not valid")
        .required(),
      city: yup
        .string()
        .matches(STRING_REGEX, "City is not valid")
        .required(),
      postalcode: yup
        .number()

        .required(),
    }),
    step2: yup.object().shape({
      msisdn: yup.string().required(),
    }),
  };

  const checkBenefit = useSelector(
    (state) => state.plansReducer.checkBenefitCode
  );
  const [anotherModal, setAnotherModal] = useState(false);
  const msidnNums = useSelector((state) => state.plansReducer.msidnNums);

  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);

  // form handle Change
  const handleChangeForm = (e) => {
    if (e.target.name === "redeemcode") {
      setRedeemCode(e.target.value);
      setRedeemBtn(false);
    } else {
      setStepForm({ ...stepForm, [e.target.name]: e.target.value });
      setErrors({ ...errors, [e.target.name]: false });
    }
  };

  // handle Submit
  const handleSubmit = useCallback(() => {
    const payload = {
      ...stepForm,
    };
    dispatch(
      customerCreateProxy(payload, () => {
        dispatch(updateData(payload, fileId, handleOpen));
      })
    );
  }, [stepForm]);

  // handle Redeem
  const handleApplyRedeem = () => {
    const payload = {
      ...stepForm,

      redeemcode,
      price: productData.price,
    };
    dispatch(applyRedeemCode(payload, () => setRedeemBtn(true)));
  };

  let price = redeemBtn
    ? checkBenefit[0].params.BENEFIT_TYPE === "DISCOUNT"
      ? productData.price - checkBenefit[0].params.REWARD_VALUE
      : productData.price
    : productData.price;

  useEffect(() => {
    dispatch(getMSIDNNums());
  }, [dispatch]);

  const handleNext = async (step) => {
    // step 0
    if (activeStep == 0) {
      const isFormValid = await formSchema.step0.isValid(stepForm, {
        abortEarly: false,
      });
      if (isFormValid) {
        setActiveStep(step);
      } else {
        formSchema.step0
          .validate(stepForm, { abortEarly: false })
          .catch((err) => {
            const errors = err.inner.reduce((acc, error) => {
              return {
                ...acc,
                [error.path]: true,
              };
            }, {});

            setErrors((prevErrors) =>
              update(prevErrors, {
                $set: errors,
              })
            );
          });
      }
    }
    // step 1
    else if (activeStep == 1) {
      const isFormValid = await formSchema.step1.isValid(stepForm, {
        abortEarly: false,
      });

      if (isFormValid) {
        setActiveStep(step);
      } else {
        formSchema.step1
          .validate(stepForm, { abortEarly: false })
          .catch((err) => {
            const errors = err.inner.reduce((acc, error) => {
              return {
                ...acc,
                [error.path]: true,
              };
            }, {});

            setErrors((prevErrors) =>
              update(prevErrors, {
                $set: errors,
              })
            );
          });
      }
    }
    // step 2
    else if (activeStep == 2) {
      const isFormValid = await formSchema.step2.isValid(stepForm, {
        abortEarly: false,
      });

      if (isFormValid) {
        setActiveStep(
          stepForm.simType === "Physical" ? step : step > 2 ? step - 1 : step
        );
      } else {
        formSchema.step2
          .validate(stepForm, { abortEarly: false })
          .catch((err) => {
            const errors = err.inner.reduce((acc, error) => {
              return {
                ...acc,
                [error.path]: true,
              };
            }, {});

            setErrors((prevErrors) =>
              update(prevErrors, {
                $set: errors,
              })
            );
          });
      }
    }
    // step 3,4,5
    else {
      setActiveStep(
        stepForm.simType === "Physical" ? step : step <= 2 ? step : step - 1
      );
    }
  };

  // Handle Payment
  const config = {
    public_key: process.env.REACT_APP_PUBLIC_KEY,
    tx_ref: Date.now(),
    amount: price,
    currency: productData.currency,
    payment_options: "card",
    customer: {
      email: stepForm.email,
      phone_number: stepForm.contactnumber,
      name: stepForm.firstname + stepForm.lastname,
      // email: "user@gmail.com",
      // phone_number: "070********",
      // name: "john doe",
    },
    customizations: {
      title: "my Payment Title",
      description: "Payment for items in cart",
      logo:
        "https://st2.depositphotos.com/4403291/7418/v/450/depositphotos_74189661-stock-illustration-online-shop-log.jpg",
    },
  };
  const handleFlutterPayment = useFlutterwave(config);
  const handlePay = () => {
    handleFlutterPayment({
      callback: (response) => {
        if (response.status === "successful") {
          toast.success("Payment Success");
          setTransactionId(response.transaction_id);
          closePaymentModal();
          handleSubmit();
        }
      },
      onClose: () => {},
    });
  };

  // All Steps
  let steps = [
    {
      label: "Customer Details",
      description: (
        <Box>
          <Row>
            <Col md={6}>
              <RedditTextField
                fullWidth
                label="First name"
                name="firstname"
                value={stepForm.firstname}
                onChange={handleChangeForm}
                id="reddit-input"
                variant="filled"
                hasError={errors.firstname}
                style={{ marginTop: 11 }}
              />
            </Col>

            <Col md={6}>
              <RedditTextField
                fullWidth
                label="Last name"
                name="lastname"
                value={stepForm.lastname}
                hasError={errors.lastname}
                onChange={handleChangeForm}
                id="reddit-input"
                variant="filled"
                style={{ marginTop: 11 }}
              />
            </Col>
            <Col md={6}>
              <RedditTextField
                fullWidth
                label="Email Id"
                name="email"
                value={stepForm.email}
                hasError={errors.email}
                onChange={handleChangeForm}
                id="reddit-input"
                variant="filled"
                style={{ marginTop: 11 }}
              />
            </Col>
            <Col md={6}>
              <RedditTextField
                fullWidth
                label="Mobile number"
                name="contactnumber"
                value={stepForm.contactnumber}
                hasError={errors.contactnumber}
                onChange={handleChangeForm}
                id="reddit-input"
                variant="filled"
                style={{ marginTop: 11 }}
              />
            </Col>
            <Col md={6}>
              <RedditTextField
                fullWidth
                label="Date Of Birth"
                name="dob"
                type="date"
                value={stepForm.dob}
                onChange={handleChangeForm}
                hasError={errors.dob}
                id="reddit-input"
                variant="filled"
                style={{ marginTop: 11 }}
                InputLabelProps={{
                  shrink: true,
                }}
              />
            </Col>
            <Col md={12}>
              <FormControl>
                <p className="color-AFAFB1 mt-3 mb-2">Gender</p>
                <RadioGroup
                  row
                  onChange={handleChangeForm}
                  aria-labelledby="demo-row-radio-buttons-group-label"
                  name="gender"
                  value={stepForm.gender}
                >
                  <FormControlLabel
                    defaultChecked
                    value="Male"
                    control={<BpRadio />}
                    label="Male"
                  />
                  <FormControlLabel
                    value="Female"
                    control={<BpRadio />}
                    label="Female"
                  />
                  <FormControlLabel
                    value="Other"
                    control={<BpRadio />}
                    label="Other"
                  />
                  <FormControlLabel
                    value="Prefer Not to Answer"
                    control={<BpRadio />}
                    label="Prefer Not to Answer"
                  />
                </RadioGroup>
                {errors.gender ? (
                  <p
                    style={{
                      color: "red",
                      fontWeight: "300",
                      fontSize: "14px",
                    }}
                  >{`Please fill correct value for Gender.`}</p>
                ) : (
                  ""
                )}
              </FormControl>
            </Col>
          </Row>
        </Box>
      ),
    },
    {
      label: "Address Details",
      description: (
        <Box>
          <Row>
            <Col md={8}>
              <RedditTextField
                fullWidth
                label="Flat no, house no, Building"
                name="addresslineone"
                value={stepForm.addresslineone}
                onChange={handleChangeForm}
                id="reddit-input"
                variant="filled"
                hasError={errors.addresslineone}
                style={{ marginTop: 11 }}
              />
            </Col>
            <Col md={8}>
              <RedditTextField
                fullWidth
                label="Area, Street"
                name="addresslinetwo"
                value={stepForm.addresslinetwo}
                onChange={handleChangeForm}
                id="reddit-input"
                variant="filled"
                hasError={errors.addresslinetwo}
                style={{ marginTop: 11 }}
              />
            </Col>
            <Col md={8}>
              <RedditTextField
                fullWidth
                label="State"
                name="state"
                value={stepForm.state}
                onChange={handleChangeForm}
                id="reddit-input"
                variant="filled"
                hasError={errors.state}
                style={{ marginTop: 11 }}
              />
            </Col>
            <Col md={8}>
              <RedditTextField
                fullWidth
                label="Country"
                name="country"
                value={stepForm.country}
                onChange={handleChangeForm}
                id="reddit-input"
                variant="filled"
                hasError={errors.country}
                style={{ marginTop: 11 }}
              />
            </Col>
            <Col md={8}>
              <RedditTextField
                fullWidth
                label="City"
                name="city"
                value={stepForm.city}
                onChange={handleChangeForm}
                id="reddit-input"
                variant="filled"
                hasError={errors.city}
                style={{ marginTop: 11 }}
              />
            </Col>
            <Col md={8}>
              <RedditTextField
                fullWidth
                label="Postal code"
                name="postalcode"
                value={stepForm.postalcode}
                onChange={handleChangeForm}
                id="reddit-input"
                variant="filled"
                hasError={errors.postalcode}
                style={{ marginTop: 11 }}
              />
            </Col>
          </Row>
        </Box>
      ),
    },
    {
      label: "Choose your Number",
      description: (
        <>
          <Box>
            <RadioGroup
              onChange={handleChangeForm}
              aria-labelledby="demo-row-radio-buttons-group-label"
              name="msisdn"
              value={stepForm.msisdn}
            >
              <Row>
                <Col md={8}>
                  {msidnNums &&
                    msidnNums.map((item) => {
                      return (
                        <Box key={item.msisdn} className="">
                          <FormControlLabel
                            className="check-bg"
                            value={item.msisdn}
                            control={<BpRadio />}
                            label={item.msisdn}
                          />
                        </Box>
                      );
                    })}
                  {errors.msisdn ? (
                    <p
                      style={{
                        color: "red",
                        fontWeight: "300",
                        fontSize: "14px",
                      }}
                    >{`Please fill correct value for MSISDN.`}</p>
                  ) : (
                    ""
                  )}
                </Col>
              </Row>
            </RadioGroup>
            <RadioGroup
              className="mt-2"
              row
              value={stepForm.simType}
              onChange={handleChangeForm}
              aria-labelledby="demo-row-radio-buttons-group-label"
              name="simType"
            >
              <FormControlLabel
                value="Physical"
                control={<BpRadio />}
                label="Physical"
              />
              <FormControlLabel
                value="E-Sim"
                control={<BpRadio />}
                label="E-Sim"
              />
            </RadioGroup>
          </Box>

          {stepForm.simType !== "Physical" && (
            <Box className="my-1">
              <small>
                Details of activation will be sent to your email address
              </small>
              <Col md={6}>
                <RedditTextField
                  fullWidth
                  disabled
                  label="Email Id"
                  defaultValue={stepForm.email}
                  id="reddit-input"
                  variant="filled"
                  style={{ marginTop: 11 }}
                />
              </Col>
            </Box>
          )}
        </>
      ),
    },
    {
      label: "Delivery Option",
      description: (
        <Box>
          <RadioGroup
            defaultValue="store"
            aria-labelledby="demo-row-radio-buttons-group-label"
            name="row-radio-buttons-group"
          >
            <Row>
              <Col md={10}>
                <div className="d-flex align-items-cneter">
                  <Box>
                    <FormControlLabel
                      className="check-bg"
                      value="store"
                      control={<BpRadio />}
                      label="Pick Up from Store"
                      style={{ marginLeft: 0 }}
                    />
                  </Box>
                  <button
                    className="link-btn"
                    onClick={() => setAnotherModal(true)}
                  >
                    Find nearest store
                  </button>
                </div>

                <Box>
                  <FormControlLabel
                    className="check-bg"
                    value="address"
                    control={<BpRadio />}
                    label="Deliver to Address"
                    style={{ marginLeft: 0 }}
                  />
                </Box>
              </Col>
            </Row>
          </RadioGroup>
        </Box>
      ),
    },
    {
      label: "Apply Promotion",
      description: (
        <Box>
          <Col md={8}>
            <Box className="check-bg d-flex align-items-center justify-content-between py-3">
              <p className="mb-0">Net Amount</p>
              <p className="mb-0">
                {productData.currency} {productData.price}
              </p>
            </Box>
          </Col>
          <Col md={8}>
            <Box className="check-bg d-flex align-items-center justify-content-between py-3">
              <input
                type="text"
                placeholder="Coupen Code"
                value={redeemcode}
                name="redeemcode"
                onChange={handleChangeForm}
                style={{
                  background: "#0000",
                  border: "none",
                }}
              />
              <span
                onClick={redeemBtn ? "" : handleApplyRedeem}
                className={`mb-0 color-71CE7B ${
                  redeemBtn ? "" : "cursor-pointer"
                }`}
              >
                {RedeemLoader ? (
                  <svg
                    style={{ width: "22px" }}
                    viewBox="0 0 100 100"
                    preserveAspectRatio="xMidYMid"
                  >
                    <circle
                      cx="50"
                      cy="50"
                      fill="none"
                      stroke="#ed1c24"
                      strokeWidth="10"
                      r="35"
                      strokeDasharray="164.93361431346415 56.97787143782138"
                    >
                      <animateTransform
                        attributeName="transform"
                        type="rotate"
                        repeatCount="indefinite"
                        dur="1s"
                        values="0 50 50;360 50 50"
                        keyTimes="0;1"
                      />
                    </circle>
                  </svg>
                ) : redeemBtn ? (
                  "Applied"
                ) : (
                  "Apply"
                )}
              </span>
            </Box>
            {redeemBtn
              ? checkBenefit[0].params.BENEFIT_TYPE === "CASHBACK"
                ? checkBenefit[0].desc
                : ""
              : ""}
          </Col>
          {/* <Col md={8}>
            <Box className="check-bg d-flex align-items-center justify-content-between py-3">
              <p className="mb-0 color-AFAFB1">Coupon Code</p>
              <a className="mb-0 color-71CE7B">Apply</a>
            </Box>
          </Col> */}
          {/* <Box className="d-flex">
            <img
              className="cards-icon"
              style={{ border: "1px solid rgb(255 5 5)" }}
              src="../images/visa.png"
            />
            <img className="cards-icon" src="../images/paypal.png" />
          </Box> */}
          {/* <Button
            onClick={handlePay}
            disabled={isPaySuccess}
            variant="outlined"
            className="my-4"
          >
            Go To Payment
          </Button> */}
          {/* <Box className="mt-3">
            <Col md={10}>
              <RedditTextField
                fullWidth
                label="Name on Card"
                name="name_on_card"
                // value={stepForm.name_on_card}
                onChange={handleChangeForm}
                id="reddit-input"
                variant="filled"
                style={{ marginTop: 11 }}
              />
            </Col>
            <Col md={10}>
              <RedditTextField
                fullWidth
                label="Card number"
                name="card_no"
                // value={stepForm.card_no}
                onChange={handleChangeForm}
                id="reddit-input"
                variant="filled"
                style={{ marginTop: 11 }}
              />
            </Col>
            <Col md={10}>
              <Row className="align-items-center mt-3">
                <Col md={6}>
                  <label>Expiry Date</label>
                </Col>
                <Col md={6}>
                  <Row>
                    <Col md={6}>
                      <FormControl fullWidth>
                        <Select
                          fullWidth
                          labelId="demo-simple-select-label"
                          id="demo-simple-select"
                        >
                          <MenuItem value={1}>1</MenuItem>
                          <MenuItem value={2}>2</MenuItem>
                          <MenuItem value={3}>3</MenuItem>
                          <MenuItem value={4}>4</MenuItem>
                          <MenuItem value={5}>5</MenuItem>
                          <MenuItem value={6}>6</MenuItem>
                          <MenuItem value={7}>7</MenuItem>
                          <MenuItem value={8}>8</MenuItem>
                          <MenuItem value={9}>9</MenuItem>
                          <MenuItem value={10}>10</MenuItem>
                          <MenuItem value={11}>11</MenuItem>
                          <MenuItem value={12}>12</MenuItem>
                        </Select>
                      </FormControl>
                    </Col>
                    <Col md={6}>
                      <FormControl fullWidth>
                        <Select
                          fullWidth
                          labelId="demo-simple-select-label"
                          id="demo-simple-select"
                        >
                          <MenuItem value={10}>2023</MenuItem>
                          <MenuItem value={10}>2024</MenuItem>
                          <MenuItem value={10}>2025</MenuItem>
                          <MenuItem value={10}>2026</MenuItem>
                          <MenuItem value={20}>2027</MenuItem>
                          <MenuItem value={30}>2028</MenuItem>
                          <MenuItem value={30}>2029</MenuItem>
                          <MenuItem value={30}>2030</MenuItem>
                          <MenuItem value={30}>2031</MenuItem>
                          <MenuItem value={30}>2032</MenuItem>
                          <MenuItem value={30}>2033</MenuItem>
                          <MenuItem value={30}>2034</MenuItem>
                        </Select>
                      </FormControl>
                    </Col>
                  </Row>
                </Col>
              </Row>
              <Row className="align-items-center mt-3">
                <Col md={6}>
                  <label>CVV</label>
                </Col>
                <Col md={6}>
                  <Row>
                    <Col md={6}>
                      <FormControl fullWidth>
                        <RedditTextField
                          fullWidth
                          defaultValue=""
                          id="reddit-input"
                          variant="filled"
                          style={{ marginTop: 11 }}
                        />
                      </FormControl>
                    </Col>
                    <Col md={6}></Col>
                  </Row>
                </Col>
              </Row>
            </Col>
          </Box> */}
        </Box>
      ),
    },
    {
      label: "Order Summary",
      description: (
        <Box>
          <Col md={8}>
            <Box className="check-bg phone-show d-flex align-items-center py-3 px-3">
              <img className="me-2" src="../images/phone-icon.png" alt="" />
              <Box className="phone-icon">
                <small>Mobile Prepaid</small>
                <h5 className="mb-0">{productData.name}</h5>
              </Box>
            </Box>
            <Box className="mt-3">
              <h6>Shipping Address</h6>
              <p className="shipping-address">
                {`${stepForm.firstname} ${stepForm.lastname} ${stepForm.addresslineone} ${stepForm.addresslinetwo} ${stepForm.state} ${stepForm.city} Postal code: ${stepForm.postalcode}`}
                {/* John Doe 47896321 Menara building,9-11, 7th Street, near nursery,
                Jakarta, Postal code: 12930 */}
              </p>
            </Box>
            <Divider />
            <Box className="mt-3">
              <h6>Payment Method</h6>
              <div className="d-flex align-items-center">
                <img
                  className="me-3 card-num"
                  src="../images/visa.png"
                  alt=""
                />
                {/* <span>{`Ending with ****${stepForm.card_no.slice(
                  stepForm.card_no.length - 4,
                  stepForm.card_no.length
                )}`}</span> */}
              </div>
            </Box>
            {/* Actual */}
            <Box className="total-rp">
              <p className="mb-0">Actual price</p>
              <p className="mb-0">
                {productData.currency} {productData.price}
              </p>
            </Box>

            {/* Discount */}
            {redeemBtn ? (
              checkBenefit[0].params.BENEFIT_TYPE === "DISCOUNT" ? (
                <Box className="total-rp">
                  <p className="mb-0">Discount</p>
                  <p className="mb-0">{checkBenefit[0].params.REWARD_VALUE}</p>
                </Box>
              ) : (
                ""
              )
            ) : (
              ""
            )}
            {/* Total */}
            <Box className="total-rp">
              <p className="mb-0">Total</p>
              <p className="mb-0">
                {productData.currency}
                <span className="px-1">{price > 0 ? price : 0}</span>
              </p>
            </Box>
          </Col>
        </Box>
      ),
    },
  ];

  const QontoConnector = styled(StepConnector)(({ theme }) => ({
    [`&.${stepConnectorClasses.alternativeLabel}`]: {
      top: 10,
      left: "calc(-50% + 16px)",
      right: "calc(50% + 16px)",
    },
    [`&.${stepConnectorClasses.active}`]: {
      [`& .${stepConnectorClasses.line}`]: {
        borderColor: "#ED1C24",
      },
    },
    [`&.${stepConnectorClasses.completed}`]: {
      [`& .${stepConnectorClasses.line}`]: {
        borderColor: "#ED1C24",
      },
    },
    [`& .${stepConnectorClasses.line}`]: {
      borderColor:
        theme.palette.mode === "dark" ? theme.palette.grey[800] : "#eaeaf0",
      borderTopWidth: 3,
      borderRadius: 1,
    },
  }));
  const QontoStepIconRoot = styled("div")(({ theme, ownerState }) => ({
    color: theme.palette.mode === "dark" ? theme.palette.grey[700] : "#eaeaf0",
    display: "flex",
    height: 22,
    alignItems: "center",
    ...(ownerState.active && {
      color: "#784af4",
    }),
    "& .QontoStepIcon-completedIcon": {
      color: "#71CE7B",
      zIndex: 1,
      fontSize: 18,
    },
    "& .QontoStepIcon-circle": {
      width: 20,
      height: 20,
      borderRadius: "50%",
      backgroundColor: "#fff",
      border: "1px solid #979797",
    },
  }));
  function QontoStepIcon(props) {
    const { active, completed, className } = props;

    return (
      <QontoStepIconRoot ownerState={{ active }} className={className}>
        {completed ? (
          <FaCheckCircle className="QontoStepIcon-completedIcon" />
        ) : (
          <div className="QontoStepIcon-circle" />
        )}
      </QontoStepIconRoot>
    );
  }

  return (
    <>
      <div className="position-relative">
        <Box
          className="card-cs px-md-5 px-2 py-4 my-4 step-form-cs"
          sx={{ maxWidth: 700, margin: "auto" }}
        >
          <Stepper
            activeStep={activeStep}
            orientation="vertical"
            connector={<QontoConnector />}
          >
            {steps.map((step, index) => {
              return stepForm.simType === "Physical" ? (
                <Step key={step.label}>
                  <StepLabel
                    StepIconComponent={QontoStepIcon}
                    onClick={() => handleNext(index)}
                  >
                    {step.label}
                  </StepLabel>
                  <StepContent>
                    <p>{step.description}</p>
                  </StepContent>
                </Step>
              ) : (
                step.label !== "Delivery Option" && (
                  <Step key={step.label}>
                    <StepLabel
                      StepIconComponent={QontoStepIcon}
                      onClick={() => handleNext(index)}
                    >
                      {step.label}
                    </StepLabel>
                    <StepContent>
                      <p>{step.description}</p>
                    </StepContent>
                  </Step>
                )
              );
            })}
          </Stepper>

          <Box className="text-center mt-3">
            <button
              onClick={price > 0 ? handlePay : handleSubmit}
              className=" btn-confirm"
              disabled={
                SubmitLoader ||
                activeStep !==
                  steps.length - (stepForm.simType === "Physical" ? 1 : 2)
              }
            >
              {SubmitLoader ? (
                <svg
                  style={{ width: "22px" }}
                  viewBox="0 0 100 100"
                  preserveAspectRatio="xMidYMid"
                >
                  <circle
                    cx="50"
                    cy="50"
                    fill="none"
                    stroke="#ffffff"
                    strokeWidth="10"
                    r="35"
                    strokeDasharray="164.93361431346415 56.97787143782138"
                  >
                    <animateTransform
                      attributeName="transform"
                      type="rotate"
                      repeatCount="indefinite"
                      dur="1s"
                      values="0 50 50;360 50 50"
                      keyTimes="0;1"
                    />
                  </circle>
                </svg>
              ) : (
                "Place your order and Pay"
              )}
            </button>
          </Box>
        </Box>

        {/* Sucessfull Order Modal */}
        <Modal
          aria-labelledby="transition-modal-title"
          aria-describedby="transition-modal-description"
          open={open}
          onClose={handleClose}
          closeAfterTransition
          BackdropComponent={Backdrop}
          BackdropProps={{
            timeout: 500,
          }}
        >
          <Fade in={open}>
            <Box className="congratulations-modal">
              <div className="get-paln-inner px-md-5">
                <div className="text-center p-md-4">
                  <img className="tick-icon" src="../images/tick.svg" />
                  <h5 className="modal-heading">Congratulations</h5>
                  <div className="congrt-text my-3">
                    <p className="color-AFAFB1 mb-1">
                      Your order has been placed successfully
                    </p>
                    <p className="">Accountype : {params.plan_title}</p>
                    <p className="">MSISDN : {stepForm.msisdn}</p>
                    <p className="">Status : Completed</p>
                    <p className="">Product Name : {productData.name}</p>
                    <p className="transaction-id">
                      Transaction ID : {transactionId}
                    </p>
                  </div>
                  <Box className="text-center">
                    <button
                      onClick={() => navigate(`/${params.plan_title}-plan`)}
                      className="primary-btn"
                    >
                      OK
                    </button>
                  </Box>
                </div>
              </div>
            </Box>
          </Fade>
        </Modal>

        {/* Find Store  Modal*/}
        <Modal
          aria-labelledby="transition-modal-title"
          aria-describedby="transition-modal-description"
          open={anotherModal}
          onClose={handleClose}
          closeAfterTransition
          BackdropComponent={Backdrop}
          BackdropProps={{
            timeout: 500,
          }}
        >
          <Fade in={anotherModal}>
            <Box className="congratulations-modal">
              <div className="get-paln-inner">
                <div className="text-center p-md-4">
                  <div className="mapouter">
                    <div className="gmap_canvas">
                      <iframe
                        width="100%"
                        height="510"
                        id="gmap_canvas"
                        src="https://maps.google.com/maps?q=california&t=&z=10&ie=UTF8&iwloc=&output=embed"
                        frameborder="0"
                        scrolling="no"
                        marginheight="0"
                        marginwidth="0"
                      ></iframe>
                    </div>
                  </div>

                  <Box className="text-center">
                    <button
                      onClick={() => setAnotherModal(false)}
                      className="primary-btn"
                    >
                      OK
                    </button>
                  </Box>
                </div>
              </div>
            </Box>
          </Fade>
        </Modal>

        <img
          className="step-form-img"
          src="../images/sim-man.png"
          alt="step-form-img"
        />
      </div>
    </>
  );
};
export default StepForm;
