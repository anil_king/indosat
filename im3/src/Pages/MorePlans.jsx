import Carousel from "react-multi-carousel";
import "react-multi-carousel/lib/styles.css";
import React, { useEffect, useState } from "react";
import { Container, Row, Col } from "react-bootstrap";
import { useNavigate, useParams } from "react-router";
import { useDispatch, useSelector } from "react-redux";
import NavigateNextIcon from "@mui/icons-material/NavigateNext";
import { Box, Typography, Link, Breadcrumbs } from "@mui/material";

import Popup from "../components/prepaid/Popup";
import { getMorePlans } from "../redux/action/prepaidPlanActions";
import { addCart } from "../redux/action/cartAction";
import { toast } from "react-hot-toast";

const responsive = {
  superLargeDesktop: {
    // the naming can be any, depends on you.
    breakpoint: { max: 4000, min: 3000 },
    items: 5,
  },
  desktop: {
    breakpoint: { max: 3000, min: 1024 },
    items: 4,
  },
  tablet: {
    breakpoint: { max: 1024, min: 654 },
    items: 2,
  },
  mobile: {
    breakpoint: { max: 654, min: 0 },
    items: 1,
  },
};

const MorePlans = () => {
  const params = useParams();
  const navigate = useNavigate();
  const dispatch = useDispatch();

  const [planID, setPlanID] = useState({});
  const [open, setOpen] = useState(false);

  const morePlans = useSelector((state) => state.plansReducer.morePlans);
  const cartVal = useSelector((state) => state.cartReducer.cart);

  const handleOpen = (id) => {
    setPlanID(id);
    setOpen(true);
  };
  const handleClose = () => setOpen(false);

  const handleCart = (id) => {
    if (cartVal.includes(id)) {
      toast.error("plan already exist in cart!");
    } else {
      dispatch(addCart(id));
    }
  };

  useEffect(() => {
    dispatch(getMorePlans());
  }, [dispatch]);

  const breadcrumbs = [
    <Link
      underline="hover"
      key="1"
      color="inherit"
      onClick={() => navigate("/")}
    >
      Home
    </Link>,
    <Typography key="2" color="#ED1C24">
      More Plans
    </Typography>,
  ];

  return (
    <>
      <Box className="prepaid-plan-banner bg-FCD401" sx={{ flexGrow: 1 }}>
        <Container className="">
          <Row className="align-items-center" container spacing={2}>
            <Col className="" md={6}>
              <h2 className="home-banner-title text-capitalize text-center pt-3">
                IM3 {params.plan_title} Plan{" "}
              </h2>
            </Col>
            <Col className="text-center" md={6}>
              <img
                className="prepaid-plan-banner-img"
                src="../images/sim-m.png"
                alt="pp"
              />
            </Col>
          </Row>
        </Container>
      </Box>

      <Container>
        <Breadcrumbs
          className="mt-4"
          separator={<NavigateNextIcon fontSize="small" />}
          aria-label="breadcrumb"
        >
          {breadcrumbs}
        </Breadcrumbs>
        <Box className="my-3">
          <Carousel
            removeArrowOnDeviceType={[
              "tablet",
              "mobile",
              "desktop",
              "superLargeDesktop",
            ]}
            autoPlay
            responsive={responsive}
          >
            {morePlans.map((item) => {
              return (
                <div key={item.Productid} className="plan-card ">
                  <div className="product_card-body flex-body">
                    <div className="benefit mb-4">
                      <img
                        className="choose-bonus-img"
                        src="../images/icon_socmedgopay.png"
                        alt="cupon"
                      />
                      <h5 className="upper-title">{item.name}</h5>
                      <div className="price mb-3 fixed-h">
                        <h6 className="binding-price">
                          {item.currency} {item.price}
                        </h6>
                      </div>
                      <div
                        className="d-flex align-items-end"
                        show="displayitems.validity"
                      >
                        <picture>
                          <img
                            src="https://im3-img.indosatooredoo.com/indosatassets/images/personal/periode.svg"
                            alt="..."
                            title=".."
                          />
                        </picture>
                        <p className="ml-2 mb-0 plan-list plan-list">
                          Validity{" "}
                          <span className="binding">
                            {item.validity_days} Days{" "}
                          </span>
                        </p>
                      </div>

                      <div
                        className="d-flex align-items-end hide"
                        show="pack.mainquota!=null &amp;&amp; displayitems.dailyquota"
                      >
                        <picture>
                          <img
                            src="https://im3-img.indosatooredoo.com/indosatassets/images/personal/quota.svg"
                            alt="..."
                            title=".."
                          />
                        </picture>
                        <p className="ml-2 mb-0 plan-list">
                          Data quota{" "}
                          <span className="binding">
                            {" "}
                            {item.allowance.data.amount}{" "}
                            {item.allowance.data.units} /{" "}
                            {item.allowance.data.frequency}
                          </span>
                        </p>
                      </div>
                      {/* <br/> */}

                      {/* <div
                        className="d-flex align-items-end"
                        show="pack.mainquota!=null &amp;&amp; displayitems.mainquota"
                      >
                        <picture>
                          <img
                            src="https://im3-img.indosatooredoo.com/indosatassets/images/personal/quota.svg"
                            alt="..."
                            title=".."
                          />
                        </picture>
                        <p className="ml-2 mb-0 plan-list">
                          Main quota <span className="binding">2 GB</span>
                        </p>
                      </div> */}
                      {/* <div
                        className="d-flex align-items-end"
                        show="displayitems.pulsasafe"
                      >
                        <picture>
                          <img
                            src="https://im3-img.indosatooredoo.com/indosatassets/images/personal/pulsa-safe.svg"
                            alt="..."
                            title=".."
                          />
                        </picture>
                        <p className="ml-2 mb-0 plan-list">Pulsa Safe</p>
                      </div> */}
                      <div
                        className="d-flex align-items-end hide"
                        show="pack.call!=null &amp;&amp; displayitems.call"
                      >
                        <picture>
                          <img
                            src="https://im3-img.indosatooredoo.com/indosatassets/images/personal/call.svg"
                            alt="..."
                            title=".."
                          />
                        </picture>
                        <p className="ml-2 mb-0 plan-list">
                          Call{" "}
                          <span className="binding">
                            {" "}
                            {item.allowance.voice.amount}{" "}
                            {item.allowance.voice.units} /{" "}
                            {item.allowance.voice.frequency}
                          </span>{" "}
                        </p>
                      </div>

                      {/* <div
                        className="d-flex align-items-end hide"
                        show="pack.calltoindosat!=null &amp;&amp; displayitems.calltoindosat"
                      >
                        <picture>
                          <img
                            src="https://im3-img.indosatooredoo.com/indosatassets/images/personal/call.svg"
                            alt="..."
                            title=".."
                          />
                        </picture>
                        <p className="ml-2 mb-0 plan-list">
                          Call to Indosat <span className="binding"> </span>{" "}
                        </p>
                      </div>

                      <div
                        className="d-flex align-items-end hide"
                        show="pack.calltoother!=null &amp;&amp; displayitems.calltoother"
                      >
                        <picture>
                          <img
                            src="https://im3-img.indosatooredoo.com/indosatassets/images/personal/call.svg"
                            alt="..."
                            title=".."
                          />
                        </picture>
                        <p className="ml-2 mb-0 plan-list">
                          Call to Others <span className="binding"> </span>{" "}
                        </p>
                      </div> */}

                      <div
                        className="d-flex align-items-end hide"
                        show="pack.sms!=null    &amp;&amp; displayitems.sms"
                      >
                        <picture>
                          <img
                            src="https://im3-img.indosatooredoo.com/indosatassets/images/personal/sms.svg"
                            alt="..."
                            title=".."
                          />
                        </picture>
                        <p className="ml-2 mb-0 plan-list">
                          SMS{" "}
                          <span bind="pack.sms" className="binding">
                            {" "}
                            {item.allowance.sms.amount}{" "}
                            {item.allowance.sms.units} /{" "}
                            {item.allowance.sms.frequency}
                          </span>{" "}
                        </p>
                      </div>
                      {/* <div
                        className="d-flex align-items-end hide"
                        show="pack.calltoim3!=null    &amp;&amp; displayitems.calltoim3"
                      >
                        <picture>
                          <img
                            src="https://im3-img.indosatooredoo.com/indosatassets/images/personal/call.svg"
                            alt="..."
                            title=".."
                          />
                        </picture>
                        <p className="ml-2 mb-0 plan-list">
                          <span className="binding"> </span> Call to IM3 Ooredoo{" "}
                        </p>
                      </div>
                      <div
                        className="d-flex align-items-end hide"
                        show="pack.smstoim3!=null  &amp;&amp; displayitems.smstoim3"
                      >
                        <picture>
                          <img
                            src="https://im3-img.indosatooredoo.com/indosatassets/images/personal/sms.svg"
                            alt="..."
                            title=".."
                          />
                        </picture>
                        <p className="ml-2 mb-0 plan-list">
                          <span className="binding"> </span> SMS to IM3 Ooredoo{" "}
                        </p>
                      </div>
                      <div
                        className="d-flex align-items-end hide"
                        show="pack.appquota!=null  &amp;&amp; displayitems.appquota"
                      >
                        <picture>
                          <img
                            src="https://im3-img.indosatooredoo.com/indosatassets/images/personal/quota-app.svg"
                            alt="..."
                            title=".."
                          />
                        </picture>
                        <p className="ml-2 mb-0 plan-list">
                          App quota <span className="binding"></span>{" "}
                        </p>
                      </div>
                      <div
                        className="d-flex align-items-end hide"
                        show="pack.streamquota!=null  &amp;&amp; displayitems.streamquota"
                      >
                        <picture>
                          <img
                            src="https://im3-img.indosatooredoo.com/indosatassets/images/personal/streaming-app.svg"
                            alt="..."
                            title=".."
                          />
                        </picture>
                        <p className="ml-2 mb-0 plan-list">
                          $$Streaming quota$$ <span className="binding"></span>{" "}
                        </p>
                      </div>
                      <div
                        className="d-flex align-items-end hide"
                        show="pack.speedbooster!=null    &amp;&amp; displayitems.speedbooster"
                      >
                        <picture>
                          <img
                            src="https://im3-img.indosatooredoo.com/indosatassets/images/personal/speed-booster.svg"
                            alt="..."
                            title=".."
                          />
                        </picture>
                        <p className="ml-2 mb-0 plan-list">
                          Speed booster <span className="binding"></span>{" "}
                        </p>
                      </div>
                      <div
                        className="d-flex align-items-end hide"
                        show="pack.impoinbonus !=null    &amp;&amp; displayitems.impoinbonus "
                      >
                        <picture>
                          <img
                            src="https://im3-img.indosatooredoo.com/indosatassets/images/personal/impoin.svg"
                            alt="..."
                            title=".."
                          />
                        </picture>
                        <p className="ml-2 mb-0 plan-list">
                          <span className="binding"> point</span> Bonus IMPoin
                        </p>
                      </div>
                      <div
                        className="d-flex align-items-end hide"
                        show="pack.impoinreward !=null   &amp;&amp; displayitems.impoinreward  "
                      >
                        <picture>
                          <img
                            src="https://im3-img.indosatooredoo.com/indosatassets/images/personal/impoin.svg"
                            alt="..."
                            title=".."
                          />
                        </picture>
                        <p className="ml-2 mb-0 plan-list">
                          <span className="binding"> point </span> rewards
                          IMPoin{" "}
                        </p>
                      </div> */}

                      {/* <div
                        className="d-flex align-items-end scope"
                        show="displayitems.cbenfit "
                        repeat="cbenfit in pack.customitems"
                      >
                        <picture>
                          <img
                            src="https://im3-img.indosatooredoo.com/indosatassets/images/personal/info.svg"
                            alt="..."
                            title=".."
                          />
                        </picture>
                        <p className="ml-2 mb-0 plan-list binding">
                          Available at *123*111#, myIM3 apps, IM3 Official
                          WhatsApp, e-Commerce and outlets{" "}
                          <span className="binding"> </span>
                        </p>
                      </div>

                      <div
                        className="d-flex align-items-end scope"
                        show="displayitems.cbenfit "
                        repeat="cbenfit in pack.customitems"
                      >
                        <picture>
                          <img
                            src="https://im3-img.indosatooredoo.com/indosatassets/images/personal/info.svg"
                            alt="..."
                            title=".."
                          />
                        </picture>
                        <p className="ml-2 mb-0 plan-list binding">
                          Call to IM3 and Tri{" "}
                          <span className="binding">
                            Limitless (5,000 min){" "}
                          </span>
                        </p>
                      </div>

                      <div
                        className="d-flex align-items-end hide"
                        show="pack.localquota!=null  &amp;&amp; displayitems.localquota"
                      >
                        <picture>
                          <img
                            src="https://im3-img.indosatooredoo.com/indosatassets/images/personal/quota.svg"
                            alt="..."
                            title=".."
                          />
                        </picture>
                        <p className="ml-2 mb-0 plan-list">
                          Local quota <span className="binding"> </span>
                        </p>
                      </div>

                      <div
                        className="d-flex align-items-end hide"
                        show="pack.midnightquota!=null &amp;&amp; displayitems.midnightquota"
                      >
                        <picture>
                          <img
                            src="https://im3-img.indosatooredoo.com/indosatassets/images/personal/quota.svg"
                            alt="..."
                            title=".."
                          />
                        </picture>
                        <p className="ml-2 mb-0 plan-list">
                          Night quota <span className="binding"> </span>
                        </p>
                      </div>

                      <div
                        className="d-flex align-items-end hide"
                        show="pack.midnightquota!=null  &amp;&amp; displayitems.extramidnightquota"
                      >
                        <picture>
                          <img
                            src="https://im3-img.indosatooredoo.com/indosatassets/images/personal/quota.svg"
                            alt="..."
                            title=".."
                          />
                        </picture>
                        <p className="ml-2 mb-0 plan-list">
                          Extra night quota <span className="binding"> </span>
                        </p>
                      </div> */}
                    </div>
                    {/* <button
                      className={`${
                        cartVal.includes(item.Productid)
                          ? "secondary-btn-outlined"
                          : "primary-btn-outlined  cursor-pointer"
                      } my-3  px-5 `}
                      disabled={cartVal.includes(item.Productid)}
                      onClick={() =>
                        !cartVal.includes(item.Productid) &&
                        handleCart(item.Productid)
                      }
                    >
                      Add To Cart
                    </button> */}
                    <div className="btn-link-cont d-flex justify-content-between align-items-center">
                      <button
                        onClick={() => handleOpen(item)}
                        className="primary-btn px-4"
                      >
                        CHOOSE
                      </button>
                      <h6 className="mb-0">SEE DETAILS</h6>
                    </div>
                  </div>
                </div>
              );
            })}
          </Carousel>
        </Box>
        <Box className="text-center mb-3">
          <button
            className="primary-btn"
            onClick={() => navigate("/prepaid-plan")}
          >
            Go Back
          </button>
        </Box>
      </Container>

      {/* Popup */}
      <Popup
        open={open}
        handleClose={handleClose}
        planData={planID}
        dynamicPath={params.plan_title}
      />
    </>
  );
};

export default MorePlans;
