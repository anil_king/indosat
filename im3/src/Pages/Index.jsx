import React from "react";
import { Box } from "@mui/material";
import { useNavigate } from "react-router";

const Index = () => {
  const navigate = useNavigate();
  return (
    <div>
      <Box
        onClick={() => navigate("/prepaid-plan")}
        className="home-banner hb-cursor position-relative"
        sx={{ flexGrow: 1 }}
      >
        <img className="" src="../images/close-up-of-girl.png" alt="cupon" />
        <div className="baneer-title">
          <h2>IM3 Prepaid Plan</h2>
        </div>
      </Box>

      <Box
        onClick={() => navigate("/postpaid-plan")}
        className="home-banner hb-cursor position-relative"
        sx={{ flexGrow: 1 }}
      >
        <img className="" src="../images/postpaid.png" alt="cupon" />
        <div className="baneer-title-r">
          <h2>IM3 Postpaid Plan</h2>
        </div>
      </Box>

      <Box
        onClick={() => navigate("/postpaid-activation")}
        className="home-banner hb-cursor position-relative"
        sx={{ flexGrow: 1 }}
      >
        <img className="" src="../images/starter.png" alt="cupon" />
        <div className="baneer-title">
          <h2>Postpaid Activation</h2>
        </div>
      </Box>

      <Box
        onClick={() => navigate("/postpaid-contract-renewal")}
        className="home-banner hb-cursor position-relative"
        sx={{ flexGrow: 1 }}
      >
        <img className="" src="../images/prepaid-starter.png" alt="cupon" />
        <div className="baneer-title-r">
          <h2>IM3 Postpaid – Contract Renewal</h2>
        </div>
      </Box>

      <Box
        onClick={() => navigate("/upgrade-prepaid-to-postpaid")}
        className="home-banner hb-cursor position-relative"
        sx={{ flexGrow: 1 }}
      >
        <img
          className=""
          src="../images/Upgrade Prepaid To Postpaid.png"
          alt="cupon"
        />
        <div className="baneer-title">
          <h2>IM3 Postpaid – Upgrade Prepaid To Postpaid</h2>
        </div>
      </Box>
    </div>
  );
};

export default Index;
