import React from "react";
import {
  Grid,
  Box,
  Modal,
  Fade,
  Backdrop,
  Radio,
  RadioGroup,
  FormControlLabel,
  FormControl,
  Stack,
  styled,
  Button,
} from "@mui/material";
import { useSelector } from "react-redux";
import { useNavigate } from "react-router";
import { MdAccessTime } from "react-icons/md";
import { FaCheckCircle } from "react-icons/fa";
import { FiUpload, FiXCircle } from "react-icons/fi";
import { Container, Row, Col } from "react-bootstrap";

const style = {
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: 700,
  bgcolor: "background.paper",
  border: "2px solid #000",
  boxShadow: 24,
  pt: 2,
  px: 4,
  pb: 3,
};

const BpIcon = styled("span")(({ theme }) => ({
  borderRadius: "50%",
  width: 16,
  height: 16,
  boxShadow:
    theme.palette.mode === "dark"
      ? "0 0 0 1px rgb(16 22 26 / 40%)"
      : "inset 0 0 0 1px rgba(16,22,26,.2), inset 0 -1px 0 rgba(16,22,26,.1)",
  backgroundColor: theme.palette.mode === "dark" ? "#394b59" : "#f5f8fa",
  backgroundImage:
    theme.palette.mode === "dark"
      ? "linear-gradient(180deg,hsla(0,0%,100%,.05),hsla(0,0%,100%,0))"
      : "linear-gradient(180deg,hsla(0,0%,100%,.8),hsla(0,0%,100%,0))",
  ".Mui-focusVisible &": {
    outline: "2px auto #ed1c23",
    outlineOffset: 2,
  },
  "input:hover ~ &": {
    backgroundColor: theme.palette.mode === "dark" ? "#30404d" : "#ed1c23",
  },
  "input:disabled ~ &": {
    boxShadow: "none",
    background:
      theme.palette.mode === "dark"
        ? "rgba(57,75,89,.5)"
        : "rgba(206,217,224,.5)",
  },
}));

const BpCheckedIcon = styled(BpIcon)({
  backgroundColor: "#ed1c23",
  backgroundImage:
    "linear-gradient(180deg,hsla(0,0%,100%,.1),hsla(0,0%,100%,0))",
  "&:before": {
    display: "block",
    width: 16,
    height: 16,
    backgroundImage: "radial-gradient(#fff,#fff 28%,transparent 32%)",
    content: '""',
  },
  "input:hover ~ &": {
    backgroundColor: "#ed1c23",
  },
});

function BpRadio(props) {
  return (
    <Radio
      disableRipple
      color="default"
      checkedIcon={<BpCheckedIcon />}
      icon={<BpIcon />}
      {...props}
    />
  );
}

function ChildModal() {
  const [open, setOpen] = React.useState(false);
  const handleOpen = () => {
    setOpen(true);
  };
  const handleClose = () => {
    setOpen(false);
  };

  return (
    <React.Fragment>
      <h4 className="add-ekyc" onClick={handleOpen}>
        e-KYC
      </h4>
      <Modal
        open={open}
        onClose={handleClose}
        aria-labelledby="child-modal-title"
        aria-describedby="child-modal-description"
      >
        <Box sx={{ ...style }}>
          <div className="">
            <iframe
              title="verifier"
              width="100%"
              height="500"
              src="https://kyc.sandboxing.tech/"
            ></iframe>
          </div>
          <Button onClick={handleClose}>Close</Button>
        </Box>
      </Modal>
    </React.Fragment>
  );
}

const PostpaidPlan = () => {
  const navigate = useNavigate();
  const [open, setOpen] = React.useState(false);
  const [openDetail, setOpenDetail] = React.useState(false);
  const morePlans = useSelector((state) => state.plansReducer.morePlans);
  const handleOpen = () => {
    setOpen(true);
  };
  const handleClose = () => setOpen(false);
  const handleOpenDetail = () => {
    setOpenDetail(true);
  };
  const handleCloseDetail = () => setOpenDetail(false);

  return (
    <div>
      <Box className="prepaid-plan-banner" sx={{ flexGrow: 1 }}>
        <Container className="h-100">
          <Grid className="h-100 align-items-center" container spacing={2}>
            <Grid className="" item xs={6}>
              <h2 className="home-banner-title text-center">
                IM3 Postpaid Plan
              </h2>
            </Grid>
            <Grid className="" item xs={6}></Grid>
          </Grid>
        </Container>
      </Box>
      <Box className="best-offer py-4">
        <Container>
          {/* <h3 className="primary-heading">
            Our Best Offer (Pre - Select Plan)
          </h3>
          <h3 className="plan-text py-3">**Plan IOH- 250**</h3> */}
          <div className="plan-benefits">
            <img
              className="coupon"
              src="../images/choose_number.png"
              alt="cupon"
            />

            <Row className="mx-md-4">
              <Col md={6}>
                <Box>
                  <h3 className="secondary-heading mb-3">
                    CHOOSE YOUR OWN BONUS
                  </h3>
                  <Box className="">
                    <Row>
                      <Col className="mb-2" md="4">
                        <Box className="white-card h-100">
                          <img
                            className="choose-bonus-img"
                            src="../images/ico_tokopedia.png"
                            alt="cupon"
                          />
                          <p className="choose-bonus-text">
                            Tokopedia Voucher up to Rp100.000
                          </p>
                        </Box>
                      </Col>
                      <Col className="mb-2" md="4">
                        <Box className="white-card h-100">
                          <img
                            className="choose-bonus-img"
                            src="../images/icon_socmedgopay.png"
                            alt="cupon"
                          />
                          <p className="choose-bonus-text">
                            Saldo GoPay up to Rp100.000
                          </p>
                        </Box>
                      </Col>
                      <Col className="mb-2" md="4">
                        <Box className="white-card h-100">
                          <img
                            className="choose-bonus-img"
                            src="../images/ovo.png"
                            alt="cupon"
                          />
                          <p className="choose-bonus-text">
                            Saldo OVO up to Rp100.000
                          </p>
                        </Box>
                      </Col>
                    </Row>
                  </Box>
                </Box>
              </Col>
              <Col md={6}>
                <Box className="white-card px-4 py-4">
                  <h3 className="secondary-heading mb-3">More Benefits</h3>
                  <ul className="p-0">
                    <li className="py-2">
                      <h6 className="d-flex align-items-center mb-0">
                        <FaCheckCircle className="check-green me-3" />
                        Billing Discount up to 25% for Google Play
                      </h6>
                    </li>
                    <li className="py-2">
                      <h6 className="d-flex align-items-center mb-0">
                        <FaCheckCircle className="check-green me-3" />
                        100% Main Quota up to 200GB
                      </h6>
                    </li>
                    <li className="py-2">
                      <h6 className="d-flex align-items-center mb-0">
                        <FaCheckCircle className="check-green me-3" />
                        Carry Over Your Unused Quota With Data Rollover
                      </h6>
                    </li>
                    <li className="py-2">
                      <h6 className="d-flex align-items-center mb-0">
                        <FaCheckCircle className="check-green me-3" />
                        Call up to 300 Minutes to All Operators
                      </h6>
                    </li>
                    <li className="py-2">
                      <h6 className="d-flex align-items-center mb-0">
                        <FaCheckCircle className="check-green me-3" />
                        More Comfortable With Bill Safe
                      </h6>
                    </li>
                    <li className="py-2">
                      <h6 className="d-flex align-items-center mb-0">
                        <FaCheckCircle className="check-green me-3" />
                        Priority network & services
                      </h6>
                    </li>
                  </ul>
                  <Box className="text-center mt-5">
                    <button className="primary-btn" onClick={handleOpen}>
                      Subscribe Postpaid
                    </button>
                  </Box>
                  <Box className="text-center  mt-3">
                    <span
                      className="link-btn cursor-pointer"
                      onClick={() => navigate("/postpaid/more-plans")}
                    >
                      More Plans
                    </span>
                  </Box>
                </Box>

                <Box className="white-card px-4 py-4">
                  <h3 className="secondary-heading color-ED1C24 mb-3">
                    Best recommendations only for you
                  </h3>
                  <FormControl fullWidth>
                    <RadioGroup
                      fullWidth
                      aria-labelledby="demo-radio-buttons-group-label"
                      defaultValue="female"
                      name="radio-buttons-group"
                    >
                      <FormControlLabel
                        fullWidth
                        className="cs-radio mb-3 w-100"
                        value="female"
                        control={<Radio />}
                        label={
                          <div className="recm-card d-flex align-items-center justify-content-between w-100">
                            <div className="d-flex">
                              <img
                                className="choose-bonus-img me-2"
                                src="../images/icon_socmedgopay.png"
                                alt="cupon"
                              />
                              <div className="py-2">
                                <h6>Prime 70 - 15GB</h6>
                                <small className="d-flex align-items-center">
                                  {" "}
                                  <MdAccessTime />6 months contract
                                </small>
                                <h6>
                                  <del className="me-2 color-AFAFB1">
                                    Rp 333.000
                                  </del>
                                  Rp 300.000
                                </h6>
                              </div>
                            </div>
                            <button className="link-btn">Details</button>
                          </div>
                        }
                      />

                      <FormControlLabel
                        className="cs-radio mb-3 w-100"
                        value="female1"
                        control={<Radio />}
                        label={
                          <div className="recm-card d-flex align-items-center justify-content-between w-100">
                            <div className="d-flex">
                              <img
                                className="choose-bonus-img me-2"
                                src="../images/icon_socmedgopay.png"
                                alt="cupon"
                              />
                              <div className="py-2">
                                <h6>Prime 70 - 15GB</h6>
                                <small className="d-flex align-items-center">
                                  <MdAccessTime />6 months contract
                                </small>
                                <h6>
                                  <del className="me-2 color-AFAFB1">
                                    Rp 333.000
                                  </del>
                                  Rp 300.000
                                </h6>
                              </div>
                            </div>
                            <button
                              className="link-btn"
                              onClick={handleOpenDetail}
                            >
                              Details
                            </button>
                          </div>
                        }
                      />
                    </RadioGroup>
                  </FormControl>
                  <Box className="text-center mt-3">
                    <button className="primary-btn" onClick={handleOpen}>
                      Buy
                    </button>
                  </Box>
                </Box>
              </Col>
            </Row>
          </div>
        </Container>
      </Box>
      {/* 1 */}
      <Modal
        // aria-labelledby="transition-modal-title"
        // aria-describedby="transition-modal-description"
        open={openDetail}
        onClose={handleCloseDetail}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 500,
        }}
      >
        <Fade in={openDetail}>
          <Box className="get-plan-modal">
            <div className="get-paln-inner">
              <Box className="text-end">
                <FiXCircle
                  onClick={() => handleCloseDetail()}
                  className="color-AFAFB1 cursor-pointer"
                />
              </Box>
              <div className="px-md-2">
                <h5 className="modal-heading">Prime 70 - 15GB</h5>
                <p>Select advance payment period</p>

                {morePlans.map((item) => {
                  return (
                    <div
                      className="d-flex align-items-end hide"
                      show="pack.sms!=null    &amp;&amp; displayitems.sms"
                    >
                      <picture>
                        <img
                          src="https://im3-img.indosatooredoo.com/indosatassets/images/personal/sms.svg"
                          alt="..."
                          title=".."
                        />
                      </picture>
                      <p className="ml-2 mb-0 plan-list">
                        SMS{" "}
                        <span bind="pack.sms" className="binding">
                          {" "}
                          {item.allowance.sms.amount} {item.allowance.sms.units}{" "}
                          / {item.allowance.sms.frequency}
                        </span>{" "}
                      </p>
                    </div>
                  );
                })}

                <p className="mb-0">MONTHLY SUBSCRIPTION PRICE</p>
                <h6>Rp 77.700</h6>
                <div className="p-md-4">
                  fghfhfghfghg
                  <h4 className="modal-heading">Upload KYC</h4>
                  <Box
                    onClick={() => navigate("/postpaid/step-form")}
                    className="text-center"
                  >
                    <button className="modal-btn">Choose</button>
                  </Box>
                </div>
              </div>
            </div>
          </Box>
        </Fade>
      </Modal>

      {/* 2 */}
      <Modal
        aria-labelledby="transition-modal-title"
        aria-describedby="transition-modal-description"
        open={open}
        onClose={handleClose}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 500,
        }}
      >
        <Fade in={open}>
          <Box className="get-plan-modal">
            <div className="get-paln-inner">
              <Box className="text-end">
                <FiXCircle
                  onClick={() => handleClose()}
                  className="color-AFAFB1 cursor-pointer"
                />
              </Box>
              <div className="p-md-4">
                <ChildModal />
                or
                <h4 className="modal-heading">Upload KYC</h4>
                <div className="">
                  <FormControl>
                    <RadioGroup
                      defaultValue="Resident"
                      row
                      aria-labelledby="demo-row-radio-buttons-group-label"
                      name="row-radio-buttons-group"
                    >
                      <FormControlLabel
                        value="Resident"
                        control={<BpRadio />}
                        label="Resident"
                      />
                      <FormControlLabel
                        value="Tourist"
                        control={<BpRadio />}
                        label="Tourist"
                      />
                    </RadioGroup>
                  </FormControl>
                  <Box className="upload-card p-3">
                    <div className="py-4 upload-text">
                      <p className="mb-0">Upload document</p>
                      <small>Supports png, jpeg, pdf (max size 2mb)</small>
                    </div>
                    <Stack direction="row" alignItems="center" spacing={2}>
                      <Box
                        className="upload-file"
                        color="primary"
                        aria-label="upload picture"
                        component="label"
                      >
                        <FiUpload className="color-f58a1f" />
                        <p className="m-0">Upload</p>
                        <input hidden accept="image/*" type="file" />
                      </Box>
                    </Stack>
                  </Box>
                </div>
                <Box
                  onClick={() => navigate("/postpaid/step-form")}
                  className="text-center"
                >
                  <button className="modal-btn">Submit</button>
                </Box>
              </div>
            </div>
          </Box>
        </Fade>
      </Modal>
    </div>
  );
};

export default PostpaidPlan;
