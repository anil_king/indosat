import React from "react";
import {
  Grid,
  Box,
  Modal,
  Fade,
  Backdrop,
  Radio,
  RadioGroup,
  FormControlLabel,
  FormControl,
  Stack,
  styled,
  Button,
  alpha,
  TextField,
} from "@mui/material";
import { useNavigate } from "react-router";
import { FiUpload, FiXCircle } from "react-icons/fi";
import { Container, Row, Col } from "react-bootstrap";

const style = {
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: 700,
  bgcolor: "background.paper",
  border: "2px solid #000",
  boxShadow: 24,
  pt: 2,
  px: 4,
  pb: 3,
};

const BpIcon = styled("span")(({ theme }) => ({
  borderRadius: "50%",
  width: 16,
  height: 16,
  boxShadow:
    theme.palette.mode === "dark"
      ? "0 0 0 1px rgb(16 22 26 / 40%)"
      : "inset 0 0 0 1px rgba(16,22,26,.2), inset 0 -1px 0 rgba(16,22,26,.1)",
  backgroundColor: theme.palette.mode === "dark" ? "#394b59" : "#f5f8fa",
  backgroundImage:
    theme.palette.mode === "dark"
      ? "linear-gradient(180deg,hsla(0,0%,100%,.05),hsla(0,0%,100%,0))"
      : "linear-gradient(180deg,hsla(0,0%,100%,.8),hsla(0,0%,100%,0))",
  ".Mui-focusVisible &": {
    outline: "2px auto #ed1c23",
    outlineOffset: 2,
  },
  "input:hover ~ &": {
    backgroundColor: theme.palette.mode === "dark" ? "#30404d" : "#ed1c23",
  },
  "input:disabled ~ &": {
    boxShadow: "none",
    background:
      theme.palette.mode === "dark"
        ? "rgba(57,75,89,.5)"
        : "rgba(206,217,224,.5)",
  },
}));

const BpCheckedIcon = styled(BpIcon)({
  backgroundColor: "#ed1c23",
  backgroundImage:
    "linear-gradient(180deg,hsla(0,0%,100%,.1),hsla(0,0%,100%,0))",
  "&:before": {
    display: "block",
    width: 16,
    height: 16,
    backgroundImage: "radial-gradient(#fff,#fff 28%,transparent 32%)",
    content: '""',
  },
  "input:hover ~ &": {
    backgroundColor: "#ed1c23",
  },
});

function BpRadio(props) {
  return (
    <Radio
      disableRipple
      color="default"
      checkedIcon={<BpCheckedIcon />}
      icon={<BpIcon />}
      {...props}
    />
  );
}
const RedditTextField = styled((props) => (
  <TextField InputProps={{ disableUnderline: true }} {...props} />
))(({ theme }) => ({
  "& .MuiFilledInput-root": {
    overflow: "hidden",
    borderRadius: 4,
    backgroundColor: theme.palette.mode === "light" ? "#F2F6F8" : "#F2F6F8",
    transition: theme.transitions.create([
      "border-color",
      "background-color",
      "box-shadow",
    ]),
    "&:hover": {
      backgroundColor: "#eeeeee",
    },
    "&.Mui-focused": {
      backgroundColor: "#eeeeee",
      boxShadow: `${alpha(theme.palette.primary.main, 0.25)} 0 0 0 2px`,
      borderColor: theme.palette.primary.main,
    },
  },
}));

function ChildModal() {
  const [open, setOpen] = React.useState(false);
  const handleOpen = () => {
    setOpen(true);
  };
  const handleClose = () => {
    setOpen(false);
  };

  return (
    <React.Fragment>
      <h4 className="add-ekyc" onClick={handleOpen}>
        e-KYC
      </h4>
      <Modal
        open={open}
        onClose={handleClose}
        aria-labelledby="child-modal-title"
        aria-describedby="child-modal-description"
      >
        <Box sx={{ ...style }}>
          <div className="">
            <iframe
              width="100%"
              height="500"
              src="https://kyc.sandboxing.tech/"
            ></iframe>
          </div>
          <Button onClick={handleClose}>Close</Button>
        </Box>
      </Modal>
    </React.Fragment>
  );
}

const UpgradePrepaidtoPostpaid = () => {
  const navigate = useNavigate();
  const [open, setOpen] = React.useState(false);
  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);

  return (
    <div>
      <Box className="prepaid-activation bg-FCD401" sx={{ flexGrow: 1 }}>
        <Container className="h-100">
          <Grid className="h-100 align-items-center" container spacing={2}>
            <Grid className="" item xs={6}>
              <h2 className="home-banner-title text-center">
                Upgrade IM3 Prepaid to Postpaid
              </h2>
            </Grid>
            <Grid className="text-center" item xs={6}>
              <img
                className="activation-b-img"
                src="../images/img_personal.png"
                alt="cupon"
              />
            </Grid>
          </Grid>
        </Container>
      </Box>
      <Box className="best-offer py-4">
        <Container>
          {/* <h3 className="primary-heading">
            Our Best Offer (Pre - Select Plan)
          </h3>
          <h3 className="plan-text py-3">**Plan IOH- 250**</h3> */}
          <div className="plan-benefits">
            <img
              className="coupon"
              src="../images/choose_number.png"
              alt="cupon"
            />

            <Row className="mx-md-4">
              <Col md={6}>
                <Box>
                  <h3 className="secondary-heading mb-3">
                    Why You Should Switch To Postpaid
                  </h3>
                  <Box className="">
                    <Row>
                      <Col className="mb-2" md="4">
                        <Box className=" h-100">
                          <img
                            className="choose-bonus-img mb-3"
                            src="../images/ico_tokopedia.png"
                            alt="cupon"
                          />
                          <h6 className="mb-0 font-size-12">Priority</h6>
                          <p className="choose-bonus-text">
                            Network & services
                          </p>
                        </Box>
                      </Col>
                      <Col className="mb-2" md="4">
                        <Box className=" h-100">
                          <img
                            className="choose-bonus-img mb-3"
                            src="../images/icon_socmedgopay.png"
                            alt="cupon"
                          />
                          <h6 className="mb-0 font-size-12">Bill Safe</h6>
                          <p className="choose-bonus-text">
                            More comfortable & safe
                          </p>
                        </Box>
                      </Col>
                      <Col className="mb-2" md="4">
                        <Box className=" h-100">
                          <img
                            className="choose-bonus-img mb-3"
                            src="../images/ovo.png"
                            alt="cupon"
                          />
                          <h6 className="mb-0 font-size-12">
                            Disc Google Play
                          </h6>
                          <p className="choose-bonus-text">
                            Disc. 25% Google Play Billing
                          </p>
                        </Box>
                      </Col>
                    </Row>
                  </Box>
                </Box>
              </Col>
              <Col md={6}>
                <Box className="white-card px-4 py-4">
                  <p className="pt-3"> Input Prepaid Number</p>
                  <h3 className="secondary-heading mb-3">IM3 Prepaid number</h3>
                  <Box className="">
                    <RedditTextField
                      fullWidth
                      label="Postpaid number "
                      defaultValue="+62"
                      id="reddit-input"
                      variant="filled"
                      style={{ marginTop: 11 }}
                    />
                  </Box>
                  <small className="small-text">
                    Use active IM3 Postpaid number
                  </small>
                  <Box className="text-center mt-5">
                    <button className="primary-btn" onClick={handleOpen}>
                      Subscribe Postpaid
                    </button>
                  </Box>
                </Box>
              </Col>
            </Row>
          </div>
        </Container>
      </Box>

      <Modal
        aria-labelledby="transition-modal-title"
        aria-describedby="transition-modal-description"
        open={open}
        onClose={handleClose}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 500,
        }}
      >
        <Fade in={open}>
          <Box className="get-plan-modal">
            <div className="get-paln-inner">
              <Box className="text-end">
                <FiXCircle
                  onClick={() => handleClose()}
                  className="color-AFAFB1 cursor-pointer"
                />
              </Box>
              <div className="p-md-4">
                <ChildModal />
                or
                <h4 className="modal-heading">Upload KYC</h4>
                <div className="">
                  <FormControl>
                    <RadioGroup
                      defaultValue="Resident"
                      row
                      aria-labelledby="demo-row-radio-buttons-group-label"
                      name="row-radio-buttons-group"
                    >
                      <FormControlLabel
                        value="Resident"
                        control={<BpRadio />}
                        label="Resident"
                      />
                      <FormControlLabel
                        value="Tourist"
                        control={<BpRadio />}
                        label="Tourist"
                      />
                    </RadioGroup>
                  </FormControl>
                  <Box className="upload-card p-3">
                    <div className="py-4 upload-text">
                      <p className="mb-0">Upload document</p>
                      <small>Supports png, jpeg, pdf (max size 2mb)</small>
                    </div>
                    <Stack direction="row" alignItems="center" spacing={2}>
                      <Box
                        className="upload-file"
                        color="primary"
                        aria-label="upload picture"
                        component="label"
                      >
                        <FiUpload className="color-f58a1f" />
                        <p className="m-0">Upload</p>
                        <input hidden accept="image/*" type="file" />
                      </Box>
                    </Stack>
                  </Box>
                </div>
                <Box
                  onClick={() => navigate("/prepaid-to-postpaid/step-form")}
                  className="text-center"
                >
                  <button className="modal-btn">Submit</button>
                </Box>
              </div>
            </div>
          </Box>
        </Fade>
      </Modal>
    </div>
  );
};

export default UpgradePrepaidtoPostpaid;
