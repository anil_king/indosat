import React, { useState } from "react";
import {
  Box,
  Button,
  Modal,
  Radio,
  Fade,
  Backdrop,
  RadioGroup,
  FormControlLabel,
  FormControl,
  Stack,
  styled,
} from "@mui/material";

import { FiUpload, FiXCircle } from "react-icons/fi";
import { GrDocumentPdf } from "react-icons/gr";
import { MdOutlineDelete } from "react-icons/md";
import { AiOutlineEye } from "react-icons/ai";
import { useNavigate } from "react-router";
import { useDispatch, useSelector } from "react-redux";
import { uploadFile } from "../../redux/action/prepaidPlanActions";
import Loader from "../Loader";
import { toast } from "react-hot-toast";

const style = {
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: 700,
  bgcolor: "background.paper",
  border: "2px solid #000",
  boxShadow: 24,
  pt: 2,
  px: 4,
  pb: 3,
};

const BpIcon = styled("span")(({ theme }) => ({
  borderRadius: "50%",
  width: 16,
  height: 16,
  boxShadow:
    theme.palette.mode === "dark"
      ? "0 0 0 1px rgb(16 22 26 / 40%)"
      : "inset 0 0 0 1px rgba(16,22,26,.2), inset 0 -1px 0 rgba(16,22,26,.1)",
  backgroundColor: theme.palette.mode === "dark" ? "#394b59" : "#f5f8fa",
  backgroundImage:
    theme.palette.mode === "dark"
      ? "linear-gradient(180deg,hsla(0,0%,100%,.05),hsla(0,0%,100%,0))"
      : "linear-gradient(180deg,hsla(0,0%,100%,.8),hsla(0,0%,100%,0))",
  ".Mui-focusVisible &": {
    outline: "2px auto #ed1c23",
    outlineOffset: 2,
  },
  "input:hover ~ &": {
    backgroundColor: theme.palette.mode === "dark" ? "#30404d" : "#ed1c23",
  },
  "input:disabled ~ &": {
    boxShadow: "none",
    background:
      theme.palette.mode === "dark"
        ? "rgba(57,75,89,.5)"
        : "rgba(206,217,224,.5)",
  },
}));

const BpCheckedIcon = styled(BpIcon)({
  backgroundColor: "#ed1c23",
  backgroundImage:
    "linear-gradient(180deg,hsla(0,0%,100%,.1),hsla(0,0%,100%,0))",
  "&:before": {
    display: "block",
    width: 16,
    height: 16,
    backgroundImage: "radial-gradient(#fff,#fff 28%,transparent 32%)",
    content: '""',
  },
  "input:hover ~ &": {
    backgroundColor: "#ed1c23",
  },
});

function BpRadio(props) {
  return (
    <Radio
      disableRipple
      color="default"
      checkedIcon={<BpCheckedIcon />}
      icon={<BpIcon />}
      {...props}
    />
  );
}

function ChildModal() {
  const [open, setOpen] = React.useState(false);
  const handleOpen = () => {
    setOpen(true);
  };
  const handleClose = () => {
    setOpen(false);
  };

  return (
    <React.Fragment>
      <h4 className="add-ekyc" onClick={handleOpen}>
        e-KYC
      </h4>
      <Modal
        open={open}
        onClose={handleClose}
        aria-labelledby="child-modal-title"
        aria-describedby="child-modal-description"
      >
        <Box sx={{ ...style }}>
          <div className="">
            <iframe
              width="100%"
              height="500"
              src="https://kyc.sandboxing.tech/"
              title="sandboxing"
            ></iframe>
          </div>
          <Button onClick={handleClose}>Close</Button>
        </Box>
      </Modal>
    </React.Fragment>
  );
}

const Popup = (props) => {
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const [files, setFiles] = useState({});
  const { handleClose, open, planData, dynamicPath } = props;
  const apiLoading = useSelector((state) => state.plansReducer.apiLoading);

  // handleFile Upload
  const handleFileUpload = () => {
    const payload = new FormData();
    payload.append("file", files[0]);
    if (files[0]) {
      dispatch(
        uploadFile(payload, (id) => {
          navigate(`/${dynamicPath}/step-form`, {
            state: { productData: planData, fileId: id },
          });
        })
      );
    } else {
      toast.error("please upload documnet!");
    }
  };
  return (
    <Modal
      aria-labelledby="transition-modal-title"
      aria-describedby="transition-modal-description"
      open={open}
      onClose={handleClose}
      closeAfterTransition
      BackdropComponent={Backdrop}
      BackdropProps={{
        timeout: 500,
      }}
    >
      <Fade in={open}>
        <Box className="get-plan-modal">
          <div className="get-paln-inner">
            <Box className="text-end">
              <FiXCircle
                onClick={() => handleClose()}
                className="color-AFAFB1 cursor-pointer"
              />
            </Box>
            <div className="p-md-4">
              <ChildModal />
              or
              <h4 className="modal-heading">Upload KYC</h4>
              <div className="">
                <FormControl>
                  <RadioGroup
                    defaultValue="Resident"
                    row
                    aria-labelledby="demo-row-radio-buttons-group-label"
                    name="row-radio-buttons-group"
                  >
                    <FormControlLabel
                      value="Resident"
                      control={<BpRadio />}
                      label="Resident"
                    />
                    <FormControlLabel
                      value="Tourist"
                      control={<BpRadio />}
                      label="Tourist"
                    />
                  </RadioGroup>
                </FormControl>
                <Box className="upload-card p-3 mb-4 mt-2">
                  <div className="py-4 upload-text">
                    <p className="mb-0">Upload document</p>
                    <small>Supports png, jpeg, pdf (max size 2mb)</small>
                  </div>
                  <Stack direction="row" alignItems="center" spacing={2}>
                    <Box
                      className="upload-file"
                      color="primary"
                      aria-label="upload picture"
                      component="label"
                    >
                      <div className="">
                        <FiUpload className="color-f58a1f cursor-pointer" />
                        <p className="m-0">Upload</p>
                        <input
                          hidden
                          accept="image/*"
                          onChange={(e) => setFiles(e.target.files)}
                          type="file"
                        />
                      </div>
                    </Box>
                  </Stack>

                  {Object.values(files).map((item) => {
                    return (
                      <Box className="upload-file d-flex justify-content-between align-items-center px-2">
                        <div className="d-flex align-items-center">
                          <GrDocumentPdf className="color-FA5A53" />
                          <div className="uploaded-review text-start px-3">
                            <h6 className="mb-0">{item.name}</h6>
                            <p className="mb-0">1.2</p>
                            <small>upload completess</small>
                          </div>
                        </div>
                        <div className="">
                          <MdOutlineDelete
                            onClick={() => setFiles({})}
                            className="color-FA5A53 cursor-pointer"
                          />
                          {/* <AiOutlineEye className="color-0387D1 mx-2" /> */}
                        </div>
                      </Box>
                    );
                  })}
                </Box>
              </div>
              <Box className="text-center">
                <button
                  onClick={handleFileUpload}
                  disabled={apiLoading}
                  className="modal-btn"
                >
                  {" "}
                  {apiLoading ? <Loader /> : "Submit"}
                </button>
              </Box>
            </div>
          </div>
        </Box>
      </Fade>
    </Modal>
  );
};

export default Popup;
