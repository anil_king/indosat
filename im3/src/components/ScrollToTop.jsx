import  { useEffect } from "react";
import { useLocation } from "react-router";

const ScrollToTop = () => {
  const location = useLocation();
  
  useEffect(() => {
    window.scrollTo({ top: "0px", left: "0px", behavior: "smooth" });
  }, [location.pathname]);
};

export default ScrollToTop;
