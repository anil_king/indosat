import axios from "axios";

const API = axios.create({ baseURL: "https://mockocsapi.sandboxing.tech/" });

API.interceptors.response.use(
  (response) => response,
  (error) => {
    return Promise.reject(error);
  }
);

export default API;
