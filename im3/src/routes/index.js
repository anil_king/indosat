import React from "react";

import Index from "../Pages/Index";
import StepForm from "../Pages/StepForm";
import { Carousel } from "react-bootstrap";
import MorePlans from "../Pages/MorePlans";
import PrepaidPlan from "../Pages/PrepaidPlan";
import PostpaidPlan from "../Pages/PostpaidPlan";
import PostpaidActivation from "../Pages/PostpaidActivation";
import PostpaidContractRenewal from "../Pages/PostpaidContractRenewal";
import UpgradePrepaidtoPostpaid from "../Pages/UpgradePrepaidtoPostpaid";
import Cart from "../Pages/Cart";

const AllRoutes = [
  {
    name: "Home",
    path: "/",
    element: <Index />,
    exact: "true",
  },
  {
    name: "Prepaid Plan",
    path: "/prepaid-plan",
    element: <PrepaidPlan />,
    exact: "true",
  },
  {
    name: "Postpaid Plan",
    path: "/postpaid-plan",
    element: <PostpaidPlan />,
    exact: "true",
  },
  {
    name: "Postpaid Activation",
    path: "/postpaid-activation",
    element: <PostpaidActivation />,
    exact: "true",
  },
  {
    name: "Postpaid Contract Renewal",
    path: "/postpaid-contract-renewal",
    element: <PostpaidContractRenewal />,
    exact: "true",
  },
  {
    name: "Upgrade Prepaid to Postpaid",
    path: "/upgrade-prepaid-to-postpaid",
    element: <UpgradePrepaidtoPostpaid />,
    exact: "true",
  },
  {
    name: "Step Form",
    path: "/:plan_title/step-form",
    element: <StepForm />,
    exact: "true",
  },
  {
    name: "More Plans",
    path: "/:plan_title/more-plans",
    element: <MorePlans />,
    exact: "true",
  },
  {
    name: "Carousel",
    path: "/carousel",
    element: <Carousel />,
    exact: "true",
  },
  {
    name: "Shopping Cart",
    path: "/cart",
    element: <Cart/>,
    exact: "true",
  },
];

export default AllRoutes;
