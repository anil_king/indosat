import React from "react";
import { Toaster } from "react-hot-toast";
import { Routes, Route } from "react-router";
import "bootstrap/dist/css/bootstrap.min.css";

import "./App.css";
import "./style.css";
import AllRoutes from "./routes";
import Header from "./Layouts/Header";

const App = () => {
  return (
    <>
      <Toaster />
      <Header />
      <Routes>
        {AllRoutes.map((item) => {
          return (
            <Route
              key={item.name}
              path={item.path}
              element={item.element}
              exact={item.exact}
              name={item.name}
            />
          );
        })}
      </Routes>
    </>
  );
};

export default App;
