import { combineReducers } from "redux";

import plansReducer from "./plansReducer";
import cartReducer from "./cartReducer";

const rootReducers = combineReducers({ plansReducer, cartReducer });

export default rootReducers;
