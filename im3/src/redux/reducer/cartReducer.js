import {
  ADD_CART,
  ADD_CART_FAIL,
  ADD_CART_SUCCESS,
  DELETE_CART,
  DELETE_CART_FAIL,
  DELETE_CART_SUCCESS,
  GET_CART,
  GET_CART_FAIL,
  GET_CART_SUCCESS,
} from "../action";

const initialState = {
  cart: [],
  loading: false,
};

const cartReducer = (state = initialState, action) => {
  switch (action.type) {
    // Get Cart
    case GET_CART:
      return { ...state, loading: true };

    case GET_CART_SUCCESS:
      return { ...state, cart: action.payload, loading: false };

    case GET_CART_FAIL:
      return { ...state, loading: false };

    // Add Cart
    case ADD_CART:
      return { ...state, loading: true };

    case ADD_CART_SUCCESS:
      return { ...state, cart: action.payload, loading: false };

    case ADD_CART_FAIL:
      return { ...state, loading: false };

    // Delete Cart
    case DELETE_CART:
      return { ...state, loading: true };

    case DELETE_CART_SUCCESS:
      return { ...state, cart: action.payload, loading: false };

    case DELETE_CART_FAIL:
      return { ...state, loading: false };

    default:
      return state;
  }
};

export default cartReducer;
