import {
  APPLY_REDEEM_CODE,
  APPLY_REDEEM_CODE_FAIL,
  APPLY_REDEEM_CODE_SUCCESS,
  CREATE_CUSTOMER_PROXY,
  CREATE_CUSTOMER_PROXY_FAIL,
  CREATE_CUSTOMER_PROXY_SUCCESS,
  GET_MSIDN_NUMS,
  GET_MSIDN_NUMS_FAIL,
  GET_MSIDN_NUMS_SUCCESS,
  MORE_PLANS,
  MORE_PLANS_FAIL,
  MORE_PLANS_SUCCESS,
  UPDATE_DATA,
  UPDATE_DATA_FAIL,
  UPDATE_DATA_SUCCESS,
  UPLOAD_FILE,
  UPLOAD_FILE_FAIL,
  UPLOAD_FILE_SUCCESS,
} from "../action";

const initialState = {
  morePlans: [],
  msidnNums: [],
  checkBenefitCode: [],
  loading: false,
  apiLoading: false,
};

const plansReducer = (state = initialState, action) => {
  switch (action.type) {
    // GET MORE_PLANS
    case MORE_PLANS:
      return { ...state, loading: true };
    case MORE_PLANS_SUCCESS:
      return { ...state, morePlans: action.payload, loading: false };
    case MORE_PLANS_FAIL:
      return { ...state, loading: false };

    // GET GET_MSIDN_NUMS
    case GET_MSIDN_NUMS:
      return { ...state, loading: true };
    case GET_MSIDN_NUMS_SUCCESS:
      return { ...state, msidnNums: action.payload, loading: false };
    case GET_MSIDN_NUMS_FAIL:
      return { ...state, loading: false };

    // GET CREATE_CUSTOMER_PROXY
    case CREATE_CUSTOMER_PROXY:
      return { ...state, apiLoading: true };
    case CREATE_CUSTOMER_PROXY_SUCCESS:
      return { ...state, apiLoading: false };
    case CREATE_CUSTOMER_PROXY_FAIL:
      return { ...state, apiLoading: false };

    // GET APPLY_REDEEM_CODE
    case APPLY_REDEEM_CODE:
      return { ...state, loading: true };
    case APPLY_REDEEM_CODE_SUCCESS:
      return { ...state, checkBenefitCode: action.payload, loading: false };
    case APPLY_REDEEM_CODE_FAIL:
      return { ...state, loading: false };

    // UPLOAD_FILE
    case UPLOAD_FILE:
      return { ...state, apiLoading: true };
    case UPLOAD_FILE_SUCCESS:
      return { ...state, apiLoading: false };
    case UPLOAD_FILE_FAIL:
      return { ...state, apiLoading: false };

    // UPDATE_DATA
    case UPDATE_DATA:
      return { ...state, apiLoading: true };
    case UPDATE_DATA_SUCCESS:
      return { ...state, apiLoading: false };
    case UPDATE_DATA_FAIL:
      return { ...state, apiLoading: false };

    default:
      return state;
  }
};

export default plansReducer;
