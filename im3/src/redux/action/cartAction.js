import { toast } from "react-hot-toast";
import {
  ADD_CART,
  ADD_CART_FAIL,
  ADD_CART_SUCCESS,
  DELETE_CART,
  DELETE_CART_FAIL,
  DELETE_CART_SUCCESS,
  GET_CART,
  GET_CART_FAIL,
  GET_CART_SUCCESS,
} from ".";

// Get Cart
export const getCart = () => (dispatch) => {
  dispatch({ type: GET_CART });
  try {
    let arr = localStorage.getItem("cart")
      ? JSON.parse(localStorage.getItem("cart"))
      : [];
    dispatch({ type: GET_CART_SUCCESS, payload: arr });
  } catch (error) {
    dispatch({ type: GET_CART_FAIL });
  }
};

// Add Cart
export const addCart = (id) => (dispatch) => {
  dispatch({ type: ADD_CART });
  try {
    let arr = localStorage.getItem("cart")
      ? JSON.parse(localStorage.getItem("cart"))
      : [];
    if (!arr.includes(id) && arr.length === 0) {
      arr.push(id);
      localStorage.setItem("cart", JSON.stringify(arr));
      dispatch({ type: ADD_CART_SUCCESS, payload: arr });
      toast.success("plan added to cart!");
    }
  } catch (error) {
    dispatch({ type: ADD_CART_FAIL });
  }
};

// Delet Cart
export const deleteCart = () => (dispatch) => {
  dispatch({ type: DELETE_CART });
  try {
    localStorage.removeItem("cart");
    dispatch({ type: DELETE_CART_SUCCESS, payload: [] });
  } catch (error) {
    dispatch({ type: DELETE_CART_FAIL });
  }
};
