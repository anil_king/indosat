import {
  APPLY_REDEEM_CODE,
  APPLY_REDEEM_CODE_FAIL,
  APPLY_REDEEM_CODE_SUCCESS,
  CREATE_CUSTOMER_PROXY,
  CREATE_CUSTOMER_PROXY_SUCCESS,
  GET_MSIDN_NUMS,
  GET_MSIDN_NUMS_FAIL,
  GET_MSIDN_NUMS_SUCCESS,
  MORE_PLANS,
  MORE_PLANS_FAIL,
  MORE_PLANS_SUCCESS,
  UPDATE_DATA,
  UPDATE_DATA_FAIL,
  UPDATE_DATA_SUCCESS,
  UPLOAD_FILE,
  UPLOAD_FILE_FAIL,
  UPLOAD_FILE_SUCCESS,
} from ".";
import API from "../../config";
import toast from "react-hot-toast";
import { deleteCart } from "./cartAction";
import axios from "axios";

// fetch all products
export const getMorePlans = () => async (dispatch) => {
  dispatch({ type: MORE_PLANS });
  try {
    const response = await API.get(`fetchallproducts`);
    dispatch({ type: MORE_PLANS_SUCCESS, payload: response.data });
  } catch (error) {
    dispatch({ type: MORE_PLANS_FAIL });
  }
};

// get all msidn numbers
export const getMSIDNNums = () => async (dispatch) => {
  dispatch({ type: GET_MSIDN_NUMS });
  try {
    const response = await API.get(`fetchfreemsisdn_nms?limit=3&type=all`);
    dispatch({ type: GET_MSIDN_NUMS_SUCCESS, payload: response.data });
  } catch (error) {
    dispatch({ type: GET_MSIDN_NUMS_FAIL });
  }
};

// post customer create proxy'
export const customerCreateProxy = (formData, callback) => async (dispatch) => {
  dispatch({ type: CREATE_CUSTOMER_PROXY });
  try {
    await API.post(`postcustomercreateproxy`, formData);
    callback();
    dispatch(deleteCart());
    dispatch({ type: CREATE_CUSTOMER_PROXY_SUCCESS });
  } catch (error) {
    toast.error("Something went wrong !");
    dispatch({ type: GET_MSIDN_NUMS_FAIL });
  }
};

// upload file
export const uploadFile = (formData, callback) => async (dispatch) => {
  dispatch({ type: UPLOAD_FILE });
  try {
    const res = await axios.post(
      `https://digitalsales.sandboxing.tech/api/add`,
      formData
    );
    if (res.data.data.message === "Document uploaded successfully!") {
      toast.success(res.data.data.message);
      callback(res.data.data.id);
      dispatch({ type: UPLOAD_FILE_SUCCESS });
    }
  } catch (error) {
    toast.error("Something went wrong !");
    dispatch({ type: UPLOAD_FILE_FAIL });
  }
};

// update data
export const updateData = (formData, id, callback) => async (dispatch) => {
  dispatch({ type: UPDATE_DATA });
  try {
    const res = await axios.put(
      `https://digitalsales.sandboxing.tech/api/update-data/${id}/`,
      formData
    );

    callback();
    dispatch({ type: UPDATE_DATA_SUCCESS });
  } catch (error) {
    toast.error("Something went wrong !");
    dispatch({ type: UPDATE_DATA_FAIL });
  }
};

// apply redeem code
export const applyRedeemCode = (formData, callback) => async (dispatch) => {
  dispatch({ type: APPLY_REDEEM_CODE });
  try {
    const payload = {
      code: formData.redeemcode,
      user: {
        mobile: formData.msisdn,
        email: formData.email,
        name: `${formData.firstname} ${formData.lastname}`,
      },
      params: {
        TRANSACTION_AMOUNT: formData.price,
        MERCHANT: "Telcom",
        PAYMENT_MODE: "CreaditCard",
        MOBILE: formData.msisdn,
        SERVICE_CODE: "BILLPAY",
      },
      lang: "en",
    };

    const response = await API.post(
      `http://rewards.sandboxing.tech/infinity/gifting-service/participation/check-code-benefit`,
      payload
    );
    if (response.data.codeStatus === "VALID") {
      const res = await API.get(
        `http://rewards.sandboxing.tech/infinity/gifting-service/participation/code?mobile=${formData.msisdn}&email=${formData.email}&code=${formData.redeemcode}&&channel=SMS&supplierid=INFINITY&inputformat=CODE_ONLY&tramt=${formData.price}&segment=`
      );

      toast.success(response.data.benefitDesc);
      callback();
      dispatch({
        type: APPLY_REDEEM_CODE_SUCCESS,
        payload: response.data.benefitDetailList,
      });
    } else {
      toast.error("Coupon code is not valid ");
      dispatch({ type: APPLY_REDEEM_CODE_SUCCESS });
    }
  } catch (error) {
    toast.error("Something went wrong !");
    dispatch({ type: APPLY_REDEEM_CODE_FAIL });
  }
};
