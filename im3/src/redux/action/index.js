export const BASEURL = "https://mockocsapi.sandboxing.tech/";

export const MORE_PLANS = "MORE_PLANS";
export const MORE_PLANS_SUCCESS = "MORE_PLANS_SUCCESS";
export const MORE_PLANS_FAIL = "MORE_PLANS_FAIL";

export const GET_MSIDN_NUMS = "GET_MSIDN_NUMS";
export const GET_MSIDN_NUMS_SUCCESS = "GET_MSIDN_NUMS_SUCCESS";
export const GET_MSIDN_NUMS_FAIL = "GET_MSIDN_NUMS_FAIL";

export const CREATE_CUSTOMER_PROXY = "CREATE_CUSTOMER_PROXY";
export const CREATE_CUSTOMER_PROXY_SUCCESS = "CREATE_CUSTOMER_PROXY_SUCCESS";
export const CREATE_CUSTOMER_PROXY_FAIL = "CREATE_CUSTOMER_PROXY_FAIL";

export const APPLY_REDEEM_CODE = "APPLY_REDEEM_CODE";
export const APPLY_REDEEM_CODE_SUCCESS = "APPLY_REDEEM_CODE_SUCCESS";
export const APPLY_REDEEM_CODE_FAIL = "APPLY_REDEEM_CODE_FAIL";

export const UPLOAD_FILE = "UPLOAD_FILE";
export const UPLOAD_FILE_SUCCESS = "UPLOAD_FILE_SUCCESS";
export const UPLOAD_FILE_FAIL = "UPLOAD_FILE_FAIL";

export const UPDATE_DATA = "UPDATE_DATA";
export const UPDATE_DATA_SUCCESS = "UPDATE_DATA_SUCCESS";
export const UPDATE_DATA_FAIL = "UPDATE_DATA_FAIL";

export const ADD_CART = "ADD_CART";
export const ADD_CART_SUCCESS = "ADD_CART_SUCCESS";
export const ADD_CART_FAIL = "ADD_CART_FAIL";

export const GET_CART = "GET_CART";
export const GET_CART_SUCCESS = "GET_CART_SUCCESS";
export const GET_CART_FAIL = "GET_CART_FAIL";

export const DELETE_CART = "DELETE_CART";
export const DELETE_CART_SUCCESS = "DELETE_CART_SUCCESS";
export const DELETE_CART_FAIL = "DELETE_CART_FAIL";
