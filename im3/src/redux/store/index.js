import thunk from "redux-thunk";
import { applyMiddleware, createStore } from "redux";

import rootReducers from "../reducer";

const store = createStore(rootReducers, applyMiddleware(thunk));

export default store;
